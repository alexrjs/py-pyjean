# PyJeAn - *Py*thon *Je*nkins *An*notater
=======

PyJeAn - *Py*thon *Je*nkins *An*notater is a rule and python based annotator for Jenkins xml configuration files.

- [MIT License; year=2014, 2015; copyright holders=arjs.net][0].

Thanks for supporting and using, have fun and enjoy!

## Feature:

### 1. Ready to launch!
You have everything built up, ready to launch your documentation of Jenkins config files ([5]). Just choose one of the shell scripts and add the xml file as parameter.

Available scripts:

- *j2seq.sh* - Generates a plantuml ([PlantUML]) sequence diagram file ([1])
- *j2png.sh* - Generate a png file with plantum ([PlantUML]), reuses the sequence rules ([2])
- *j2yaml.sh* - Generate a yaml file which can be used e.g. with ObenStack Jenkins Job Builder ([3]) ([OpenStack Jenkins Job Builder])
- *j2dmp.sh* - Generate a dmp file; This is only useful to see how PyJeAn works ([4])

### 2. Extend it!
**PyJeAn** is open, so you can extend it with own rules and rule implementations. It is simple.

- Create a new folder in the `rules`folder.
- Create a `__init__.py` file 
- Create a new pyhton file with a unique name; e.g. `domyownrules.py`; Have a look at the `rule.py` in the `assets` folder ([6])
- Create a rule file `myown.rule`in the `data` folder and add the line `domyownrules` in it
- Call the `pyjean.sh` script from the `bin` folder; e.g. `pyjean.sh domyownrules job.config.xml`


### 3. Highly customizable!
You don't like the default behaviour? It's extremely easy to add or change elements, optimize the python code, add new features, or fix bugs. And Hey!, it's open source, so get your hands dirty. Pull requests and support are welcome.

## Prerequisits:

+ pypy - I use pypy, so all shell scripts are using pypy
+ Text Editor or Python IDE, like Sublime Text or JetBrains PyCharme

## Structure:

* assets/ - An example job from my jenkins and a sample rule file
* bin/ - Some bash scripts
* docs/ - Dokumentation; TBD
* src/ - PeJeAn soruces
	+ controller/ - Controller
  		+ common/ - Global controller stuff
  		+ export/ - Export controller stuff
  		+ jenkins/ - Jenkins controller stuff
	+ data/ - .rule files
	+ libs/ - Place for custom libs; e.g. plantuml.jar
	+ model/ - Data model stuff
		+ jenkins/ - Jenkins model stuff
	+ rules/ - Rules implementation stuff
		+ common/ - common rules implementation
		+ jenkins/ - jenkins rules implemntation
		+ parts/ - parts rules implementation
		+ sequence/ - sequence and picture rules implementation
		+ yaml/ - yaml rules implementation
	+ tests/ - Tests; TBD

## Run

		<path to sikuli>pyjean <rule without .rule> <xml config of a jenkins job>

- <path to sikuli>: The path to the sikuli script that starts the sikuli script system
- <rule without .rule>: Path to the rule file without .rule extension
- <xml config of a jenkins job>: Path to the jenkins job xml config file

**Important remarks**:
 
- No wildcard support for rule files or folders; 
- same for jenkins xml files;
- Bash scripts are minimalistic

## Extra
As little extra you will find a kivy program `viewer` which I used as a litte helper program. To use it call `kivy main.py`. You might have to adapt the pathes in the `main.py` to make it work. And of course you need kivy installed ([Kivy]).

## Help:

You want or need help or support for your initial setup. Contact me at info(at)arjs.net

## Changelog:

2014/02/20	Added the Werk1 example

2014/02/09	Added documentation and a short tutorial; Some minor changes to the Readme files; Inline documentation fixes

2014/02/06	Redme Updates; First example

2014/02/01	Initial public release.

2013/12/01	Initial silent release.

## Links:

+ [Jenkins CI Server](http://jenkins-ci.org)
+ [Python programming language](http://python.org)
+ [PlantUML](http://plantuml.sourceforge.net/)
+ [OpenStack Jenkins Job Builder](http://ci.openstack.org/jjb.html)
+ [Kivy](http://kivy.org/)
+ [JePySi Test Framework](http://jepysi.arjs.net) - My other OSS project

## Installation
- Download the zip package from bitbucket to a place of your choice
- Unpack it
- Use it

[Kivy]: http://kivy.org/
[PlantUML]: http://plantuml.sourceforge.net/
[OpenStack Jenkins Job Builder]: http://ci.openstack.org/jjb.html
[0]: http://opensource.org/licenses/mit-license.html
[1]: assets/C-T-WS01-startPage/C-T-WS01-startPage.seq
[2]: assets/C-T-WS01-startPage/C-T-WS01-startPage.png
[3]: assets/C-T-WS01-startPage/C-T-WS01-startPage.yaml
[4]: assets/C-T-WS01-startPage/C-T-WS01-startPage.dmp
[5]: assets/C-T-WS01-startPage.config.xml
[6]: assets/rule.py
