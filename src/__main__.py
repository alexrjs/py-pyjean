"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

# standard imports
import os
import argparse

# custom imports
from controller.common import rule
from controller.common.rule import __rules__, __stacks__
from controller.common.log import log, view
from controller.export import sequence, save

__args__ = None


def onexit(func):
    """
    Marker for the exit function
    :rtype : function
    :param f: Exit function
    :type f: function
    :return:
    """
    import atexit

    atexit.register(func)
    return func


@log()
@onexit
def goodbye():
    """
    Exit function
    :rtype : None
    """
    if rule.__showstatistics__:
        if not __stacks__['tags'].isempty and not __stacks__['parts'].isempty:
            view('', '')
            view('*', 'Statistics:')
            view('.', 'Tags left = %s' % __stacks__['tags'].count)
            view('.', 'Parts to process = %s' % __stacks__['parts'].count)

    if rule.__dumpparts__ and not __stacks__['parts'].isempty:
        view('', '')
        view('*', 'Dump parts')
        while not __stacks__['parts'].isempty:
            part = __stacks__['parts'].first()
            if not part:
                continue

            view('.', 'Part (%s): %s' % (part.__class__.__name__, part.name))
            while not part.elements.isempty:
                element = part.elements.first()
                name = element.name if element is not isinstance(str) else element
                view('.' * 2, 'Element (%s): %s' % (element.__class__.__name__, name))
                if element is not isinstance(str):
                    __rules__.dumpelements(element, 3)
            view('', '')

        if not __stacks__['unhandled'].isempty:
            view('*', 'Dump unhandled')
            while not __stacks__['unhandled'].isempty:
                part = __stacks__['unhandled'].first()
                if not part:
                    continue

                name = part.name if part is not isinstance(str) else part
                view('!!', 'Unhandled (%s): %s' % (part.__class__.__name__, name))

    view('', '=' * 123)


@log()
def preparework(show):
    """
    Prepare the work
    :rtype : None
    """
    if __args__.verbose:
        view('*', 'import rules')

    __rules__.importer(show)


@log()
def dowork(filetoread):
    """
    Do the actual work
    :rtype : bool
    :param filetoread:
    :return:
    """
    if __args__.verbose:
        view('#', '=' * 120)
        view('#', 'Processing file %s' % (__args__.file if __args__.file else filetoread))
        view('#', '=' * 120)
        view('*', 'Prepare file with rules')

    __rules__.prepare(filetoread)

    if __args__.verbose:
        view('*', 'Process file with rules')

    __rules__.projectfilename = __rules__.projectname = filetoread.replace('.xml', '')
    if __rules__.projectfilename.endswith('.config'):
        __rules__.projectname = os.path.basename(__rules__.projectname.replace('.config', ''))

        if __args__.folder and __args__.trailing:
            __rules__.projectpath = __rules__.projectfilename.replace('.config', '').replace(__args__.folder.replace(__args__.trailing, ''), '').replace(__rules__.projectname, '')
            if __rules__.projectpath == '\\':
                __rules__.projectpath = ''
            else:
                pass
        elif __args__.folder:
            __rules__.projectpath = __rules__.projectfilename.replace('.config', '').replace(__args__.folder, '').replace(__rules__.projectname, '')
            if __rules__.projectpath == '\\':
                __rules__.projectpath = ''
            elif __rules__.projectpath.startswith('\\'):
                __rules__.projectpath = __rules__.projectpath.replace('\\', '', 1)
        elif __args__.trailing:
            __rules__.projectpath = __args__.trailing
        else:
            __rules__.projectpath = ''

        __rules__.projectfilename = __rules__.projectfilename.replace('.config', '')

    __rules__.process(__args__.process, __args__.verbose)

    if __args__.verbose:
        view('#', '=' * 120)

    return not __stacks__['parts'].isempty


@log()
def main():
    """
    Main function
    :rtype : int
    :raise AttributeError:
    """
    parser = argparse.ArgumentParser(
        description='Process (a) jenkins job xml(s) and generates (a) some new file(s) depending on a rule.'
    )
    parser.add_argument(
        '-f', '--file', dest='file', help='Process a single file.'
    )
    parser.add_argument(
        '-t', '--trailing', dest='trailing', help='Trailing folder for a single file.'
    )
    parser.add_argument(
        '-r', '--recurse', dest='folder', help='Process a single folder recursivly.'
    )
    parser.add_argument(
        '-p', '--process', dest='process', help='Rules to process.', required=True
    )
    parser.add_argument(
        '-i', '--ignores', dest='ignores', help='Ingore list separated with comma use with -r option only.'
    )
    parser.add_argument(
        '-s', '--statistics', dest='showstatistics', action='store_true', help='Show some statistics.'
    )
    parser.add_argument(
        '-d', '--dumpparts', dest='dumpparts', action='store_true', help='Dump parts found.'
    )
    parser.add_argument(
        '-v', '--verbose', dest='verbose', action='store_true', help='Be verbose.'
    )
    parser.add_argument(
        '-l', '--skiplegend', dest='skiplegend', action='store_true', help='Skip legend generation.'
    )
    parser.add_argument(
        '-o', '--output', dest='output', help='Store results in output folder.'
    )

    global __args__
    __args__ = parser.parse_args()

    rule.showstatistics = __args__.showstatistics
    rule.dumpparts = __args__.dumpparts
    sequence.skiplegend = __args__.skiplegend
    save.__outfolder__ = __args__.output

    if __args__.verbose:
        view('', 'PyJeAn v1.0.1')

    handle = os.path.isfile(__args__.process) and os.path.exists(__args__.process)
    if __args__.file:
        if __args__.file.endswith('.xml'):
            if os.path.isfile(__args__.file) and os.path.exists(__args__.file):
                handle = True
                preparework(__args__.verbose)
                dowork(__args__.file)
            else:
                raise EnvironmentError("File '%s' not found!" % __args__.file)
        else:
            raise AttributeError("File '%s' has no .xml extension and might not be a xml file." % __args__.file)

    elif __args__.folder:
        if os.path.isdir(__args__.folder) and os.path.exists(__args__.folder):
            handle = True
            preparework(__args__.verbose)

            ignore = False
            if __args__.ignores:
                ignores = __args__.ignores.split(',')
            else:
                ignores = None

            view('#', '=' * 120)
            view('#', 'Processing folder "%s"' % __args__.folder)
            for ppath, _, files in os.walk(__args__.folder):
                for pfile in files:
                    pfile = os.path.join(ppath, pfile)
                    if ignores:
                        for ignore in ignores:
                            if ignore in pfile:
                                break
                        else:
                            ignore = None

                    if ignore:
                        continue

                    if not pfile.endswith('.xml'):
                        if __args__.verbose:
                            view('--', 'Skipping file: %s' % pfile)

                        continue

                    view('++', 'Processing file %s' % pfile)
                    _, stop_work = dowork(pfile)
                    if stop_work:
                        exit(1)

        view('#', '=' * 121)

    if not handle:
        parser.print_help()
        exit(1)


if __name__ == '__main__':
    #import logging.config
    #logging.config.fileConfig('/path/to/logging.conf')
    main()
