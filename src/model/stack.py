"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""


class Stack:
    """
    Class for stack with special methods
    """
    def __init__(self):
        self.__storage = []

    def push(self, p):
        """
        Push an element to the stack
        :param p:
        :rtype : None
        """
        self.__storage.append(p)

    def pop(self, idx=-1):
        """
        Pop an element from the stack
        :param idx:
        :rtype : object
        """
        return self.__storage.pop(idx)

    def peek(self, idx=-1):
        """
        Peek at an element on the stack
        :rtype : object
        :param idx:
        :return:
        """
        if not self.isempty:
            return self.__storage[idx]
        else:
            return None

    def first(self):
        """
        Get the first element on the stack
        :rtype : object
        :return:
        """
        return self.__storage.pop(0) if len(self.__storage) > 0 else None

    def find(self, element):
        """
        Find an element on the stack
        :rtype : object
        :param element:
        :return:
        """
        if not self.isempty:
            return self.__storage.index(element)
        else:
            return None

    @property
    def count(self):
        return len(self.__storage)

    @property
    def isempty(self):
        return len(self.__storage) == 0

    def __iter__(self):
        return self.__storage.__iter__()