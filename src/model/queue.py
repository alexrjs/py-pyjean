"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""


class Queue:
    """
    Class for queues with special methods
    """
    def __init__(self):
        self.__storage = []

    def put(self, value):
        """
        Put an value in the queue
        :param value:
        :rtype : None
        """
        self.__storage.append(value)

    def set(self, idx, value):
        """
        Set an value at a specific index
        :param idx:
        :param value:
        :rtype : None
        """
        self.__storage[idx] = value

    def get(self, idx=0):
        """
        Get a value at 0 or at a specific index
        :param idx:
        :rtype : object
        """
        return self.__storage.pop(idx)

    def first(self):
        """
        Get the first value
        :rtype : object
        :return:
        """
        if not self.isempty:
            element = self.__storage[0]
            self.__storage.remove(element)
            return element
        else:
            return None

    def last(self):
        """
        Get the last value
        :rtype : object
        :return:
        """
        if not self.isempty:
            element = self.__storage[self.count - 1]
            self.__storage.remove(element)
            return element
        else:
            return None

    def remove(self, element):
        """
        Remove an element
        :param element:
        :rtype : object
        """
        if not self.isempty:
            return self.__storage.remove(element)
        else:
            return None

    def peek(self, idx=0):
        """
        Peek at the value at 0 or at a specific index
        :rtype : object
        :param idx:
        :return:
        """
        if not self.isempty:
            return self.__storage[idx]
        else:
            return None

    def find(self, element):
        """
        Find an elment
        :rtype : object
        :param element:
        :return:
        """
        if not self.isempty:
            return self.__storage.index(element)
        else:
            return None

    @property
    def count(self):
        return len(self.__storage)

    @property
    def isempty(self):
        return len(self.__storage) == 0

    def __iter__(self):
        return self.__storage.__iter__()
