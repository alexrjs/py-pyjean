"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.queue import Queue


class Job(object):
    """
    Class for handling jobs
    """

    def __init__(self, name):
        super(Job, self).__init__()
        self._name = name
        self._description = ''
        self._disabled = ''
        self._flavour = ''
        self._properties = Queue()
        self._scms = Queue()
        self._triggers = Queue()
        self._builders = Queue()
        self._publishers = Queue()
        self._buildwrappers = Queue()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

    @property
    def disabled(self):
        return self._disabled

    @disabled.setter
    def disabled(self, value):
        self._disabled = value

    @property
    def flavour(self):
        return self._flavour

    @flavour.setter
    def flavour(self, value):
        self._flavour = value

    @property
    def properties(self):
        return self._properties

    @property
    def scms(self):
        return self._scms

    @property
    def triggers(self):
        return self._triggers

    @property
    def builders(self):
        return self._builders

    @property
    def reporters(self):
        return self._publishers

    @property
    def publishers(self):
        return self._publishers

    @property
    def buildwrappers(self):
        return self._buildwrappers
