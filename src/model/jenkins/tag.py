# A. Schiessl
# 2014.04.05
#

from model.stack import Stack

TAG_TYPE_UNKNOWN = -1
TAG_TYPE_SINGLE = 0
TAG_TYPE_OPEN = 1
TAG_TYPE_CLOSE = 2


class Tag(object):
    """
    Class for handling a tag
    """

    def __init__(self, name):
        super(Tag, self).__init__()
        self._name = name
        # self._level = 0
        self._elements = Stack()

    @property
    def cname(self):
        return self.__class__.__name__

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    # @property
    # def level(self):
    # return self._level
    #
    # @level.setter
    # def level(self, value):
    # self._level = value

    @property
    def elements(self):
        return self._elements

    @elements.setter
    def elements(self, value):
        self._elements = value

    def element(self, key):
        rvalue = ''
        akey = '[[%s]]' % key
        if akey in self._elements:
            idx = self._elements.find(akey)
            if idx + 1 < self._elements.count:
                rvalue = self._elements.peek(idx + 1)
                if rvalue.startswith('[['):
                    rvalue = ''
            return rvalue
        else:
            return None

    def dump(self):
        # print "%s%s" % (' ' * self.level, self.name)
        print "%s%s" % (' ', self.name)


class Project(Tag):
    """
    Class for the Project Tag
    """

    def __init__(self, name):
        super(Project, self).__init__(name)
        self._flavour = ''

    @property
    def flavour(self):
        return self._flavour

    @flavour.setter
    def flavour(self, value):
        self._flavour = value


class Property(Tag):
    """
    Class for the Property Tag
    """

    def __init__(self, name):
        super(Property, self).__init__(name)
        self._properties = False


class Properties(Tag):
    def __init__(self, name):
        super(Properties, self).__init__(name)


class Scm(Tag):
    """
    Class for the Scm Tag
    """

    def __init__(self, name):
        super(Scm, self).__init__(name)


class Dsl(Tag):
    """
    Class for the Dsl Tag
    """

    def __init__(self, name):
        super(Dsl, self).__init__(name)


class Views(Tag):
    """
    Class for the Views Tag
    """

    def __init__(self, name):
        super(Views, self).__init__(name)


class Matrix(Tag):
    """
    Class for the Matrix Tag
    """

    def __init__(self, name):
        super(Matrix, self).__init__(name)


class Trigger(Tag):
    """
    Class for the Trigger Tag
    """

    def __init__(self, name):
        super(Trigger, self).__init__(name)


class Buildwrapper(Tag):
    """
    Class for the Buildwrapper Tag
    """

    def __init__(self, name):
        super(Buildwrapper, self).__init__(name)


class PreBuilder(Tag):
    """
    Class for the PreBuilder Tag
    """

    def __init__(self, name):
        super(PreBuilder, self).__init__(name)


class Builder(Tag):
    """
    Class for the Builder Tag
    """

    def __init__(self, name):
        super(Builder, self).__init__(name)


class PostBuilder(Tag):
    """
    Class for the PostBuilder Tag
    """

    def __init__(self, name):
        super(PostBuilder, self).__init__(name)


class Publisher(Tag):
    """
    Class for the Publisher Tag
    """

    def __init__(self, name):
        super(Publisher, self).__init__(name)


class Reporter(Tag):
    """
    Class for the Reporter Tag
    """

    def __init__(self, name):
        super(Reporter, self).__init__(name)
