"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""


def enum(*sequential, **named):
    """
    A helper funciton to create enums
    :rtype : type
    :param sequential:
    :param named:
    :return:
    """
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse

    return type('Enum', (), enums)
