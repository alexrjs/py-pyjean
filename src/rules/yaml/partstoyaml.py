"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import os
from model.jenkins.tag import Project, Matrix, Property, Property
from controller.common.log import view, info
from controller.common.rule import rule, __stacks__, __rules__
from controller.export import save
from controller.export.save import __outfolder__
from controller.jenkins.extract import extract


__filecount__ = {}
__ident__ = '    '


def execute_es(file_pointer, element, level, dash_sign=False):
    """
    Handle execution strategy
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    name = exchange(element.name)
    if name == 'execution-strategy':
        file_pointer.write('%s%s:\n' % (__ident__ * level, name))
        while not element.elements.isempty:
            execute_es(file_pointer, element.elements.first(), level + 1)
    elif name == 'expr':
        file_pointer.write('%s%s:\n' % (__ident__ * level, 'touchstone'))
        file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), name, enclose(element.name, element.elements.first())))
    elif name == 'result':
        child = element.elements.first().elements.first()
        file_pointer.write('%s%s: %s\n' % (__ident__ * (level + 1), name, exchange(child)))
    else:
        child = element.elements.first()
        child = child if child not in ['true', 'false'] else exchange(child)
        file_pointer.write("%s%s: %s\n" % (__ident__ * level, name, child))


def execute_cb(file_pointer, element, level, dash_sign=False):
    """
    Handle conditional builds
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    name = element.name
    if name == 'command':
        inc = include(name, element.elements.first())
        file_pointer.write("%s%s: %s\n" % (__ident__ * level, 'condition-command', inc))
    elif name in ['condition (AlwaysRun)', 'runCondition (AlwaysRun)']:
        file_pointer.write("%scondition-kind: always\n" % (__ident__ * level))

        if name == 'runCondition (AlwaysRun)':
            file_pointer.write("%ssteps: \n" % (__ident__ * level))
    elif name == 'condition (BatchFileCondition)':
        file_pointer.write("%scondition-kind: windows-shell\n" % (__ident__ * level))
        execute_cb(file_pointer, element.elements.first(), level)
        file_pointer.write("%ssteps: \n" % (__ident__ * level))
    elif name in ['condition (StatusCondition)', 'runCondition (StatusCondition)']:
        file_pointer.write("%scondition-kind: current-status\n" % (__ident__ * level))
        while not element.elements.isempty:
            child = element.elements.first()
            if child.name == 'worstResult':
                file_pointer.write("%scondition-worst: " % (__ident__ * level))
            elif child.name == 'bestResult':
                file_pointer.write("%scondition-best: " % (__ident__ * level))

            while not child.elements.isempty:
                first = child.elements.first()
                if first.name == 'name':
                    file_pointer.write("'%s'\n" % first.elements.first())
                    break

        if name == 'runCondition (StatusCondition)':
            file_pointer.write("%ssteps: \n" % (__ident__ * level))
    elif name == 'condition (StringsMatchCondition)':
        file_pointer.write("%scondition-kind: string-match\n" % (__ident__ * level))
        while not element.elements.isempty:
            child = element.elements.first()
            if child.name != 'ignoreCase':
                file_pointer.write("%s%s: " % ((__ident__ * level), child.name))
            elif child.name == 'ignoreCase':
                file_pointer.write("%s%s: " % ((__ident__ * level), exchange(child.name)))
            else:
                break

            while not child.elements.isempty:
                first = child.elements.first()
                if isinstance(first, str):
                    fmt = "%s\n" if first in ['false', 'true'] else "'%s'\n"
                    file_pointer.write(fmt % exchange(first))
                    break

        file_pointer.write("%ssteps: \n" % (__ident__ * level))
    else:
        file_pointer.write("%s%s: %s\n" % (__ident__ * level, name, element.elements.first()))


def execute_tb(file_pointer, element, level, dash_sign=False):
    """
    Handle triggered builds
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    if isinstance(element, str):
        file_pointer.write('%s%s:\n' % (__ident__ * level, element))
    else:
        name = exchange(element.name)
        word = __ident__
        use_dash = '  ' if dash_sign else ''

        if name.startswith('predefined'):
            file_pointer.write('%s%s%s:\n' % (word * level, use_dash, name))
            props = element.elements.first()
            while not props.elements.isempty:
                childs = props.elements.first().split('\n')
                for child in childs:
                    file_pointer.write('%s%s\n' % (word * (level + 1), child))
        elif name in ['block', 'current-parameters', 'generator-current-parameters']:
            file_pointer.write('%s%s%s: %s\n' % (word * level, use_dash, name, 'true'))
        elif name in ['threshold']:
            file_pointer.write('%s%s%s: %s\n' % (word * level, use_dash, name, element.elements.first().elements.first()))
        elif name in ['trigger-parameterized-builds', 'trigger-builds', 'trigger', '']:
            if name:
                use_dash = '- ' if dash_sign else ''
                file_pointer.write('%s%s%s:\n' % (__ident__ * level, use_dash, name))
                level += 1

            for child in filter(lambda p: p.name == 'projects', element.elements):
                name = exchange(child.name)
                use_dash = '- ' if islist(child.name) else ''
                file_pointer.write('%s%s%s: \'' % (word * level, use_dash, name))
                strbuf = ''
                while not child.elements.isempty:
                    first = child.elements.first()
                    strbuf += '%s,' % first

                strbuf = strbuf[:-1] if strbuf.endswith(',') else strbuf
                file_pointer.write(strbuf)
                idx = element.elements.find(child)
                element.elements.pop(idx)
                file_pointer.write('\'\n')

            while not element.elements.isempty:
                child = element.elements.first()
                execute_tb(file_pointer, child, level, True)
        else:
            while not element.elements.isempty:
                child = element.elements.first()
                child = child if child not in ['true', 'false'] else exchange(child)
                file_pointer.write('%s%s%s: %s\n' % (word * level, use_dash, name, enclose(element.name, child)))


def execute_pb(file_pointer, element, level, dash_sign=False):
    """
    Handle promoted builds
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    file_pointer.write("%s- promoted-build:\n" % (__ident__ * (level + 1)))
    file_pointer.write("%snames:\n" % (__ident__ * (level + 2)))
    for element in element.elements:
        file_pointer.write("%s- '%s'\n" % (__ident__ * (level + 3), element))


def execute_ee(file_pointer, element, level, dash_sign=True):
    """
    Handle extended email
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    used = []
    etrigger = {}

    if dash_sign:
        file_pointer.write("%s- %s:\n" % ((__ident__ * level), exchange(element.name)))

    for child in element.elements:
        if isinstance(child, str):
            file_pointer.write("%s%s: ''\n" % (__ident__ * (level + 1), child))
            continue

        name = exchange(child.name)
        if name in [
                'always', 'unstable', 'first-failure', 'not-built', 'aborted',
                'regression', 'failure', 'second-failure', 'improvement', 'still-failing',
                'success', 'fixed', 'still-unstable', 'pre-build'
        ]:
            etrigger[name] = child
        else:
            if name in used:
                continue

            used.append(name)
            if child.elements.count >= 1:
                while not child.elements.isempty:
                    item = child.elements.first()
                    if isinstance(item, str):
                        if map_child(child.name):
                            item = map_child(child.name)[item]
                        # enc = enclose(child.name, exchange(item)).replace('\n', '\\n\n')
                        enc = enclose(child.name, item).replace('\n', '\\n\n')
                        file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), name, enc))
                    else:
                        execute_ee(file_pointer, item, level, False)
            else:
                file_pointer.write("%s%s: ''\n" % (__ident__ * (level + 1), name))

    level += 1
    for key in etrigger:
        file_pointer.write("%s%s: True\n" % (__ident__ * level, key))
        for child in etrigger[key].elements:
            if isinstance(child, str):
                file_pointer.write("%s%s: ''\n" % (__ident__ * (level + 1), child))

    file_pointer.write("%ssend-to:\n" % (__ident__ * level))
    for key in etrigger:
        for child in etrigger[key].elements:
            if isinstance(child, str):
                continue
            else:
                name = exchange(child.name)
                if name in used:
                    continue

                if child.elements.count >= 1:
                    used.append(name)
                    while not child.elements.isempty:
                        item = child.elements.first()
                        if isinstance(item, str):
                            if map_child(child.name):
                                item = map_child(child.name)[item]
                            enc = enclose(child.name, exchange(item)).replace('\n', '\\n\n')
                            file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), name, enc))
                        else:
                            name = exchange(item.name)
                            if name in used:
                                continue

                            value = item.elements.first()
                            if value:
                                if value == 'true':
                                    file_pointer.write("%s- %s\n" % (__ident__ * (level + 1), name))
                            else:
                                file_pointer.write("%s%s: ''\n" % (__ident__ * (level + 1), name))
                else:
                    file_pointer.write("%s%s: ''\n" % (__ident__ * (level + 1), name))


def handle_gb(file_pointer, element, level, dash_sign=False):
    """
    Handle branch spec
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    file_pointer.write("%sbranches:\n" % (__ident__ * level))
    for element in element.elements:
        file_pointer.write("%s- '%s'\n" % (__ident__ * (level + 1), element.elements.first()))


def handle_gt(file_pointer, element, level, dash_sign=False):
    """
    Handle git scm trigger
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    file_pointer.write("%s- pollscm:\n" % (__ident__ * level))
    for element in element.elements:
        if isinstance(element, str):
            file_pointer.write("%sspec: '%s'\n" % (__ident__ * (level + 1), element))
        else:
            file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), exchange(element.name), element.elements.first()))


def handle_mj(file_pointer, element, level, dash_sign=False):
    """
    Handle multi job
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    for element in element.elements:
        if isinstance(element, str):
            file_pointer.write("%sspec: '%s'\n" % (__ident__ * level, element))
        elif element.name == 'jobName':
            child = element.elements.first()
            file_pointer.write('%s%s- %s: %s\n' % (__ident__ * (level - 1), '  ', 'name', enclose(element.name, child)))
        else:
            dumpparts(file_pointer, element, level)


def handle_eipw(file_pointer, element, level, dash_sign=False):
    """
    Handle predefinded build parameters
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    name = exchange(element.name)
    file_pointer.write("%s%s:\n" % ((__ident__ * level), name))
    for item in element.elements:
        if isinstance(item, str):
            file_pointer.write("%s: '%s'\n" % (__ident__ * level, item))
        elif item.name == 'name':
            name = 'name'
            child = item.elements.first()
            file_pointer.write('%s%s- %s: %s\n' % ((__ident__ * level), '  ', name, enclose(item.name, child)))
        elif item.name == 'value':
            name = 'password'
            child = item.elements.first()
            file_pointer.write('%s%s%s: %s\n' % ((__ident__ * (level + 1)), '', name, enclose(item.name, child)))
            # else:
            # dumpparts(file_pointer, item, level)


def handle_pp(file_pointer, element, level, dash_sign=False):
    """
    Handle predefinded build parameters
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    for element in element.elements:
        if isinstance(element, str):
            file_pointer.write("%s'%s'\n" % (__ident__ * (level + 1), element.replace(' ', '%20')))
        else:
            file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), exchange(element.name), element.elements.first()))


def handle_mvn(file_pointer, element, level, dash_sign=False):
    """
    Handle mvn
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    for element in element.elements:
        if isinstance(element, str):
            file_pointer.write("%s'%s'\n" % (__ident__ * (level + 1), element.replace(' ', '%20')))
        else:
            file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), exchange(element.name), element.elements.first()))


def handle_rpsr(file_pointer, element, level, dash_sign=False):
    """
    Handle post steps if result
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    name = exchange(element.name)
    file_pointer.write("%s%s:\n" % (__ident__ * level, name))
    for item in element.elements:
        if item.name == 'name':
            value = item.elements.first()
            if map_child(element.name):
                value = map_child(element.name)[value]
            file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), item.name, value))
        else:
            dumpparts(file_pointer, item, level + 1)


def handle_fp(*args):
    """
    Handle filepath
    :rtype : None
    :param args: filehandle, element, level
    """
    file_pointer = args[0]  # filehandle
    element = args[1]  # element
    level = args[2]  # level
    name = exchange(element.name)
    file_pointer.write("%s%s:\n" % (__ident__ * level, name))
    for item in element.elements:
        value = enclose(item.name, item.elements.first())
        file_pointer.write("%s%s: %s\n" % (__ident__ * (level + 1), item.name, value))


def handle_am(file_pointer, element, level, dash_sign=False):
    """
    Handle authorisation matrix
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param dash_sign:
    """
    dash_sign = dash_sign
    file_pointer.write("%s- %s:\n" % (__ident__ * level, exchange(element.name)))

    rights = {
        'hudson.model.Item.Delete': 'job-delete',
        'hudson.model.Item.Configure': 'job-configure',
        'hudson.model.Item.Read': 'job-read',
        'hudson.model.Item.ExtendedRead': 'job-extended-read',
        'hudson.model.Item.Discover': 'job-discover',
        'hudson.model.Item.Build': 'job-build',
        'hudson.model.Item.Workspace': 'job-workspace',
        'hudson.model.Item.Cancel': 'job-cancel',
        'hudson.model.Run.Delete': 'run-delete',
        'hudson.model.Run.Update': 'run-update',
        'hudson.scm.SCM.Tag': 'scm-tag'
    }

    users = {}
    for element in element.elements:
        if element.name not in 'permission':
            continue

        items = element.elements.first().split(':')
        user = items[1]
        if user not in users:
            users[user] = []

        users[user].append(rights[items[0]])

    for user, items in users.iteritems():
        file_pointer.write("%s'%s':\n" % (__ident__ * (level + 1), user))
        for right in items:
            file_pointer.write("%s  - %s\n" % (__ident__ * (level + 1), right))


__type_mapping__ = {
    'PT_SINGLE_SELECT': 'single-select',
    'PT_MULTI_SELECT': 'multi-select',
    'PT_MULTI_LEVEL_SINGLE_SELECT': 'multi-single-select',
    'PT_MULTI_LEVEL_MULTI_SELECT': 'multi-level-multi-select',
    'PT_RADIO': 'radio',
    'PT_CHECKBOX': 'checkbox'
}

__email_type_mapping__ = {
    'default': 'default',
    'text/html': 'html',
    'text/plain': 'text'
}

__project_type_mapping__ = {
    'freestyle': 'freestyle',
    'folder': 'folder',
    'matrix': 'matrix',
    'generator': 'generator',
    'multijob': 'multijob',
    'flow': 'flow',
    'maven2': 'maven',
    'maven2-set': 'maven-set'
}

__build_type_mapping__ = {
    'SUCCESS': 'stable',
    'UNSTABLE': 'unstable',
    'FAILURE': 'failure'
}

__mapping__ = {
    'true': {'name': 'True'},
    'false': {'name': 'False'},
    'SUCCESS': {'name': 'stable'},
    'UNSTABLE': {'name': 'unstable'},
    'FAILURE': {'name': 'failure'},

    'project-type': {'name': 'project-type', 'map-child': __project_type_mapping__},

    'GeneralProperties': {'name': ''},
    'properties': {'name': 'properties'},
    'displayName': {'name': 'display-name', 'enclose': "'"},
    'blockBuildWhenDownstreamBuilding': {'name': 'block-downstream'},
    'blockBuildWhenUpstreamBuilding': {'name': 'block-upstream'},
    'assignedNode': {'name': 'node'},
    'canRoam': {'name': 'can-roam'},
    'quietPeriod': {'name': 'quiet-period'},
    'concurrentBuild': {'name': 'concurrent'},
    'customWorkspace': {'name': 'workspace'},
    'keepDependencies': {'name': 'keep-dependencies'},
    'logRotator (LogRotator)': {'name': 'logrotate'},
    'generatedJobName': {'name': 'generated-job-name'},
    'generatedDisplayJobName': {'name': 'generated-job-display'},
    'autoRunJob': {'name': 'autorun '},
    'authToken': {'name': 'auth-token'},

    'JICustomIconProperty': {'name': 'custom-icon', 'list': True},
    'iconfile': {'name': 'iconfile'},

    'AdvancedQueueSorterJobProperty': {'name': 'queue-job-sorter', 'list': True},
    'useJobPriority': {'name': 'use-job-priority'},

    'AuthorizationMatrixProperty': {'name': 'authorization', 'list': True, 'before': True, 'handle': handle_am},

    'RebuildSettings': {'name': 'rebuild', 'list': True},
    'autoRebuild': {'name': 'auto-rebuild'},

    'ParametersDefinitionProperty': {'name': 'parameters', 'putback': True},
    'StringParameterDefinition': {'name': 'string', 'list': True},
    'BooleanParameterDefinition': {'name': 'bool', 'list': True},
    'defaultValue': {'name': 'default'},

    'FolderCredentialsProvider_-FolderCredentialsProperty': {'name': 'folder-credential'},
    'domainCredentialsMap (CopyOnWriteMap$Hash)': {'name': 'credentialmap'},
    'Domain': {'name': 'domain'},
    'CopyOnWriteArrayList': {'name': 'copyonwrite'},
    'icon (StockFolderIcon)': {'name': 'icon'},

    'EnvInjectJobProperty': {'name': 'inject', 'list': True},
    'info': {'name': ''},
    'propertiesFilePath': {'name': 'properties-file'},
    'propertiesContent': {'name': 'properties-content'},
    'loadFilesFromMaster': {'name': 'load-from-master'},
    'keepJenkinsSystemVariables': {'name': 'keep-system-variables'},
    'keepBuildVariables': {'name': 'keep-build-variables'},

    'EnvInjectPasswordWrapper': {'name': 'inject-passwords'},
    'injectGlobalPasswords': {'name': 'global'},
    'EnvInjectPasswordEntry': {'name': 'job-passwords', 'handle': handle_eipw},
    'EnvInjectBuildWrapper': {'name': 'inject', 'list': True},

    'ThrottleJobProperty': {'name': 'throttle', 'list': True},
    'maxConcurrentPerNode': {'name': 'max-per-node'},
    'maxConcurrentTotal': {'name': 'max-total'},
    'categories': {'name': '', 'inject': '-,categories:', 'list': True},
    'throttleEnabled': {'name': 'enabled'},
    'throttleOption': {'name': 'option'},
    'configVersion': {'name': 'version'},

    'SBScriptlerParameterDefinition_-ScriptParameter': {'name': 'parameter', 'list': True},
    'SBStringParameterDefinition': {'name': 'dynamic-string', 'list': True},
    'SBScriptlerChoiceParameterDefinition': {'name': 'dynamic-choice-scriptler', 'list': True},
    'SBChoiceParameterDefinition': {'name': 'dynamic-choice', 'list': True},
    '__uuid': {'name': 'uuid'},
    '__remote': {'name': 'remote'},
    '__scriptlerScriptId': {'name': 'script-id'},
    '__parameters': {'name': 'parameters'},
    '__script': {'name': 'script', 'include': 'groovy'},
    '__localBaseDirectory': {'name': ''},
    '__remoteBaseDirectory': {'name': 'remote-base'},
    '__classPath': {'name': 'classpath'},
    'FilePath': {'name': 'file-path', 'handle': handle_fp, 'before': True},
    'readonlyInputField': {'name': 'read-only'},

    'GeneratorKeyValueParameterDefinition': {'name': 'generator-key', 'list': True},
    'NodeParameterDefinition': {'name': 'node', 'list': True},
    'allowedSlaves': {'name': 'allowed-slaves'},
    'defaultSlaves': {'name': 'default-slaves'},
    'triggerIfResult': {'name': 'trigger-if-result'},
    'allowMultiNodeSelection': {'name': 'allow-multi-node'},
    'triggerConcurrentBuilds': {'name': 'trigger-concurrent'},
    'ignoreOfflineNodes': {'name': 'ignore-offline'},
    'nodeEligibility (AllNodeEligibility)': {'name': 'node-eligibility'},
    'ChoiceParameterDefinition': {'name': 'choice', 'list': True},
    'choices (Arrays$ArrayList)': {'name': '', 'inject': 'choices:', 'list': True, 'enclose': "'", 'level': '-'},
    'string': {'name': '', 'list': True},
    'PasswordParameterDefinition': {'name': 'password', 'list': True},
    'TextParameterDefinition': {'name': 'text', 'list': True},
    'FileParameterDefinition': {'name': 'file', 'list': True},
    'ListSubversionTagsParameterDefinition': {'name': 'svn-tags', 'list': True},
    'tagsDir': {'name': 'url'},
    'tagsFilter': {'name': 'filter'},
    'maxTags': {'name': 'max'},
    'reverseByDate': {'name': 'reverse-date'},
    'reverseByName': {'name': 'reverse-name'},
    'ExtendedChoiceParameterDefinition': {'name': 'extended-choice', 'list': True},
    'type': {'name': 'type', 'map-child': __type_mapping__},
    'ECPdefaultValue': {'name': 'default-value'},
    'quoteValue': {'name': 'quote-value'},
    'propertyFile': {'name': 'property-file'},
    'propertyKey': {'name': 'property-key'},
    'visibleItemCount': {'name': 'visible-items'},
    'multiSelectDelimiter': {'name': 'multi-select-delimiter'},

    'PipelineProperty': {'name': 'delivery-pipeline', 'list': True},
    'taskName': {'name': 'task'},
    'stageName': {'name': 'stage'},

    'activeProcessNames': {'name': '', 'handle': execute_pb, 'before': True},

    'Views': {'name': 'views'},
    'VIEWS': {'name': ''},
    'AllView': {'name': 'all', 'list': True},
    'ListView': {'name': 'list', 'list': True},
    'SectionedView': {'name': 'section', 'list': True},
    'ListViewSection': {'name': 'listsection', 'list': True},
    'TextSection': {'name': 'textsection', 'list': True},
    'text': {'name': 'text', 'include': 'txt'},
    'owner (Folder)': {'name': 'owner'},
    'columns': {'name': 'columns'},
    'filterExecutors': {'name': 'filter-executors'},
    'filterQueue': {'name': 'filter-queue'},
    'comparator (CaseInsensitiveComparator)': {'name': ''},
    'jobNames': {'name': 'job-names'},
    'jobFilters': {'name': 'job-filters'},
    'JobStatusFilter': {'name': 'job-status-filter'},
    'includeExcludeTypeString': {'name': 'include-exclude-type'},
    'StatusColumn': {'name': 'status-column', 'list': True},
    'WeatherColumn': {'name': 'weather-column', 'list': True},
    'JobColumn': {'name': 'job-column', 'list': True},
    'LastSuccessColumn': {'name': 'last-success-column', 'list': True},
    'LastFailureColumn': {'name': 'last-failure-column', 'list': True},
    'LastStableAndUnstableColumn': {'name': 'last-stable-and-unstable-column', 'list': True},
    'LastSuccessAndFailedColumn': {'name': 'last-success-and-failed-column', 'list': True},
    'timeAgoTypeString': {'name': 'time-ago-type'},
    'LastDurationColumn': {'name': 'last-duration-column', 'list': True},
    'NumBuildsColumn': {'name': 'num-builds-column', 'list': True},
    'BuildButtonColumn': {'name': 'build-button-column', 'list': True},
    'CustomIconColumn': {'name': 'custom-icon-column', 'list': True},
    'JobTypeColumn': {'name': 'job-type-column', 'list': True},
    'QualityColumn': {'name': 'quality-column', 'list': True},
    'JIincludeRegex': {'name': 'job-icon-regex-column', 'list': True},
    'JICustomIconColumn': {'name': 'job-icon-custom-column', 'list': True},
    'properties (View$PropertyList)': {'name': 'viewproperties'},
    'includeRegex': {'name': 'include-regex'},
    'RegExJobFilter': {'name': 'regex-job-filter'},
    'valueTypeString': {'name': 'value-type'},
    'viewsTabBar (DefaultViewsTabBar)': {'name': 'views-tabbar', 'list': True},
    'primaryView': {'name': 'primary-view', 'list': True},
    'healthMetrics': {'name': 'health-metrics', 'list': True},
    'WorstChildHealthMetric': {'name': 'worst-child-health-metric'},

    'Matrix': {'name': 'axes'},
    'MATRIX': {'name': 'axis'},
    'LabelAxis': {'name': '', 'inject': 'type: slave'},
    'GroovyAxis': {'name': '', 'inject': 'type: groovy'},
    'TextAxis': {'name': '', 'inject': 'type: user-defined'},
    'groovyString': {'name': 'groovy-string', 'include': 'groovy'},
    'values': {'name': '', 'inject': '-,values:', 'list': True},
    'computedValues': {'name': '', 'inject': '-,computed-values:', 'list': True},
    'executionStrategy (DefaultMatrixExecutionStrategyImpl)': {'name': 'execution-strategy', 'handle': execute_es},
    'runSequentially': {'name': 'sequential'},
    'touchStoneCombinationFilter': {'name': 'expr', 'enclose': "'"},
    'touchStoneResultCondition': {'name': 'result'},

    'Maven': {'name': 'maven-target'},
    'maven2': {'name': 'maven', 'handle': handle_mvn},
    'maven2-set': {'name': 'maven-set'},
    'rootModule': {'name': 'root-module', 'inject': '-,maven:', 'level': '+'},
    'dependency': {'name': 'dependency', 'list': True},
    'groupId': {'name': 'group-id'},
    'artifactId': {'name': 'artifact-id'},
    'parsedVersion (DefaultArtifactVersion)': {'name': 'parsed-version'},
    'majorVersion': {'name': 'major-version'},
    'minorVersion': {'name': 'minor-version'},
    'list': {'name': 'comparable-list'},
    'default': {'name': 'comparable-default', 'list': True},
    'int': {'name': 'comparable-int', 'list': True},
    'ComparableVersion_-IntegerItem': {'name': 'comparable-version-integer', 'list': True},
    'ComparableVersion_-StringItem': {'name': 'comparable-version-string', 'list': True},
    'goals': {'name': 'goals'},
    'aggregatorStyleBuild': {'name': 'aggregator-style'},
    'incrementalBuild': {'name': 'incremental'},
    'ignoreUpstremChanges': {'name': 'ignore-upstream-changes'},
    'archivingDisabled': {'name': 'archiving-disabled'},
    'siteArchivingDisabled': {'name': 'site-archiving-disabled'},
    'resolveDependencies': {'name': 'resolve-dependencies'},
    'processPlugins': {'name': 'process-plugins'},
    'mavenValidationLevel': {'name': 'validation-level'},
    'runHeadless': {'name': 'headless'},
    'disableTriggerDownstreamProjects': {'name': 'disable-trigger-downstream'},

    'runPostStepsIfResult': {'name': 'run-post-steps', 'level': '-', 'map-child': __build_type_mapping__,
                             'handle': handle_rpsr},
    'completeBuild': {'name': 'complete'},
    'mavenName': {'name': 'maven-version'},
    'usePrivateRepository': {'name': 'private-repository'},
    'targets': {'name': 'goals'},

    'MultiJobBuilder': {'name': 'multijob', 'listname': 'projects:'},
    'phaseName': {'name': 'name'},
    'continuationCondition': {'name': 'condition'},
    'PhaseJobsConfig': {'name': '', 'list': True, 'handle': handle_mj},
    'currParams': {'name': 'current-parameters'},
    'exposedSCM': {'name': 'exposed-scm'},
    'disableJob': {'name': 'disable-job'},
    'killPhaseOnJobResultCondition': {'name': 'kill-phase-on'},

    'Scm': {'name': 'scm'},
    'UserRemoteConfig': {'name': ''},
    'doGenerateSubmoduleConfigurations': {'name': 'disable-submodules'},
    'BranchSpec': {'name': '', 'handle': handle_gb},
    'locations': {'name': 'repos'},
    'SubversionSCM_-ModuleLocation': {'name': ''},
    'browser (ViewSVN)': {'name': 'scm-browser'},
    'ignoreDirPropChanges': {'name': 'ignore-property-changes-on-directories'},
    'filterChangelog': {'name': 'filter-changelog'},
    'remote': {'name': 'url', 'list': True},
    'local': {'name': '  basedir'},
    'depthOption': {'name': '  depth-option'},
    'ignoreExternalsOption': {'name': '  ignore-externals-option'},


    'Trigger': {'name': 'triggers'},
    'TimerTrigger': {'name': 'timed'},
    'PromotionTrigger': {'name': 'promotion'},
    'jobName': {'name': 'job'},
    'process': {'name': 'process'},
    'SCMTrigger': {'name': '', 'handle': handle_gt, 'before': True},
    'ignorePostCommitHooks': {'name': 'ignore-post-commit'},


    'Buildwrapper': {'name': 'wrappers'},
    'PreBuildCleanup': {'name': 'workspace-cleanup'},
    'deleteDirs': {'name': 'dirmatch'},
    'BuildNameSetter': {'name': 'build-name'},
    'template': {'name': 'name'},
    'TimestamperBuildWrapper': {'name': 'timestamps'},
    'CopyDataToWorkspacePlugin': {'name': 'copy-to-workspace'},
    'folderPath': {'name': 'folder-path'},
    'makeFilesExecutable': {'name': 'make-executable'},
    'deleteFilesAfterBuild': {'name': 'delete-files-after-build'},
    'copiedFiles': {'name': '', 'inject': 'copied-files:', 'list': True},


    'Builder': {'name': 'builders'},
    'PreBuilder': {'name': 'prebuilders'},
    'PostBuilder': {'name': 'postbuilders'},

    'BatchFileBuilder': {'name': 'batch', 'join': True},
    'PowerShell': {'name': 'powershell', 'join': True},
    'Shell': {'name': 'shell', 'join': True},
    'command': {'name': '', 'include': 'cmd'},

    'EnvInjectBuilder': {'name': 'inject'},

    'SystemGroovy': {'name': 'system-groovy'},
    'scriptSource (FileScriptSource)': {'name': ''},
    'scriptSource (StringScriptSource)': {'name': 'command', 'join': True},
    'scriptFile': {'name': 'file'},
    'bindings': {'name': 'bindings', 'join': True},

    'ScriptlerBuilder': {'name': 'scriptler', 'listname': 'parameters:'},
    'builderId': {'name': 'builder-id', 'enclose': "'"},
    'scriptId': {'name': 'script-id', 'enclose': "'"},
    'Parameter': {'name': 'parameter', 'list': True},
    'propagateParams': {'name': 'propagate'},
    'value': {'name': 'value', 'join': True},

    'ConditionalBuilder': {'name': 'conditional-step'},
    'SingleConditionalBuilder': {'name': 'single-conditional-step'},
    'condition (BatchFileCondition)': {'name': '', 'handle': execute_cb},
    'condition (AlwaysRun)': {'name': '', 'handle': execute_cb},
    'condition (StringsMatchCondition)': {'name': '', 'handle': execute_cb},
    'condition (StatusCondition)': {'name': '', 'handle': execute_cb},
    'runCondition (StatusCondition)': {'name': '', 'handle': execute_cb},
    'runCondition (AlwaysRun)': {'name': '', 'handle': execute_cb},
    'buildStep (BatchFile)': {'name': 'batch', 'list': True, 'join': True},
    'buildStep (TriggerBuilder)': {'name': ''},
    'runner (BuildStepRunner$Fail)': {'name': ''},
    'ignoreCase': {'name': 'ignore-case'},

    'BuildTriggerTask': {'name': 'trigger', 'handle': execute_tb, 'before': True, 'list': True},
    'TriggerBuilder': {'name': 'trigger-builds', 'handle': execute_tb, 'before': True, 'list': True},
    'BlockableBuildTriggerConfig': {'name': ''},
    'projects': {'name': 'project', 'enclose': "'", 'list': True},
    'PredefinedBuildParameters': {'name': 'predefined-parameters', 'handle': handle_pp},
    'PredefinedGeneratorParameters': {'name': 'predefined-generator-parameters'},
    'triggerWithNoParameters': {'name': 'no-parameters'},
    'buildAllNodesWithLabel': {'name': 'build-all-nodes-with-label'},

    'BapFtpBuilder': {'name': 'ftp'},
    'delegate': {'name': ''},
    'BapFtpTransfer': {'name': ''},
    'BapFtpPublisher': {'name': ''},
    'consolePrefix': {'name': 'console-prefix'},
    'publishers': {'name': ''},
    'configName': {'name': 'site'},
    'remoteDirectory': {'name': 'target'},
    'sourceFiles': {'name': 'source'},
    'removePrefix': {'name': 'remove-prefix'},
    'remoteDirectorySDF': {'name': 'remote-directory-sdf'},
    'cleanRemote': {'name': 'clean-remote'},
    'noDefaultExcludes': {'name': 'no-default-excludes'},
    'makeEmptyDirs': {'name': 'make-empty-dirs'},
    'patternSeparator': {'name': 'pattern-separator'},
    'asciiMode': {'name': 'ascii-mode'},
    'useWorkspaceInPromotion': {'name': 'use-workspace'},
    'usePromotionTimestamp': {'name': 'use-timestamp'},
    'excludes': {'name': 'excludes'},
    'continueOnError': {'name': 'continue-on-error'},
    'failOnError': {'name': 'fail-on-error'},
    'alwaysPublishFromMaster': {'name': 'from-master'},

    'Gradle': {'name': 'gradle'},
    'tasks': {'name': 'tasks'},
    'switches': {'name': '', 'enclose': "'", 'listname': 'switches:', 'list': True},
    'rootBuildScriptDir': {'name': 'root-build-script-dir'},
    'buildFile': {'name': 'build-file'},
    'gradleName': {'name': 'gradle-name'},
    'useWrapper': {'name': 'wrapper'},
    'makeExecutable': {'name': 'executable'},
    'fromRootBuildScriptDir': {'name': 'use-root-dir'},
    'useWorkspaceAsHome': {'name': 'workspace-as-home'},

    'Ant': {'name': 'ant'},
    'antName': {'name': 'ant-name'},
    'antOpts': {'name': 'java-opts'},
    'antTargets': {'name': 'targets'},


    'Publisher': {'name': 'publishers'},
    'DescriptionSetterPublisher': {'name': 'description-setter'},
    'regexp': {'name': 'regexp', 'enclose': '"', 'encode': True},
    'regexpForFailed': {'name': 'regexp-for-failed', 'enclose': '"'},
    'description': {'name': 'description', 'enclose': "'", 'encode': True},
    'descriptionForFailed': {'name': 'description-for-failed', 'enclose': "'"},
    'setForMatrix': {'name': 'set-for-matrix'},

    'TextFinderPublisher': {'name': 'text-finder'},
    'alsoCheckConsoleOutput': {'name': 'also-check-console-output'},
    'succeedIfFound': {'name': 'succeed-if-found'},
    'unstableIfFound': {'name': 'unstable-if-found'},

    'SiteMonitorRecorder': {'name': 'sitemonitor'},
    'mSites': {'name': 'sites'},
    'Site': {'name': '', 'list': True},
    'mUrl': {'name': 'url', 'list': True},

    'Mailer': {'name': 'email'},
    'recipients': {'name': 'recipients'},
    'dontNotifyEveryUnstableBuild': {'name': 'notify-every-unstable-build'},
    'sendToIndividuals': {'name': 'send-to-individuals'},

    'JUnitResultArchiver': {'name': 'junit'},
    'testResults': {'name': 'results'},
    'keepLongStdio': {'name': 'keep-long-stdio'},
    'testDataPublishers': {'name': 'test-stability'},

    'BuildTrigger': {'name': 'trigger-parameterized-builds', 'handle': execute_tb, 'before': True, 'list': True},
    'BuildTriggerConfig': {'name': '', 'handle': execute_tb, 'before': True, 'list': True},
    'CurrentBuildParameters': {'name': 'current-parameters'},
    'GeneratorCurrentParameters': {'name': 'generator-current-parameters'},
    'childProjects': {'name': 'project', 'enclose': "'"},
    'threshold': {'name': 'threshold', 'enclose': "'"},

    'ArtifactArchiver': {'name': 'archive'},
    'artifacts': {'name': 'artifacts'},
    'latestOnly': {'name': 'latest-only'},
    'allowEmptyArchive': {'name': 'allow-empty'},
    'onlyIfSuccessful': {'name': 'only-if-successful'},
    'fingerprint': {'name': 'fingerprint'},
    'defaultExcludes': {'name': 'default-excludes'},

    'NaginatorPublisher': {'name': 'nag'},
    'regexpForRerun': {'name': 'regex'},
    'rerunIfUnstable': {'name': 'rerun-unstable'},
    'checkRegexp': {'name': 'check-regex'},
    'delay': {'name': 'delay'},
    'maxSchedule': {'name': 'retry'},

    'ExtendedEmailPublisher': {'name': 'email-ext', 'handle': execute_ee, 'before': True, 'list': True},
    'recipientList': {'name': 'recipients'},
    'AlwaysTrigger': {'name': 'always'},
    'UnstableTrigger': {'name': 'unstable'},
    'FirstFailureTrigger': {'name': 'first-failure'},
    'NotBuiltTrigger': {'name': 'not-built'},
    'AbortedTrigger': {'name': 'aborted'},
    'RegressionTrigger': {'name': 'regression'},
    'FailureTrigger': {'name': 'failure'},
    'SecondFailureTrigger': {'name': 'second-failure'},
    'ImprovementTrigger': {'name': 'improvement'},
    'StillFailingTrigger': {'name': 'still-failing'},
    'SuccessTrigger': {'name': 'success'},
    'FixedTrigger': {'name': 'fixed'},
    'StillUnstableTrigger': {'name': 'still-unstable'},
    'PreBuildTrigger': {'name': 'pre-build'},
    'contentType': {'name': 'content-type', 'map-child': __email_type_mapping__},
    'defaultSubject': {'name': 'subject'},
    'defaultContent': {'name': 'body'},
    'attachBuildLog': {'name': 'attach-build-log'},
    'compressBuildLog': {'name': 'compress-build-log'},
    'attachmentsPattern': {'name': 'attachments'},
    'presendScript': {'name': 'presend-script'},
    'replyTo': {'name': 'reply-to'},
    'sendToDevelopers': {'name': 'developers'},
    'sendToRequester': {'name': 'requester'},
    'includeCulprits': {'name': 'culprits'},
    'sendToRecipientList': {'name': 'recipients '},  # blank at the end to avoid filtered away
    'saveOutput': {'name': 'save-output'},


    'Reporter': {'name': 'reporters'},
    'MavenMailer': {'name': 'email'},
    'perModuleEmail': {'name': 'per-module'},


    'MaskPasswordsBuildWrapper': {'name': 'mask-passwords'},
    'varPasswordPairs': {'name': 'password-pairs'},

    'Dsl': {'name': 'dsl'},
    'DSL': {'name': '', 'dash': '', 'level': '-'},
}


def exchange(item):
    """
    Check and exchange
    :rtype : str
    :param item:
    :return:
    """
    if item in __mapping__:
        return __mapping__[item]['name']
    else:
        return item


def dash(item):
    """
    Check and return dash
    :rtype : str
    :param item:
    :return:
    """
    if item in __mapping__:
        if 'dash' in __mapping__[item]:
            return __mapping__[item]['dash']
        else:
            return '- '
    else:
        return '- '


def join(item):
    """
    Check and return bool
    :rtype : bool
    :param item:
    :return:
    """
    if item in __mapping__:
        if 'join' in __mapping__[item]:
            return __mapping__[item]['join']
        else:
            return False
    else:
        return False


def islist(item):
    """
    Check and return bool
    :rtype : bool
    :param item:
    :return:
    """
    if item in __mapping__:
        if 'list' in __mapping__[item]:
            return __mapping__[item]['list']
        else:
            return False
    else:
        return False


def listname(item, default=None):
    """
    Check and return list name
    :rtype : str
    :param item:
    :param default:
    :return:
    """
    if item in __mapping__:
        if 'listname' in __mapping__[item]:
            return __mapping__[item]['listname']
        else:
            return default
    else:
        return default


def putback(item):
    """
    Check and return bool
    :rtype : bool
    :param item:
    :return:
    """
    if item in __mapping__:
        if 'putback' in __mapping__[item]:
            return __mapping__[item]['putback']
        else:
            return False
    else:
        return False


def adjustlevel(item, value):
    """
    Check and do adjust level
    :rtype : int
    :param item:
    :param value:
    :return:
    """
    if item in __mapping__:
        if 'level' in __mapping__[item]:
            lvl = __mapping__[item]['level']
            value = value + 1 if lvl == '+' else value - 1
            return value
        else:
            return value
    else:
        return value


def enclose(item, text, src='\'', repl='\"'):
    """
    Check and do enclosment
    :rtype : str
    :param item:
    :param text:
    :param s:
    :param r:
    :return:
    """
    if text in ['true', 'false', 'True', 'False']:
        return text

    if item in __mapping__:
        if 'encode' in __mapping__[item]:
            if '\'' in text:
                text = text.replace('\'', '&apos;')
            if '\d' in text:
                text = text.replace('\d', '\\\\d')

        if 'enclose' in __mapping__[item]:
            return '%s%s%s' % (__mapping__[item]['enclose'], text.replace(src, repl), __mapping__[item]['enclose'])
        else:
            return '%s%s%s' % (src, text.replace(src, repl), src)
    else:
        return '%s%s%s' % (src, text.replace(src, repl), src)


def include(name, item):
    """
    Check and do include handling
    :rtype : str
    :param name:
    :param item:
    :return:
    """
    if name in __mapping__:
        if 'include' in __mapping__[name]:
            ext = __mapping__[name]['include']
            if ext not in __filecount__:
                __filecount__[ext] = 0
            __filecount__[ext] += 1
            file_path = os.path.dirname(__rules__.projectfilename)
            file_name = __rules__.projectname
            file_name = '%s.%03d.%s' % (file_name, __filecount__[ext], ext)
            if save.outfolder:
                fnp = os.path.join(os.path.join(file_path, save.outfolder), file_name)
            else:
                fnp = os.path.join(file_path, file_name)

            file_pointer = open(fnp, 'w')
            if 'encode' in __mapping__[name]:
                file_pointer.write(item.replace('\\\\', '\\'))
            else:
                file_pointer.write(item)
            file_pointer.close()

            return "!include-raw '%s'" % file_name
        else:
            return None
    else:
        return None


def inject(item, default=None):
    """
    Check and return inject str
    :rtype : str
    :param item:
    :param default:
    :return:
    """
    if item in __mapping__:
        if 'inject' in __mapping__[item]:
            return __mapping__[item]['inject']
        else:
            return default
    else:
        return default


def handle(item, default):
    """
    Check for special handling
    :rtype : str, bool
    :param item:
    :param default:
    :return:
    """
    if item in __mapping__:
        before = __mapping__[item]['before'] if 'before' in __mapping__[item] else False
        if 'handle' in __mapping__[item]:
            return __mapping__[item]['handle'], before
        else:
            return default, False
    else:
        return default, False


def map_child(item):
    """
    Check and map child values
    :rtype : bool or str
    :param item:
    :return:
    """
    if item in __mapping__:
        if 'map-child' in __mapping__[item]:
            return __mapping__[item]['map-child']
        else:
            return False
    else:
        return False


def dumpparts(file_pointer, element, level, make_list=False):
    """
    Default dump parts function
    :rtype : None
    :param f:
    :param element:
    :param level:
    :param make_list:
    :return:
    """
    name = exchange(element.name)
    j = join(element.name)
    if putback(element.name):
        if 'wasputback' not in __mapping__[element.name]:
            __stacks__['parts'].put(element)
            __mapping__[element.name]['wasputback'] = True
            return
        elif __mapping__[element.name]['wasputback']:
            make_list = False

    is_list = islist(element.name)
    use_dash = dash(element.name) if make_list or is_list else ''

    _inject = inject(element.name)
    if _inject:
        if _inject.startswith('-,'):
            file_pointer.write('%s%s\n' % (__ident__ * (level - 1), _inject.replace('-,', '')))
        elif _inject.startswith('+,'):
            file_pointer.write('%s%s\n' % (__ident__ * (level + 1), _inject.replace('+,', '')))
        else:
            file_pointer.write('%s%s\n' % (__ident__ * level, _inject))

    handler, base = handle(element.name, dumpparts)
    if base:
        handler(file_pointer, element, level, is_list)
        return

    if element.elements.count == 0:
        if use_dash:
            if name:
                file_pointer.write('%s%s%s\n' % (__ident__ * level, use_dash, name))
            else:
                file_pointer.write('%s%s%s\n' % (__ident__ * (level + 1), use_dash, '<blank>'))
        elif name:
            file_pointer.write("%s%s: ''\n" % (__ident__ * level, name))

    elif element.elements.count == 1:
        child = element.elements.first()
        level = adjustlevel(element.name, level)
        if isinstance(child, str):
            if map_child(element.name):
                child = map_child(element.name)[child]
            inc = include(element.name, child)
            if not name:
                if inc:
                    file_pointer.write(' %s\n' % inc)
                else:
                    enc = enclose(element.name, child).replace('\n', '\\n\n')
                    file_pointer.write('%s%s%s\n' % (__ident__ * (level + 1), use_dash, enc))
            else:
                if inc:
                    file_pointer.write("%s%s%s: %s\n" % (__ident__ * level, use_dash, name, inc))
                else:
                    enc = enclose(element.name, child).replace('\n', '\\n\n')
                    file_pointer.write("%s%s%s: %s\n" % (__ident__ * level, use_dash, name, enc))
        else:
            if j:
                file_pointer.write('%s%s%s:' % (__ident__ * level, use_dash, name))
            else:
                if name:
                    file_pointer.write('%s%s%s:\n' % (__ident__ * level, use_dash, name))
                else:
                    level -= 1

            if name == 'steps':
                handler(file_pointer, child, level, True)
            else:
                handler(file_pointer, child, level + 1)

    elif element.elements.count > 1:
        if name:
            if j:
                file_pointer.write('%s%s%s: \'' % (__ident__ * level, use_dash, name))
            else:
                file_pointer.write('%s%s%s:\n' % (__ident__ * level, use_dash, name))
                level += 1

        first = True
        _listname = listname(element.name)
        while not element.elements.isempty:
            child = element.elements.first()
            if isinstance(child, str):
                if map_child(element.name):
                    child = map_child(element.name)[child]
                if j:
                    if first:
                        file_pointer.write('%s' % child)
                        first = False
                    else:
                        file_pointer.write(',%s' % child)
                elif not is_list:
                    file_pointer.write('%s%s\n' % (__ident__ * level, exchange(child)))
                else:
                    enc = enclose(element.name, exchange(child)).replace('\n', '\\n\n')
                    file_pointer.write('%s- %s\n' % (__ident__ * level, enc))
            else:
                is_list = islist(child.name)
                _ln = _listname if not listname(child.name) else listname(child.name)
                if is_list:
                    if _ln:
                        file_pointer.write('%s%s\n' % (__ident__ * level, _ln))
                        _listname = ''
                    level += 1

                handler, _ = handle(child.name, dumpparts)
                level = adjustlevel(child.name, level)
                handler(file_pointer, child, level, is_list)

                if is_list:
                    level -= 1

        if name:
            if j:
                file_pointer.write('\'\n')
    else:
        file_pointer.write('%s- %s\n' % (__ident__ * level, name))


@rule(__name__)
def execute(show=False):
    """
    Execute parts to yaml rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    parts = __stacks__['parts']
    rules = __stacks__['rules']
    unhandled = __stacks__['unhandled']
    if parts.isempty:
        return

    info(show, '.', 'partstoyaml', 'Start convert to yaml')

    extension = 'yaml'
    if outfolder:
        file_name = os.path.basename(rules.projectfilename)
        file_path = os.path.dirname(rules.projectfilename)
        file_path = os.path.join(file_path, outfolder)
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        file_name = '%s.%s' % (file_name, extension)
        file_name = os.path.join(file_path, file_name)
    else:
        file_name = '%s.%s' % (rules.projectfilename, extension)

    ignore = ['Properties', 'Property']
    currentblock = ''
    with open(file_name, 'w') as file_pointer:
        # first do special parts
        for typ in [Project, Matrix, Property, Property]:  # 2nd Property for 'putback' properties
            newline = False
            for part in [p for p in parts if isinstance(p, typ)]: # filter(lambda p: type(p) is typ, parts):
                if typ is Project:
                    part.name = 'job'
                    dumpparts(file_pointer, part, 0, True)
                else:
                    name = part.__class__.__name__
                    if name not in ignore:
                        if currentblock != name:
                            currentblock = name
                            file_pointer.write('%s%s:\n' % (__ident__, exchange(currentblock)))

                        dumpparts(file_pointer, part, 2, True)
                    else:
                        dumpparts(file_pointer, part, 1, True)

                parts.remove(part)
                newline = True

            if newline:
                file_pointer.write('\n')

        # second do the common parts
        level = 1
        while not parts.isempty:
            part = parts.first()
            name = part.__class__.__name__
            if currentblock != name:
                currentblock = name
                file_pointer.write('%s%s:\n' % (__ident__, exchange(currentblock)))

            dumpparts(file_pointer, part, level + 1, True)

            file_pointer.write('\n')

        # third, handle unhandled
        if not unhandled.isempty:
            view('!', 'Dump unhandled\n')
            file_pointer.write('Dump unhandled\n')
            while not unhandled.isempty:
                part = unhandled.first()
                if not part:
                    continue

                name = part.name if not isinstance(part, str) else part
                out = '%s Unhandled (%s): %s\n' % ('!!', part.__class__.__name__, name)
                file_pointer.write(out)
                view('.', out)

    info(show, '.', 'partstoyaml', 'Finished convert to yaml')

    return True
