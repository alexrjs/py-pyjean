"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Reporter
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup

__external__ = {
    'MavenMailer': 'Mailer',
}


@rule(__name__)
def execute(show=False):
    """
    Execute reporter rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    participant = 'Reporter'
    code = '\n'.join(fillgroup(__stacks__['parts'], Reporter, participant, 'report', __external__))
    if code:
        return save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            ''
        ]), 'seq'
                   )
    else:
        return False
