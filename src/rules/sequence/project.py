"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Project, Property
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import getcolor


@rule(__name__)
def execute(show=False):
    """
    Execute project rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    has = []
    code = []
    project = None
    color = getcolor('enabled')
    for part in __stacks__['parts']:
        if isinstance(part, Project):
            project = part
            name = part.name.split('/')
            for item in part.elements:
                if not isinstance(item, Property):
                    continue

                if item.name == 'description' or item.name == 'displayName':
                    if item.elements.isempty:
                        continue

                    if not item.elements.isempty:
                        code.append("' Title ")
                        code.append('title')
                        for element in item.elements:
                            code.append(
                                element.replace('&amp;', '&')
                                .replace('&gt;', '>')
                                .replace('&lt;', '<')
                                .replace('&quot;', '\"')
                                .replace('&apos;', '\'')
                                .replace('&#13;', '')
                                .replace('&#xd;', '')
                                .replace('<hr>', '')
                                .replace('<br>', '')
                            )
                        code.append('end title')
                        code.append('')
                elif item.name == 'disabled':
                    if item.elements.isempty:
                        continue

                    disabled = item.elements.peek()
                    color = getcolor('disabled') if disabled == 'true' else getcolor('enabled')

            code.append("' Participants ")
            code.append('actor %s %s' % ('External', getcolor('External')))
            code.append('control %s %s' % ('Trigger', getcolor('Trigger')))

            if part.flavour:
                code.append('box "%s" %s' % (part.flavour + ': ' + name[-1], color))
            else:
                code.append('box "%s" %s' % (name[-1], color))

            code.append(' entity %s %s' % ('Job', getcolor('Job')))
            code.append(' control %s %s' % ('Engine', getcolor('Engine')))

            if 'Matrix' in has:
                code.append(' participant %s %s' % ('Matrix', getcolor('Matrix')))

            if 'Buildwrapper' in has:
                code.append(' participant %s %s' % ('Buildwrapper', getcolor('Buildwrapper')))

            if 'Builder' in has:
                code.append(' participant %s %s' % ('Builder', getcolor('Builder')))

            if 'PreBuilder' in has:
                code.append(' participant %s %s' % ('PreBuilder', getcolor('Builder')))

            if 'PostBuilder' in has:
                code.append(' participant %s %s' % ('PostBuilder', getcolor('Builder')))

            if 'Publisher' in has:
                code.append(' participant %s %s' % ('Publisher', getcolor('Publisher')))

            if 'Reporter' in has:
                code.append(' participant %s %s' % ('Reporter', getcolor('Reporter')))

            if 'Dsl' in has:
                code.append(' participant %s %s' % ('DSL', getcolor('DSL')))

        else:
            cname = part.cname
            if cname not in has:
                has.append(cname)
            continue

        code.append('end box')

        if 'Scm' in has:
            code.append('database %s %s' % ('SCM', getcolor('SCM')))

        code.append('')

    __stacks__['parts'].remove(project)

    return save('\n'.join([
        '',
        "' Project",
        '\n'.join(code),
        ''
    ]), 'seq'
               )
