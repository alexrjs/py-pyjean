"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Trigger
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup, fillnote, register, registercategories, activate, noteover, \
    deactivate

__highlighttags__ = [
    'process',
    'jobName',
]


def addgroupitem(item, participant, action, external):
    """
    Add group item
    :rtype : str[]
    :param item:
    :param participant:
    :param action:
    :param external:
    :return:
    """
    participant = participant
    action = action
    external = external
    code = [
        ' Job->Trigger : enable( ' + item.name + ' )',
        activate('Trigger'),
        '',
        noteover('Job'),
    ]

    for element in item.elements:
        code += fillnote(element, 2)

    code.append(' end note')
    code.append('')
    code.append(' Job<-Trigger')
    code.append(deactivate('Trigger'))
    code.append(' Trigger-->Job : %s( %s )' % (action, item.name))
    code.append('')

    return code


@rule(__name__)
def execute(show=False):
    """
    Execute trigger rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    register(registercategories.Highlights, __highlighttags__)
    participant = 'Trigger'
    code = '\n'.join(fillgroup(__stacks__['parts'], Trigger, participant, 'triggers', None, addgroupitem))
    if code:
        return save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            ' External-->Job : batch()',
            activate('Job'),
            ' External-->Job : manual()',
            ' External-->Job : rebuild()',
            code,
            "end ' %ss" % participant,
            '',
            '',
        ]), 'seq'
                   )
    else:
        if __stacks__['parts'].peek().name == 'VIEWS':
            return False
        else:
            return save('\n'.join([
                "' %ss" % participant,
                'group %ss' % participant,
                '',
                ' External-->Job : batch()',
                activate('Job'),
                ' External-->Job : manual()',
                ' External-->Job : rebuild()',
                '',
                "end ' %ss" % participant,
                '',
                '',
            ]), 'seq'
                       )
