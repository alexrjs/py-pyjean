"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Buildwrapper
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup


@rule(__name__)
def execute(show=False):
    """
    Execute buildwrapper rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    participant = 'Buildwrapper'
    code = '\n'.join(fillgroup(__stacks__['parts'], Buildwrapper, participant, 'activate'))
    if code:
        return save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            '',
            ''
        ]), 'seq'
                   )
    else:
        return False
