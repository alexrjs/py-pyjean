"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Scm
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup, register, registercategories

__highlighttags__ = [
    'url',
]


@rule(__name__)
def execute(show=False):
    """
    Execute scm rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    register(registercategories.Highlights, __highlighttags__)
    participant = 'SCM'
    code = '\n'.join(fillgroup(__stacks__['parts'], Scm, participant, 'use'))
    if code:
        return save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            '',
            ''
        ]), 'seq'
                   )
    else:
        return False
