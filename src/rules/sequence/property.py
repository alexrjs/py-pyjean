"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Property, Properties
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillnote, register, registercategories, activate, noteright

__highlighttags__ = [
    'name',
    'value',
    '__script',
]


def doactivate(activated, code):
    """
    Activates the Engine
    :rtype : None
    :param activated:
    :param code:
    """
    if activated:
        code.append('')
        code.append(activate('Engine'))


def addgroupitem(item, activated, level):
    """
    Add a group item
    :rtype : str[]
    :param item:
    :param activated:
    :param level:
    :return:
    """
    code = []
    items = item.name.split(' ')
    name = items[0]
    if isinstance(item, str):
        code.append('%s%s:' % (' ' * level, item))
        doactivate(activated, code)
    elif item.elements.count == 1 and isinstance(item.elements.peek(), str):
        code.append(' Job-->Engine : %s : %s' % (name, item.elements.peek()))
        doactivate(activated, code)
    else:
        code.append(' Job-->Engine : %s :' % name)
        doactivate(activated, code)
        code.append(noteright('Job'))
        for element in item.elements:
            code += fillnote(element, level + 2)
        code.append(' end note')

    code.append('')

    return code


def _fillgroup(parts):
    """
    Fill a group
    :rtype : str[]
    :param parts:
    :return:
    """
    code = []
    activated = True
    # filter(lambda p: p is isinstance(Property), parts):
    for part in [p for p in parts if isinstance(p, Property)]:
        parts.remove(part)
        for element in part.elements:
            if isinstance(element, Properties):
                for elem in element.elements:
                    code += addgroupitem(elem, activated, 0)
                continue

            if not isinstance(element, Property):
                continue

            code += addgroupitem(element, activated, 0)
            activated = False

    return code


@rule(__name__)
def execute(show=False):
    """
    Execute property rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    if show:
        pass

    register(registercategories.Highlights, __highlighttags__)
    participant = 'Properties'
    return save('\n'.join([
        "' %s" % participant,
        'group %s' % participant,
        '',
        '\n'.join(_fillgroup(__stacks__['parts'])),
        ' deactivate Engine',
        '',
        "end ' %s" % participant,
        '',
        ''
        ]), 'seq'
               )
