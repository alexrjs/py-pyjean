"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from datetime import datetime

from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import register, registercategories, getcolor, skiplegend


__colors__ = {
    'skinparam LegendBackgroundColor': 'yellow',
    'skinparam LegendBorderColor': 'black',
    'skinparam sequenceParticipantBorderColor': 'black',
    'skinparam sequenceActorBorderColor': 'black',
    'skinparam sequenceLifeLineBorderColor': 'black',
}


@rule(__name__)
def execute(show=False):
    """
    Execute header rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    register(registercategories.Color, __colors__)
    code = [
        "' %s" % str(datetime.now()) if not skiplegend else '',
        '@startuml',
        '',
        "' Skin",
        'skin Rose',
    ]
    for key in __colors__:
        code.append('%s %s' % (key, getcolor(key)))

    code.append('')
    return save('\n'.join(code), 'seq', True)
