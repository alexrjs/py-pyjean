"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Publisher
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup, register, registercategories

__colors__ = {
    'Mailer': '#Crimson',
    'Confluence': '#Crimson',
}

__highlighttags__ = [
    'mUrl',
    'recipients',
    'recipientList',
]

__external__ = {
    'Mailer': 'Mailer',
    'BuildTrigger': 'Jenkins',
    'NaginatorPublisher': 'Jenkins',
    'ConfluencePublisher': 'Confluence',
    'ExtendedEmailPublisher': 'Mailer',
    'GroovyPostbuildRecorder': 'Groovy',
}


@rule(__name__)
def execute(show=False):
    """
    Execute publisher rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    register(registercategories.Color, __colors__)
    register(registercategories.Highlights, __highlighttags__)
    participant = 'Publisher'
    code = '\n'.join(fillgroup(__stacks__['parts'], Publisher, participant, 'publish', __external__))
    if code:
        return save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            '',
            ''
        ]), 'seq'
                   )
    else:
        return False
