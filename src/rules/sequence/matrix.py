"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Matrix
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup, register, registercategories

__highlighttags__ = [
    'groovyString',
]

__external__ = {
    'MATRIX': 'Jenkins',
}


@rule(__name__)
def execute(show=False):
    """
    Execute matrix rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    register(registercategories.Highlights, __highlighttags__)
    participant = 'Matrix'
    code = '\n'.join(fillgroup(__stacks__['parts'], Matrix, participant, 'execute', __external__))
    if code:
        return save('\n'.join([
            "' %ses" % participant,
            'group %ses' % participant,
            '',
            code,
            '',
            "end ' %ses" % participant,
            ''
        ]), 'seq'
                   )
    else:
        return False
