"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Builder, PreBuilder, PostBuilder
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup, register, registercategories

__colors__ = {
    'Maven': '#Crimson',
    'Shell': '#Crimson',
    'Command': '#Crimson',
    'Groovy': '#Crimson',
    'PowerShell': '#Crimson',
    'Repo': '#Crimson',
}

__splitnote__ = {
    'BatchFile': ['Builder', 'Command'],
    'PhaseJobConfig': ['Builder', 'Jenkins'],
    'BlockableBuildTriggerConfig': ['Builder', 'Jenkins'],
}

__highlighttags__ = [
    'command',
    'jobName',
    'scriptId',
    'projects',
    'phaseName',
    'scriptFile',
    'configName',
]

__external__ = {
    'Maven': 'Maven',
    'Shell': 'Shell',
    'BatchFile': 'Command',
    'SystemGroovy': 'Groovy',
    'PowerShell': 'PowerShell',
    'ArtifactResolver': 'Repo',
    'MultiJobBuilder': 'Jenkins',
    'ScriptlerBuilder': 'Groovy',
}


@rule(__name__)
def execute(show=False):
    """
    Execute builder rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    ret_value = True
    register(registercategories.Color, __colors__)
    register(registercategories.Highlights, __highlighttags__)
    register(registercategories.Splits, __splitnote__)
    participant = 'PreBuilder'
    code = '\n'.join(fillgroup(__stacks__['parts'], PreBuilder, participant, 'execute', __external__))
    if code:
        save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            '',
            ''
        ]), 'seq'
            )
    else:
        ret_value = False

    participant = 'Builder'
    code = '\n'.join(fillgroup(__stacks__['parts'], Builder, participant, 'execute', __external__))
    if code:
        save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            '',
            ''
        ]), 'seq'
            )
    else:
        ret_value = False

    participant = 'PostBuilder'
    code = '\n'.join(fillgroup(__stacks__['parts'], PostBuilder, participant, 'execute', __external__))
    if code:
        save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            '',
            ''
        ]), 'seq'
            )
    else:
        ret_value = False

    return ret_value
