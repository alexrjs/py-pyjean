"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Dsl
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup


@rule(__name__)
def execute(show=False):
    """
    Execute dsl rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    participant = 'DSL'
    code = '\n'.join(fillgroup(__stacks__['parts'], Dsl, participant, 'use'))
    if code:
        return save('\n'.join([
            "' %s" % participant,
            'group %s' % participant,
            '',
            code,
            "end ' %s" % participant,
            '',
            ''
        ]), 'seq'
                   )
    else:
        return False
