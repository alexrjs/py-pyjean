"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from datetime import datetime
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import skiplegend


@rule(__name__)
def execute(show=False):
    """
    Execute footer rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    code = [
        '',
        "' Legend",
        'legend right',
        '  boxcolor: lightblue = enabled',
        '  boxcolor: lightgrey = disabled',
        '',
        '  created with PlantUML and PyJeAn',
        '  %s' % str(datetime.now()),
        'endlegend',
        '',
    ]
    return save('\n'.join([
        '',
        'deactivate Job',
        '\n'.join(code) if not skiplegend else '',
        '@enduml',
        ''
    ]), 'seq'
               )
