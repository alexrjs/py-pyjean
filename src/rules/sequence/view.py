"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import controller
from model.jenkins.tag import Views
from controller.common.rule import rule, __stacks__
from controller.export.save import save
from controller.export.sequence import fillgroup, fillnote, register, activate, registercategories, \
    deactivate, noteover

__splitnote__ = {
    'ListViewSection': None,
}

__highlighttags__ = [
    'name',
    'jobNames',
    'includeRegex',
    'excludeRegex',
]


def buildnote(element, name, action, participant):
    """
    Build a note
    :rtype : str[]
    :param element:
    :param name:
    :param action:
    :param participant:
    :return:
    """
    note = fillnote(element, 1)
    code = '\n'.join(note) if element.elements.count >= 1 else element.name
    return [
        'Engine->%s : %s( %s )' % (participant, action, name),
        noteover('Engine'),
        code,
        'end note' if not code.strip().endswith('end note') else ''
    ]


def addgroupitem(item, participant, action, external=None):
    """
    Add group item
    :rtype : str[]
    :param item:
    :param participant:
    :param action:
    :param external:
    :return:
    """
    external = external
    code = [
        ' Job->Engine : enable( ' + item.name + ' )',
        activate('Engine'),
    ]

    for element in item.elements:
        controller.export.sequence.endnote = True
        name = element.name.split(' ')
        if element.elements.count > 1:
            code += buildnote(element, name[0], action, participant)
            code.append(activate(participant))
            code.append(' Engine<-%s' % participant)
            code.append(deactivate(participant))
            code.append('')
        else:
            elem = element.elements.peek()
            if isinstance(elem, str):
                code.append(' Engine->%s : %s( %s )' % (participant, name[0], elem))
            elif elem:
                code += buildnote(elem, name[0], action, participant)
            else:
                code.append(' Engine->%s : %s' % (participant, name[0]))

            code.append(activate(participant))
            code.append(' Engine<-%s' % participant)
            code.append(deactivate(participant))
            code.append('')

    code.append('  Job<-Engine')
    code.append(deactivate('Engine'))
    code.append('')

    return code


@rule(__name__)
def execute(show=False):
    """
    Execute view rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    register(registercategories.Highlights, __highlighttags__)
    register(registercategories.Splits, __splitnote__)
    participant = 'View'
    code = '\n'.join(fillgroup(__stacks__['parts'], Views, participant, 'use', None, addgroupitem))
    if code:
        return save('\n'.join([
            "' %ss" % participant,
            'group %ss' % participant,
            '',
            code,
            "end ' %ss" % participant,
            '',
            ''
        ]), 'seq'
                   )
    else:
        return False
