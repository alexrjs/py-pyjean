"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Trigger

__triggertags__ = {
    'hudson.triggers.TimerTrigger': [False, Trigger, {
        'spec': [True, Property, None],
    }],
    'hudson.triggers.SCMTrigger': [False, Trigger, {
        'spec': [True, Property, None],
        'ignorePostCommitHooks': [False, Property, None],
    }],
    'com.cloudbees.jenkins.plugins.BitBucketTrigger': [False, Trigger, {
        'spec': [False, Property, None],
    }],
    'hudson.tasks.BuildTrigger': [False, Trigger, {
        'childProjects': [True, Property, None],
        'threshold': [False, Trigger, {
            'name': [True, Property, None],
            'ordinal': [True, Property, None],
            'color': [True, Property, None],
            'completeBuild': [True, Property, None],
        }],
    }],
    'jenkins.triggers.ReverseBuildTrigger': [False, Trigger, {
        'spec': [True, Property, None],
        'upstreamProjects': [True, Property, None],
        'threshold': [False, Trigger, {
            'name': [True, Property, None],
            'ordinal': [True, Property, None],
            'color': [True, Property, None],
            'completeBuild': [True, Property, None],
        }],
    }],
    'com.progress.hudson.ScheduleFailedBuildsTrigger': [False, Trigger, {
        'spec': [True, Property, None],
    }],
    'hudson.plugins.promoted__builds.PromotionTrigger': [False, Trigger, {
        'spec': [True, Property, None],
        'jobName': [False, Property, None],
        'process': [False, Property, None],
    }],
    'com.sonyericsson.hudson.plugins.gerrit.trigger.hudsontrigger.GerritTrigger': [False, Trigger, {
        'spec': [True, Property, None],
        'gerritProjects': [True, Trigger, {
            'com.sonyericsson.hudson.plugins.gerrit.trigger.hudsontrigger.data.GerritProject': [False, Trigger, {
                'compareType': [False, Property, None],
                'pattern': [False, Property, None],
                'branches': [True, Trigger, {
                    'com.sonyericsson.hudson.plugins.gerrit.trigger.hudsontrigger.data.Branch': [False, Trigger, {
                        'compareType': [False, Property, None],
                        'pattern': [False, Property, None],
                    }],
                }],
            }],
        }],
        'skipVote': [False, Trigger, {
            'onSuccessful': [False, Property, None],
            'onFailed': [False, Property, None],
            'onUnstable': [False, Property, None],
            'onNotBuilt': [False, Property, None],
        }],
        'gerritBuildSuccessfulVerifiedValue': [False, Property, None],
        'gerritBuildSuccessfulCodeReviewValue': [False, Property, None],
        'silentMode': [False, Property, None],
        'escapeQuotes': [False, Property, None],
        'noNameAndEmailParameters': [False, Property, None],
        'buildStartMessage': [True, Property, None],
        'buildFailureMessage': [True, Property, None],
        'buildSuccessfulMessage': [True, Property, None],
        'buildUnstableMessage': [True, Property, None],
        'buildNotBuiltMessage': [True, Property, None],
        'buildUnsuccessfulFilepath': [True, Property, None],
        'customUrl': [True, Property, None],
        'triggerOnEvents': [False, Trigger, {
            'com.sonyericsson.hudson.plugins.gerrit.trigger.hudsontrigger.events.PluginPatchsetCreatedEvent': [
                False, Property, None
            ],
            'com.sonyericsson.hudson.plugins.gerrit.trigger.hudsontrigger.events.PluginDraftPublishedEvent': [
                False, Property, None
            ],
            'com.sonyericsson.hudson.plugins.gerrit.trigger.hudsontrigger.events.PluginRefUpdatedEvent': [
                False, Property, None
            ],
            'com.sonyericsson.hudson.plugins.gerrit.trigger.hudsontrigger.events.PluginChangeMergedEvent': [
                False, Property, None
            ],
        }],
        'dynamicTriggerConfiguration': [False, Property, None],
        'triggerConfigURL': [True, Property, None],
        'triggerInformationAction': [True, Property, None],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract trigger rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    return extract(show, None, 'triggers', __triggertags__)
