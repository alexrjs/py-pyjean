"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Publisher, Trigger

__extratags__ = {
    'email': [False, Property, {
        'recipientProviders': [False, Property, {
            'hudson.plugins.emailext.plugins.recipients.DevelopersRecipientProvider': [False, Property, None],
            'hudson.plugins.emailext.plugins.recipients.ListRecipientProvider': [False, Property, None],
            'hudson.plugins.emailext.plugins.recipients.RequesterRecipientProvider': [False, Property, None],
        }],
        'recipientList': [False, Property, None],
        'subject': [False, Property, None],
        'body': [False, Property, None],
        'sendToDevelopers': [False, Property, None],
        'sendToRequester': [False, Property, None],
        'includeCulprits': [False, Property, None],
        'sendToRecipientList': [False, Property, None],
        'attachmentsPattern': [False, Property, None],
        'attachBuildLog': [False, Property, None],
        'compressBuildLog': [False, Property, None],
        'replyTo': [False, Property, None],
        'contentType': [False, Property, None],
    }],
}

__publishertags__ = {
    'hudson.plugins.statusmonitor.MonitorPublisher': [False, Publisher, None],
    'hudson.plugins.claim.ClaimPublisher': [False, Publisher, None],
    'hudson.plugins.blame__upstream__commiters.BlameUpstreamCommitersPublisher': [False, Publisher, {
        'sendToIndividuals': [False, Property, None],
    }],
    'hudson.plugins.chucknorris.CordellWalkerRecorder': [False, Publisher, {
        'factGenerator': [True, Property, None],
    }],
    'hudson.plugins.fitnesse.FitnesseResultsRecorder': [False, Publisher, {
        'fitnessePathToXmlResultsIn': [False, Property, None],
    }],
    'hudson.plugins.girls.CordellWalkerRecorder': [False, Publisher, {
        'factGenerator': [False, Property, None],
    }],
    'hudson.tasks.test.AggregatedTestResultPublisher': [False, Publisher, {
        'includeFailedBuilds': [False, Property, None],
    }],
    'hudson.tasks.ArtifactArchiver': [False, Publisher, {
        'artifacts': [False, Property, None],
        'latestOnly': [False, Property, None],
        'allowEmptyArchive': [False, Property, None],
        'onlyIfSuccessful': [False, Property, None],
        'fingerprint': [False, Property, None],
        'defaultExcludes': [False, Property, None],
    }],
    'org.jvnet.hudson.plugins.groovypostbuild.GroovyPostbuildRecorder': [False, Publisher, {
        'groovyScript': [False, Property, None],
        'behavior': [False, Property, None],
        'script': [False, Property, {
            'script': [False, Property, None],
            'sandbox': [False, Property, None],
        }],
        'behavior': [False, Property, None],
        'runForMatrixParent': [False, Property, None],
    }],
    'hudson.tasks.Fingerprinter': [False, Publisher, {
        'targets': [False, Property, None],
        'recordBuildArtifacts': [False, Property, None],
    }],
    'com.progress.hudson.ScheduleFailedBuildsPublisher': [False, Publisher, {
        'interval': [False, Property, None],
        'maxRetries': [False, Property, None],
    }],
    'hudson.maven.RedeployPublisher': [False, Publisher, {
        'id': [True, Property, None],
        'url': [False, Property, None],
        'uniqueVersion': [False, Property, None],
        'evenIfUnstable': [False, Property, None],
    }],
    'hudson.tasks.junit.JUnitResultArchiver': [False, Publisher, {
        'testResults': [False, Property, None],
        'keepLongStdio': [False, Property, None],
        'testDataPublishers': [False, Property, {
            'hudson.plugins.claim.ClaimTestDataPublisher': [False, Property, None],
        }],
    }],
    'hudson.tasks.Mailer': [False, Publisher, {
        'recipients': [False, Property, None, True],
        'dontNotifyEveryUnstableBuild': [False, Property, None],
        'sendToIndividuals': [False, Property, None],
    }],
    'hudson.plugins.textfinder.TextFinderPublisher': [False, Publisher, {
        'regexp': [False, Property, None, True],
        'succeedIfFound': [False, Property, None],
        'unstableIfFound': [False, Property, None],
        'alsoCheckConsoleOutput': [False, Property, None],
    }],
    'hudson.plugins.ws__cleanup.WsCleanup': [False, Publisher, {
        'deleteDirs': [False, Property, None],
        'skipWhenFailed': [False, Property, None],
        'notFailBuild': [False, Property, None],
        'cleanupMatrixParent': [False, Property, None],
    }],
    'hudson.plugins.sonar.SonarPublisher': [False, Publisher, {
        'jdk': [False, Property, None],
        'branch': [False, Property, None],
        'language': [False, Property, None],
        'mavenOpts': [False, Property, None],
        'jobAdditionalProperties': [False, Property, None],
    }],
    'hudson.plugins.descriptionsetter.DescriptionSetterPublisher': [False, Publisher, {
        'regexp': [False, Property, None, True],
        'regexpForFailed': [False, Property, None, True],
        'description': [False, Property, None, True],
        'descriptionForFailed': [False, Property, None, True],
        'setForMatrix': [False, Property, None],
    }],
    'com.bmw.jenkins.notifier.BrokerNotifyBuilder': [False, Publisher, {
        'jobName': [False, Property, None],
        'jobCategory': [False, Property, None],
        'jobEnvironment': [False, Property, None],
        'JMS__ADMIN__USER': [False, Property, None],
        'JMS__ADMIN__PASSWORD': [False, Property, None],
    }],
    'ruby-proxy-object': [False, Publisher, {
        'ruby-object': [False, Property, {
            'object': [False, Property, None],
            'pluginid': [False, Property, None],
        }],
    }],
    'hudson.plugins.sitemonitor.SiteMonitorRecorder': [False, Publisher, {
        'mSites': [False, Publisher, {
            'hudson.plugins.sitemonitor.model.Site': [False, Publisher, {
                'mUrl': [False, Property, None],
            }],
        }],
    }],
    'net.masterthought.jenkins.CucumberReportPublisher': [False, Publisher, {
        'jsonReportDirectory': [False, Property, None],
        'skippedFails': [False, Property, None],
        'pendingFails': [False, Property, None],
        'undefinedFails': [False, Property, None],
        'missingFails': [False, Property, None],
        'ignoreFailedTests': [False, Property, None],
        'jenkinsBasePath': [False, Property, None],
        'fileIncludePattern': [False, Property, None],
        'fileExcludePattern': [False, Property, None],
        'parallelTesting': [False, Property, None],
    }],
    'hudson.tasks.BuildTrigger': [False, Trigger, {
        'childProjects': [True, Property, None],
        'threshold': [False, Trigger, {
            'name': [True, Property, None],
            'ordinal': [True, Property, None],
            'color': [True, Property, None],
            'completeBuild': [True, Property, None],
        }],
    }],
    'hudson.plugins.performance.PerformancePublisher': [False, Publisher, {
        'errorFailedThreshold': [False, Property, None],
        'errorUnstableThreshold': [False, Property, None],
        'relativeFailedThresholdPositive': [False, Property, None],
        'relativeFailedThresholdNegative': [False, Property, None],
        'relativeUnstableThresholdPositive': [False, Property, None],
        'relativeUnstableThresholdNegative': [False, Property, None],
        'errorUnstableResponseTimeThreshold': [False, Property, None],
        'modeRelativeThresholds': [False, Property, None],
        'configType': [False, Property, None],
        'modeOfThreshold': [False, Property, None],
        'compareBuildPrevious': [False, Property, None],
        'nthBuildNumber': [False, Property, None],
        'xml': [False, Property, None],
        'modePerformancePerTestCase': [False, Property, None],
        'modeThroughput': [False, Property, None],
        'parsers': [True, Property, {
            'hudson.plugins.performance.JUnitParser': [False, Property, {
                'glob': [False, Property, None],
            }],
            'hudson.plugins.performance.JMeterParser': [False, Property, {
                'glob': [False, Property, None],
            }],
        }],
    }],
    'com.chikli.hudson.plugin.naginator.NaginatorPublisher': [False, Publisher, {
        'regexpForRerun': [True, Property, None],
        'rerunMatrixPart': [False, Property, None],
        'regexpForMatrixStrategy': [False, Property, None],
        'rerunIfUnstable': [False, Property, None],
        'checkRegexp': [False, Property, None],
        'delay': [True, Property, {
            'delay': [False, Property, None],
            'increment': [False, Property, None],
            'max': [False, Property, None],
        }],
        'maxSchedule': [False, Property, None],
    }],
    'htmlpublisher.HtmlPublisher': [False, Publisher, {
        'reportTargets': [True, Property, {
            'htmlpublisher.HtmlPublisherTarget': [False, Property, {
                'reportName': [False, Property, None],
                'reportDir': [False, Property, None],
                'reportFiles': [False, Property, None],
                'keepAll': [False, Property, None],
                'wrapperName': [False, Property, None],
            }],
        }],
    }],
    'hudson.plugins.robot.RobotPublisher': [False, Publisher, {
        'outputPath': [False, Property, None],
        'reportFileName': [False, Property, None],
        'logFileName': [False, Property, None],
        'outputFileName': [False, Property, None],
        'passThreshold': [False, Property, None],
        'unstableThreshold': [False, Property, None],
        'logFileLink': [False, Property, None],
        'otherFiles': [False, Property, {
            'string': [True, Property, None],
        }],
        'onlyCritical': [False, Property, None],
    }],
    'hudson.plugins.git.GitPublisher': [False, Publisher, {
        'configVersion': [False, Property, None],
        'pushMerge': [False, Property, None],
        'pushOnlyIfSuccess': [False, Property, None],
        'forcePush': [False, Property, None],
        'tagsToPush': [False, Property, {
            'hudson.plugins.git.GitPublisher_-TagToPush': [False, Property, {
                'targetRepoName': [False, Property, None],
                'tagName': [False, Property, None],
                'tagMessage': [False, Property, None],
                'createTag': [False, Property, None],
                'updateTag': [False, Property, None],
            }],
        }],
        'branchesToPush': [False, Property, None],
    }],
    'hudson.plugins.jabber.im.transport.JabberPublisher': [False, Publisher, {
        'targets': [True, Property, {
            'hudson.plugins.im.GroupChatIMMessageTarget': [False, Property, {
                'name': [False, Property, None],
                'notificationOnly': [False, Property, None],
            }],
        }],
        'strategy': [False, Property, None],
        'notifyOnBuildStart': [False, Property, None],
        'notifySuspects': [False, Property, None],
        'notifyCulprits': [False, Property, None],
        'notifyFixers': [False, Property, None],
        'notifyUpstreamCommitters': [False, Property, None],
        'buildToChatNotifier': [False, Property, None],
        'matrixMultiplier': [False, Property, None],
    }],
    'hudson.plugins.emailext.ExtendedEmailPublisher': [False, Publisher, {
        'configuredTriggers': [True, Property, {
            'hudson.plugins.emailext.plugins.trigger.PreBuildTrigger': [False, Property, __extratags__],
            'hudson.plugins.emailext.plugins.trigger.FailureTrigger': [False, Property, __extratags__],
            'hudson.plugins.emailext.plugins.trigger.SuccessTrigger': [False, Property, __extratags__],
            'hudson.plugins.emailext.plugins.trigger.UnstableTrigger': [False, Property, __extratags__],
            'hudson.plugins.emailext.plugins.trigger.AlwaysTrigger': [False, Property, __extratags__],
        }],
        'recipientList': [False, Property, None, True],
        'defaultSubject': [False, Property, None, True],
        'defaultContent': [False, Property, None, True],
        'attachmentsPattern': [False, Property, None],
        'presendScript': [False, Property, None],
        'attachBuildLog': [False, Property, None],
        'compressBuildLog': [False, Property, None],
        'replyTo': [False, Property, None],
        'contentType': [False, Property, None],
        'saveOutput': [False, Property, None],
        'matrixTriggerMode': [False, Property, None],
        'postsendScript': [False, Property, None],
        'disabled': [False, Property, None],
    }],
    'hudson.plugins.parameterizedtrigger.BuildTrigger': [False, Publisher, {
        'configs': [True, Property, {
            'hudson.plugins.parameterizedtrigger.BuildTriggerConfig': [False, Property, {
                'configs': [True, Property, {
                    'hudson.plugins.parameterizedtrigger.CurrentBuildParameters': [False, Property, None],
                    'hudson.plugins.parameterizedtrigger.NodeParameters': [False, Property, None],
                    'hudson.plugins.parameterizedtrigger.PredefinedBuildParameters': [False, Property, {
                        'properties': [False, Property, None],
                    }],
                    'hudson.plugins.parameterizedtrigger.FileBuildParameters': [False, Property, {
                        'propertiesFile': [False, Property, None],
                    }],
                }],
                'projects': [False, Property, None],
                'condition': [False, Property, None],
                'triggerWithNoParameters': [False, Property, None],
            }],
        }, True],
    }],
    'com.myyearbook.hudson.plugins.confluence.ConfluencePublisher': [False, Publisher, {
        'siteName': [False, Property, None],
        'attachArchivedArtifacts': [False, Property, None],
        'buildIfUnstable': [False, Property, None],
        'fileSet': [False, Property, None],
        'spaceName': [False, Property, None],
        'pageName': [False, Property, None],
        'editors': [False, Property, {
            'com.myyearbook.hudson.plugins.confluence.wiki.editors.BetweenTokensEditor': [False, Property, {
                'generator': [False, Property, {
                    'text': [False, Property, None],
                }],
                'startMarkerToken': [False, Property, None],
                'endMarkerToken': [False, Property, None],
            }],
        }],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract publisher rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    return extract(show, None, 'publishers', __publishertags__)
