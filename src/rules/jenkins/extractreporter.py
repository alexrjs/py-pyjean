"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Reporter

__handledtags__ = {
    'hudson.maven.reporters.MavenMailer': [False, Reporter, {
        'recipients': [False, Property, None],
        'dontNotifyEveryUnstableBuild': [False, Property, None],
        'sendToIndividuals': [False, Property, None],
        'perModuleEmail': [False, Property, None],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract reporter rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    return extract(show, None, 'reporters', __handledtags__)
