"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Dsl


@rule(__name__)
def execute(show=False):
    """
    Extract Dsl rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    master = Dsl('DSL')
    if extract(show, master, 'dsl', None):
        __stacks__['parts'].put(master)
        return True
    else:
        return False
