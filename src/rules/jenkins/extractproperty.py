"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.jenkins.tag import Property, Properties, Builder
from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from rules.jenkins.extractbuilder import __buildstepthresholdtags__

__paramdefinitiondefualttags__ = {
    'name': [False, Property, None],
    'description': [False, Property, None, True],
    'defaultValue': [False, Property, None, True],
}

__seitenbaudynamictags__ = {
    'name': [False, Property, None],
    'description': [False, Property, None, True],
    'readonlyInputField': [False, Property, None],
    '__uuid': [False, Property, None],
    '__remote': [False, Property, None],
    '__script': [False, Property, None, True],
    '__remoteBaseDirectory': [False, Property, None],
    '__classPath': [False, Property, None],
    '__scriptlerScriptId': [False, Property, None],
    '__localBaseDirectory': [False, Property, {
        'hudson.FilePath': [False, Property, {
            'default': [True, Property, {
                'remote': [False, Property, None],
            }],
            'boolean': [False, Property, None],
        }],
    }],
    '__parameters': [False, Property, {
        'com.seitenbau.jenkins.plugins.dynamicparameter.scriptler.'
        'ScriptlerParameterDefinition_-ScriptParameter': [False, Property, {
            'name': [False, Property, None],
            'value': [False, Property, None],
        }],
    }],
}

__propertytags__ = {
    # common
    'actions': [True, Property, None],
    'keepDependencies': [False, Property, None],
    'assignedNode': [False, Property, None],
    'quietPeriod': [False, Property, None],
    'canRoam': [False, Property, None],
    'blockBuildWhenDownstreamBuilding': [False, Property, None],
    'blockBuildWhenUpstreamBuilding': [False, Property, None],
    'jdk': [False, Property, None],
    'authToken': [False, Property, None],
    'icon': [False, Property, None],
    'concurrentBuild': [False, Property, None],
    'customWorkspace': [False, Property, None],
    'generatedJobName': [False, Property, None],
    'generatedDisplayJobName': [False, Property, None],
    'autoRunJob': [False, Property, None],
    'logRotator': [False, Property, {
        'daysToKeep': [False, Property, None],
        'numToKeep': [False, Property, None],
        'artifactDaysToKeep': [False, Property, None],
        'artifactNumToKeep': [False, Property, None],
    }],

    # properties
    'properties': [False, Properties, {
        'hudson.plugins.disk__usage.DiskUsageProperty': [True, Property, None],
        'org.jvnet.hudson.plugins.shelveproject.ShelveProjectProperty': [True, Property, None],
        'org.jenkinsci.plugins.flow__plugin.FlowProjectProperty': [True, Property, None],
        'hudson.queueSorter.PrioritySorterJobProperty': [False, Property, {
            'priority': [False, Property, None],
        }],
        'jenkins.plugins.jobicon.CustomIconProperty': [False, Property, {
            'iconfile': [False, Property, None],
        }],
        'hudson.security.AuthorizationMatrixProperty': [False, Property, {
            'permission': [False, Property, None],
        }],
        'hudson.plugins.promoted__builds.JobPropertyImpl': [True, Property, {
            'activeProcessNames': [False, Property, {
                'string': [True, Property, None],
            }]
        }],
        'com.sonyericsson.jenkins.plugins.bfa.model.ScannerJobProperty': [False, Property, {
            'doNotScan': [False, Property, None],
        }],
        'com.sonyericsson.rebuild.RebuildSettings': [False, Property, {
            'autoRebuild': [False, Property, None],
        }],
        'hudson.plugins.heavy__job.HeavyJobProperty': [False, Property, {
            'weight': [False, Property, None],
        }],
        'jenkins.advancedqueue.AdvancedQueueSorterJobProperty': [False, Property, {
            'useJobPriority': [False, Property, None],
            'priority': [False, Property, None],
        }],
        'se.diabol.jenkins.pipeline.PipelineProperty': [False, Property, {
            'taskName': [False, Property, None],
            'stageName': [False, Property, None],
        }],
        'com.chikli.hudson.plugin.naginator.NaginatorOptOutProperty': [False, Property, {
            'optOut': [False, Property, None],
        }],
        'jp.ikedam.jenkins.plugins.scoringloadbalancer.preferences.BuildPreferenceJobProperty': [False, Property, {
            'buildPreferenceList': [True, Property, {
                'jp.ikedam.jenkins.plugins.scoringloadbalancer.preferences.BuildPreference': [False, Property, {
                    'labelExpression': [False, Property, None],
                    'preference': [False, Property, None]
                }],
            }],
        }],
        'com.synopsys.arc.jenkinsci.plugins.jobrestrictions.jobs.JobRestrictionProperty': [False, Property, None],
        'jenkins.model.BuildDiscarderProperty': [False, Property, {
            'strategy': [True, Property, {
                'daysToKeep': [False, Property, None],
                'numToKeep': [False, Property, None],
                'artifactDaysToKeep': [False, Property, None],
                'artifactNumToKeep': [False, Property, None],
            }],
        }],
        'hudson.plugins.throttleconcurrents.ThrottleJobProperty': [False, Property, {
            'maxConcurrentPerNode': [False, Property, None],
            'maxConcurrentTotal': [False, Property, None],
            'throttleEnabled': [False, Property, None],
            'throttleOption': [False, Property, None],
            'categories': [False, Property, {
                'string': [True, Property, None],
            }],
            'configVersion': [False, Property, None],
        }],
        'com.cloudbees.hudson.plugins.folder.properties.FolderCredentialsProvider_-FolderCredentialsProperty': [
            False, Property, {
                'domainCredentialsMap': [False, Property, {
                    'entry': [True, Property, {
                        'com.cloudbees.plugins.credentials.domains.Domain': [False, Property, {
                            'specifications': [False, Property, None],
                        }],
                        'java.util.concurrent.CopyOnWriteArrayList': [False, Property, None],
                    }],
                }],
            }
        ],
        'EnvInjectJobProperty': [False, Property, {
            'info': [False, Property, {
                'groovyScriptContent': [False, Property, None],
                'propertiesFilePath': [False, Property, None],
                'propertiesContent': [False, Property, None],
                'loadFilesFromMaster': [False, Property, None],
            }],
            'on': [False, Property, None],
            'keepJenkinsSystemVariables': [False, Property, None],
            'overrideBuildParameters': [False, Property, None],
            'keepBuildVariables': [False, Property, None],
            'contributors': [True, Property, None],
        }],
        'hudson.model.ParametersDefinitionProperty': [False, Property, {
            'parameterDefinitions': [True, Property, {
                'hudson.model.ChoiceParameterDefinition': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None, True],
                    'choices': [False, Property, {
                        'a': [True, Property, {
                            'string': [False, Property, None, True],
                        }],
                    }],
                }],
                'org.jenkinsci.plugins.jobgenerator.parameters.GeneratorKeyValueParameterDefinition': [
                    False, Property, __paramdefinitiondefualttags__
                ],
                'hudson.model.FileParameterDefinition': [
                    False, Property, __paramdefinitiondefualttags__
                ],
                'hudson.model.BooleanParameterDefinition': [
                    False, Property, __paramdefinitiondefualttags__
                ],
                'hudson.model.PasswordParameterDefinition': [
                    False, Property, __paramdefinitiondefualttags__
                ],
                'hudson.model.TextParameterDefinition': [
                    False, Property, __paramdefinitiondefualttags__
                ],
                'hudson.model.StringParameterDefinition': [
                    False, Property, __paramdefinitiondefualttags__
                ],
                'hudson.plugins.copyartifact.BuildSelectorParameter': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None, True],
                    'defaultSelector': [True, Property, None],
                }],
                'hudson.plugins.validating__string__parameter.ValidatingStringParameterDefinition': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None, True],
                    'defaultValue': [False, Property, None],
                    'regex': [False, Property, None],
                    'failedValidationMessage': [False, Property, None],
                }],
                'hpi.ScriptSelectionTaskDefinition': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None, True],
                    'defaultValue': [False, Property, None],
                    'testDelimSymbol': [True, Property, None],
                    'nodeDelimSymbol': [True, Property, None],
                    'path': [False, Property, None],
                    'countDelimiterSymbol': [False, Property, None],
                    'delimiter': [False, Property, None],
                    'deep': [False, Property, None],
                }],
                'org.jvnet.jenkins.plugins.nodelabelparameter.NodeParameterDefinition': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None, True],
                    'allowedSlaves': [False, Property, {
                        'string': [True, Property, None],
                    }],
                    'defaultSlaves': [False, Property, {
                        'string': [True, Property, None],
                    }],
                    'triggerIfResult': [False, Property, None],
                    'allowMultiNodeSelection': [False, Property, None],
                    'triggerConcurrentBuilds': [False, Property, None],
                    'ignoreOfflineNodes': [False, Property, None],
                    'nodeEligibility': [False, Property, None],
                }],
                'com.cwctravel.hudson.plugins.extended__choice__parameter.ExtendedChoiceParameterDefinition': [
                    False, Property, {
                        'name': [False, Property, None],
                        'description': [False, Property, None, True],
                        'quoteValue': [False, Property, None],
                        'visibleItemCount': [False, Property, None],
                        'type': [False, Property, None],
                        'value': [False, Property, None],
                        'propertyFile': [False, Property, None],
                        'propertyKey': [False, Property, None],
                        'defaultValue': [False, Property, None],
                        'defaultPropertyFile': [True, Property, None],
                        'defaultPropertyKey': [True, Property, None],
                        'multiSelectDelimiter': [False, Property, None, True],
                    }],
                'hudson.scm.listtagsparameter.ListSubversionTagsParameterDefinition': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None, True],
                    'tagsDir': [False, Property, None],
                    'tagsFilter': [False, Property, None],
                    'reverseByDate': [False, Property, None],
                    'reverseByName': [False, Property, None],
                    'defaultValue': [True, Property, None],
                    'maxTags': [False, Property, None],
                    'uuid': [False, Property, None],
                }],
                'hudson.model.ChoiceParameterDefinition': [False, Property, {
                    'choices': [False, Property, {
                        'a': [True, Property, {
                            'string': [True, Property, None],
                        }],
                    }],
                    'name': [False, Property, None],
                    'description': [False, Property, None],
                    'projectName': [False, Property, None],
                }],
                'org.biouno.unochoice.ChoiceParameter': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None],
                    'visibleItemCount': [False, Property, None],
                    'randomName': [False, Property, None],
                    'script': [False, Property, {
                        'scriptlerScriptId': [False, Property, None],
                        'parameters': [False, Property, {
                            'entry': [True, Property, {
                                'string': [True, Property, None],
                            }],
                        }],
                    }],
                    'projectName': [False, Property, None],
                    'fallbackScript': [False, Property, None],
                    'filterable': [False, Property, None],
                    'choiceType': [False, Property, None],
                }],
                'org.biouno.unochoice.CascadeChoiceParameter': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None],
                    'randomName': [False, Property, None],
                    'projectName': [False, Property, None],
                    'visibleItemCount': [False, Property, None],
                    'script': [False, Property, {
                        'script': [False, Property, None],
                        'fallbackScript': [False, Property, None],
                    }],
                    'filterable': [False, Property, None],
                    'choiceType': [False, Property, None],
                    'referencedParameters': [False, Property, None],
                    'parameters': [False, Property, {
                        'entry': [True, Property, {
                            'string': [True, Property, None],
                        }],
                    }],
                }],
                'org.biouno.unochoice.DynamicReferenceParameter': [False, Property, {
                    'name': [False, Property, None],
                    'description': [False, Property, None],
                    'visibleItemCount': [False, Property, None],
                    'randomName': [False, Property, None],
                    'script': [False, Property, {
                        'script': [False, Property, None],
                        'fallbackScript': [False, Property, None],
                    }],
                    'projectName': [False, Property, None],
                    'parameters': [False, Property, {
                        'entry': [True, Property, {
                            'string': [True, Property, None],
                        }],
                    }],
                    'referencedParameters': [False, Property, None],
                    'choiceType': [False, Property, None],
                    'omitValueField': [False, Property, None],
                }],
                'com.seitenbau.jenkins.plugins.dynamicparameter.scriptler.ScriptlerChoiceParameterDefinition': [
                    False, Property, __seitenbaudynamictags__
                ],
                'com.seitenbau.jenkins.plugins.dynamicparameter.StringParameterDefinition': [
                    False, Property, __seitenbaudynamictags__
                ],
                'com.seitenbau.jenkins.plugins.dynamicparameter.ChoiceParameterDefinition': [
                    False, Property, __seitenbaudynamictags__
                ],
            }],
        }],
    }],

    # Maven
    'rootModule': [False, Property, {
        'groupId': [False, Property, None],
        'artifactId': [False, Property, None],
    }],
    'rootPOM': [False, Property, None, Builder],
    'goals': [False, Property, None, Builder],
    'defaultGoals': [False, Property, None, Builder],
    'mavenName': [False, Property, None, Builder],
    'mavenOpts': [False, Property, None, Builder],
    'aggregatorStyleBuild': [False, Property, None],
    'incrementalBuild': [False, Property, None, Builder],
    'localRepository': [True, Property, None],
    'perModuleEmail': [False, Property, None],
    'ignoreUpstremChanges': [False, Property, None],
    'usePrivateRepository': [False, Property, None, Builder],
    'archivingDisabled': [False, Property, None, Builder],
    'siteArchivingDisabled': [False, Property, None, Builder],
    'resolveDependencies': [False, Property, None, Builder],
    'fingerprintingDisabled': [False, Property, None, Builder],
    'processPlugins': [False, Property, None, Builder],
    'mavenValidationLevel': [False, Property, None],
    'runHeadless': [False, Property, None, Builder],
    'disableTriggerDownstreamProjects': [False, Property, None, Builder],
    'settings': [True, Property, None],
    'globalSettings': [False, Property, None, Builder],
    'settingConfigId': [False, Property, None, Builder],
    'globalSettingConfigId': [True, Property, None],
    'blockTriggerWhenBuilding': [True, Property, None],
    # 'displayName': [False, Property, None],
    'version': [False, Property, None],
    'packaging': [False, Property, None],
    'relativePath': [False, Property, None],
    'children': [True, Property, None],
    'runPostStepsIfResult': [False, Property, __buildstepthresholdtags__],
    'dependencies': [False, Property, {
        'dependency': [False, Property, {
            'groupId': [False, Property, None],
            'artifactId': [False, Property, None],
            'version': [False, Property, None],
            'plugin': [False, Property, None],
            'parsedVersion': [False, Property, {
                'majorVersion': [False, Property, None],
                'minorVersion': [False, Property, None],
                'incrementalVersion': [False, Property, None],
                'qualifier': [False, Property, None],
                'comparable': [False, Property, {
                    'value': [False, Property, None],
                    'canonical': [False, Property, None, True],
                    'items': [True, Property, {
                        'unserializable-parents': [True, Property, None],
                        'list': [False, Property, {
                            'default': [False, Property, {
                                'size': [False, Property, None],
                            }],
                            'int': [False, Property, None],
                            'org.apache.maven.artifact.versioning.ComparableVersion_-IntegerItem': [False, Property, {
                                'value': [False, Property, None],
                            }],
                            'org.apache.maven.artifact.versioning.ComparableVersion_-StringItem': [False, Property, {
                                'value': [False, Property, None],
                            }],
                        }],
                    }],
                }],
            }],
        }],
    }],

    # Matrix
    'executionStrategy': [False, Property, {
        'runSequentially': [False, Property, None],
        'touchStoneCombinationFilter': [False, Property, None],
        'touchStoneResultCondition': [False, Property, __buildstepthresholdtags__],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract property rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    master = Property('GeneralProperties')
    #parts.put(master)
    __stacks__['parts'].put(master)
    return extract(show, master, None, __propertytags__, False)
