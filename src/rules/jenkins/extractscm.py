"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common import log
from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Scm

__scmtags__ = {
    # Git
    'configVersion': [False, Property, None],
    'userRemoteConfigs': [True, Property, {
        'hudson.plugins.git.UserRemoteConfig': [False, Property, {
            'name': [True, Property, None],
            'refspec': [True, Property, None],
            'url': [False, Property, None],
            'credentialsId': [False, Property, None],
        }],
    }],
    'branches': [True, Property, {
        'hudson.plugins.git.BranchSpec': [False, Property, {
            'name': [False, Property, None],
        }],
    }],
    'localBranch': [False, Property, None],
    'disableSubmodules': [False, Property, None],
    'recursiveSubmodules': [False, Property, None],
    'doGenerateSubmoduleConfigurations': [False, Property, None],
    'authorOrCommitter': [False, Property, None],
    'clean': [False, Property, None],
    'wipeOutWorkspace': [False, Property, None],
    'pruneBranches': [False, Property, None],
    'remotePoll': [False, Property, None],
    'ignoreNotifyCommit': [False, Property, None],
    'useShallowClone': [False, Property, None],
    'buildChooser': [False, Property, {
        'separator': [False, Property, None],
    }],
    'gitTool': [False, Property, None],
    'submoduleCfg': [True, Property, None],
    'relativeTargetDir': [True, Property, None],
    'reference': [True, Property, None],
    'gitConfigName': [True, Property, None],
    'gitConfigEmail': [True, Property, None],
    'skipTag': [False, Property, None],
    'scmName': [True, Property, None],
    'extensions': [False, Property, {
        'hudson.plugins.git.extensions.impl.CleanBeforeCheckout': [False, Property, None],
        'hudson.plugins.git.extensions.impl.SparseCheckoutPaths': [False, Property, {
            'sparseCheckoutPaths': [False, Property, {
                'hudson.plugins.git.extensions.impl.SparseCheckoutPath': [False, Property, {
                    'path': [False, Property, None],
                }],
            }],
        }],
        'hudson.plugins.git.extensions.impl.RelativeTargetDirectory': [False, Property, {
            'relativeTargetDir': [False, Property, None],
        }],
        'hudson.plugins.git.extensions.impl.LocalBranch': [False, Property, {
            'localBranch': [False, Property, None],
        }],
    }],

    # SVN
    'locations': [False, Property, {
        'hudson.scm.SubversionSCM_-ModuleLocation': [False, Property, {
            'remote': [False, Property, None],
            'local': [False, Property, None],
            'credentialsId': [False, Property, None],
            'depthOption': [False, Property, None],
            'ignoreExternalsOption': [False, Property, None],
        }],
    }],
    'browser': [False, Property, {
        'url': [False, Property, None],
        'rootModule': [True, Property, None],
        'spaceName': [True, Property, None],
    }],
    'excludedRevprop': [True, Property, None],
    'excludedCommitMessages': [True, Property, None],
    'workspaceUpdater': [True, Property, None],
    'ignoreDirPropChanges': [False, Property, None],

    # Template
    'projectName': [False, Property, None],

    # Common
    'excludedRegions': [True, Property, None],
    'includedRegions': [True, Property, None],
    'excludedUsers': [True, Property, None],
    'filterChangelog': [False, Property, None],
}


@rule(__name__)
def execute(show=False):
    """
    Extract Scm rule
    :rtype : bool
    :type show: bool
    :param show:
    """

    scm = 'SCM'
    marker = 'scm'
    tag = '<' + marker
    elements = __stacks__['elements']
    parts = __stacks__['parts']
    tags = set(elements)
    not_matched = [a for a in tags if a and a.startswith(tag)]

    if not_matched:
        register = {'NullSCM': 'null', 'SubversionSCM': 'svn', 'GitSCM': 'git'}
        element = not_matched[0]
        for item in register:
            if item in element:
                scm = register[item]
                break
    else:
        log.info(show, '#', __name__, 'Marker %s not found!' % marker)

    master = Scm(scm)
    if extract(show, master, marker, __scmtags__):
        parts.put(master)
        return True
    else:
        return False
