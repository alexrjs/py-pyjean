"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import re

import __main__
from controller.common.rule import rule, __stacks__


@rule(__name__)
def execute():
    """
    Remove xml tag
    :rtype : bool
    :rtype bool
    """
    elements = __stacks__['elements']
    if elements.isempty:
        return False

    element = elements.peek()
    rexml = re.compile('\s*(<\?xml\s*.*\?>)')
    m = rexml.search(element)
    if m:
        elements.get()
        return True

    return False
