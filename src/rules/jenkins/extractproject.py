"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import os
from model.jenkins.tag import Property, Project
from controller.common.rule import rule, __stacks__, __rules__
from controller.jenkins.extract import extract


__projects__ = {
    '<project': ['FreeStyle Project', 'freestyle'],
    '<matrix-project': ['Matrix Project', 'matrix'],
    '<maven2-moduleset': ['Maven2 Project', 'maven2'],
    '<maven2-module-set': ['Maven2 Project', 'maven2-set'],
    '<com.cloudbees.plugins.flow.BuildFlow': ['BuildFlow Project', 'flow'],
    '<com.cloudbees.hudson.plugins.folder.Folder': ['Cloudbee Folder', 'folder'],
    '<org.jenkinsci.plugins.jobgenerator.JobGenerator': ['Generator Project', 'generator'],
    '<com.tikal.jenkins.plugins.multijob.MultiJobProject': ['MultiJob Project', 'multijob'],
}

__projecttags__ = {
    'description': [False, Property, None, True],
    'displayName': [False, Property, None, True],
    'disabled': [False, Property, None],
}


@rule(__name__)
def execute(show=False):
    """
    Extract project rul
    :rtype : bool
    :type show: bool
    :param show:
    """
    elements = __stacks__['elements']
    if elements.isempty:
        return False

    element = elements.peek()
    for project in __projects__:
        if project in element:
            _ = elements.first()
            part = __rules__.project = Project(os.path.basename(__rules__.projectname))
            part.flavour = __projects__[project][0]
            prop = Property('name')
            prop.elements.push(part.name)
            part.elements.push(prop)
            prop = Property('project-type')
            prop.elements.push(__projects__[project][1])
            part.elements.push(prop)
            __stacks__['parts'].put(part)
            _ = elements.last()

            return extract(show, part, None, __projecttags__)
        else:
            continue
    else:
        return False
