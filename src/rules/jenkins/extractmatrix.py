"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Matrix

__matrixtags__ = {
    'hudson.matrix.TextAxis': [False, Matrix, {
        'name': [False, Property, None],
        'values': [False, Property, {
            'string': [True, Property, None],
        }],
    }],
    'hudson.matrix.LabelAxis': [False, Matrix, {
        'name': [False, Property, None],
        'values': [False, Property, {
            'string': [True, Property, None],
        }],
    }],
    'hudson.matrix.JDKAxis': [False, Matrix, {
        'name': [False, Property, None],
        'values': [False, Property, {
            'string': [True, Property, None],
        }],
    }],
    'org.jenkinsci.plugins.GroovyAxis': [False, Matrix, {
        'name': [False, Property, None],
        'values': [False, Property, {
            'string': [True, Property, None],
        }],
        'groovyString': [False, Property, None, True],
        'computedValues': [False, Property, {
            'string': [True, Property, None],
        }],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract Matrix rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    master = Matrix('MATRIX')
    if extract(show, master, 'axes', __matrixtags__):
        __stacks__['parts'].put(master)
        return True
    else:
        return False
