"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Views

__viewcolumnstags__ = {
    'hudson.views.StatusColumn': [False, Property, None],
    'hudson.views.WeatherColumn': [False, Property, None],
    'hudson.views.JobColumn': [False, Property, None],
    'hudson.views.LastSuccessColumn': [False, Property, None],
    'hudson.views.LastDurationColumn': [False, Property, None],
    'hudson.views.LastFailureColumn': [False, Property, None],
    'hudson.views.BuildButtonColumn': [False, Property, None],
    'jenkins.plugins.jobicon.includeRegex': [False, Property, None],
    'jenkins.plugins.jobicon.CustomIconColumn': [False, Property, None],
    'hudson.plugins.jobtype__column.JobTypeColumn': [False, Property, None],
    'hudson.plugins.projectstats.column.NumBuildsColumn': [False, Property, None],
    'com.sonatype.insight.ci.hudson.QualityColumn': [False, Property, None],
    'com.robestone.hudson.compactcolumns.LastSuccessAndFailedColumn': [False, Property, {
        'timeAgoTypeString': [False, Property, None],
    }],
    'com.robestone.hudson.compactcolumns.LastStableAndUnstableColumn': [False, Property, {
        'timeAgoTypeString': [False, Property, None],
    }],
}

__viewscectiontags__ = {
    'owner': [True, Property, None],
    'name': [False, Property, None],
    'title': [False, Property, None],
    'text': [False, Property, None, True],
    'filterExecutors': [False, Property, None],
    'filterQueue': [False, Property, None],
    'properties': [True, Property, None],
    'jobNames': [False, Property, {
        'comparator': [False, Property, None],
        'string': [True, Property, None],
    }],
    'jobFilters': [False, Property, {
        'hudson.views.RegExJobFilter': [False, Property, {
            'includeExcludeTypeString': [False, Property, None],
            'valueTypeString': [False, Property, None],
            'regex': [False, Property, None],
        }],
        'hudson.views.JobStatusFilter': [False, Property, {
            'includeExcludeTypeString': [False, Property, None],
            'unstable': [False, Property, None],
            'failed': [False, Property, None],
            'aborted': [False, Property, None],
            'disabled': [False, Property, None],
            'stable': [False, Property, None],
        }],
    }],
    'noOfDisplayedBuilds': [False, Property, None],
    'buildViewTitle': [False, Property, None],
    'consoleOutputLinkStyle': [False, Property, None],
    'cssUrl': [False, Property, None],
    'triggerOnlyLatestJob': [False, Property, None],
    'alwaysAllowManualTrigger': [False, Property, None],
    'showPipelineParameters': [False, Property, None],
    'showPipelineParametersInHeaders': [False, Property, None],
    'startsWithParameters': [False, Property, None],
    'refreshFrequency': [False, Property, None],
    'showPipelineDefinitionHeader': [False, Property, None],
    'includeRegex': [False, Property, None],
    'width': [False, Property, None],
    'alignment': [False, Property, None],
    'columns': [False, Property, __viewcolumnstags__],
    'recurse': [False, Property, None],
    'style': [False, Property, None],
    'gridBuilder': [False, Property, {
        'firstJob': [False, Property, None],
        'firstJobLink': [False, Property, None],        
    }],
    'config': [False, Property, {
        'order': [False, Property, None],
    }],
}

__viewstags__ = {
    'viewsTabBar': [False, Property, None],
    'primaryView': [False, Property, None],
    'healthMetrics': [False, Property, {
        'com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric': [False, Property, None],
    }],
    'views': [True, Views, {
        'hudson.model.AllView': [False, Property, {
            'owner': [False, Property, None],
            'name': [False, Property, None],
            'filterExecutors': [False, Property, None],
            'filterQueue': [False, Property, None],
            'properties': [False, Property, None],
        }],
        'com.smartcodeltd.jenkinsci.plugins.buildmonitor.BuildMonitorView': [False, Property, __viewscectiontags__],
        'au.com.centrumsystems.hudson.plugin.buildpipeline.BuildPipelineView': [False, Property, __viewscectiontags__],
        'hudson.model.ListView': [False, Property, __viewscectiontags__],
        'hudson.plugins.sectioned__view.SectionedView': [False, Property, {
            'owner': [False, Property, None],
            'name': [False, Property, None],
            'filterExecutors': [False, Property, None],
            'filterQueue': [False, Property, None],
            'properties': [False, Property, None],
            'sections': [False, Property, {
                'hudson.plugins.sectioned__view.ListViewSection': [False, Property, __viewscectiontags__],
                'hudson.plugins.sectioned__view.TextSection': [False, Property, __viewscectiontags__],
            }],
        }],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract view rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    master = Views('VIEWS')
    if extract(show, master, None, __viewstags__, False):
        __stacks__['parts'].put(master)
        return True
    else:
        return False
