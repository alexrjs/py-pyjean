"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Buildwrapper
from rules.jenkins.extractbuilder import __buildertags__

__buildwrappertags__ = {
    'hudson.plugins.sonar.SonarBuildWrapper': [False, Buildwrapper, None],
    'hudson.plugins.timestamper.TimestamperBuildWrapper': [False, Buildwrapper, None],
    'org.jenkinsci.plugins.envinject.EnvInjectListener_-JobSetupEnvironmentWrapper': [False, Buildwrapper, None],
    'hudson.plugins.toolenv.ToolEnvBuildWrapper': [False, Buildwrapper, {
        'vars': [False, Property, None],
    }],
    'org.jenkinsci.plugins.buildnamesetter.BuildNameSetter': [False, Buildwrapper, {
        'template': [False, Property, None, True],
        'runAtStart': [False, Property, None, True],
        'runAtEnd': [False, Property, None, True]
    }],
    'com.michelin.cio.hudson.plugins.maskpasswords.MaskPasswordsBuildWrapper': [False, Buildwrapper, {
        'varPasswordPairs': [False, Property, {
            'varPasswordPair': [False, Property, None, True],
        }],
    }],
    'hudson.plugins.locksandlatches.LockWrapper': [False, Buildwrapper, {
        'locks': [False, Property, {
            'hudson.plugins.locksandlatches.LockWrapper_-LockWaitConfig': [False, Property, {
                'name': [False, Property, None],
            }],
        }],
    }],
    'EnvInjectBuildWrapper': [False, Buildwrapper, {
        'info': [False, Property, {
            'propertiesContent': [False, Property, None],
            'loadFilesFromMaster': [False, Property, None],
        }],
    }],
    'EnvInjectPasswordWrapper': [False, Buildwrapper, {
        'injectGlobalPasswords': [False, Property, None],
        'maskPasswordParameters': [False, Property, None],
        'passwordEntries': [True, Property, {
            'EnvInjectPasswordEntry': [False, Property, {
                'name': [False, Property, None],
                'value': [False, Property, None, True],
            }],
        }],
    }],
    'hpi.CopyDataToWorkspacePlugin': [False, Buildwrapper, {
        'folderPath': [False, Property, None],
        'makeFilesExecutable': [False, Property, None],
        'deleteFilesAfterBuild': [False, Property, None],
        'copiedFiles': [False, Property, {
            'string': [True, Property, None],
        }],
    }],
    'hudson.plugins.build__timeout.BuildTimeoutWrapper': [False, Buildwrapper, {
        'timeoutMinutes': [False, Property, None],
        'failBuild': [False, Property, None],
        'writingDescription': [False, Property, None],
        'timeoutPercentage': [False, Property, None],
        'timeoutType': [False, Property, None],
        'timeoutMinutesElasticDefault': [False, Property, None],
    }],
    'hudson.plugins.ws__cleanup.PreBuildCleanup': [False, Buildwrapper, {
        'patterns': [False, Property, {
            'hudson.plugins.ws__cleanup.Pattern': [False, Property, {
                'pattern': [False, Property, None],
                'type': [False, Property, None],
            }],
        }],
        'deleteDirs': [False, Property, None],
        'cleanupParameter': [True, Property, None],
        'externalDelete': [True, Property, None],
    }],
    'hudson.plugins.release.ReleaseWrapper': [False, Buildwrapper, {
        'releaseVersionTemplate': [True, Property, None],
        'doNotKeepLog': [False, Property, None],
        'overrideBuildParameters': [False, Property, None],
        'parameterDefinitions': [True, Property, {
            'hudson.model.StringParameterDefinition': [False, Property, {
                'name': [False, Property, None],
                'description': [False, Property, None, True],
                'defaultValue': [False, Property, None],
            }],
        }],
        'preBuildSteps': [False, Property, __buildertags__],
        'postBuildSteps': [False, Property, __buildertags__],
        'postSuccessfulBuildSteps': [True, Property, None],
        'postFailedBuildSteps': [True, Property, None],
        'preMatrixBuildSteps': [True, Property, None],
        'postSuccessfulMatrixBuildSteps': [True, Property, None],
        'postFailedMatrixBuildSteps': [True, Property, None],
        'postMatrixBuildSteps': [True, Property, None],
    }],
    'hudson.plugins.m2extrasteps.M2ExtraStepsWrapper': [False, Buildwrapper, {
        'preBuildSteps': [False, Buildwrapper, __buildertags__],
        'postBuildSteps': [False, Buildwrapper, __buildertags__],
        'runIfResult': [False, Property, None],
    }],
    'hudson.plugins.m2__repo__reaper.M2RepoReaperWrapper': [False, Buildwrapper, {
        'artifactPatterns': [False, Property, None],
        'patterns': [False, Property, {
            'string': [True, Property, None],
        }],
    }],
    'org.jvnet.hudson.plugins.m2release.M2ReleaseBuildWrapper': [False, Buildwrapper, {
        'scmUserEnvVar': [False, Property, None],
        'scmPasswordEnvVar': [False, Property, None],
        'releaseEnvVar': [False, Property, None],
        'releaseGoals': [False, Property, None],
        'numberOfReleaseBuildsToKeep': [False, Property, None],
        'dryRunGoals': [False, Property, None],
        'selectCustomScmCommentPrefix': [False, Property, None],
        'selectAppendHudsonUsername': [False, Property, None],
        'selectScmCredentials': [False, Property, None],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract buildwrapper rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    return extract(show, None, 'buildWrappers', __buildwrappertags__)
