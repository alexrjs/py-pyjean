"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__
from controller.jenkins.extract import extract
from model.jenkins.tag import Property, Builder, PreBuilder, PostBuilder

__buildstepthresholdtags__ = {
    'name': [False, Property, None],
    'ordinal': [False, Property, None],
    'color': [False, Property, None],
    'completeBuild': [False, Property, None],
}

__triggerconfigtags__ = {
    'hudson.plugins.parameterizedtrigger.NodeParameters': [False, Property, None],
    'hudson.plugins.parameterizedtrigger.CurrentBuildParameters': [False, Property, None],
    'org.jenkinsci.plugins.jobgenerator.parameterizedtrigger.GeneratorCurrentParameters': [False, Property, None],
    'org.jenkinsci.plugins.jobgenerator.parameterizedtrigger.PredefinedGeneratorParameters': [False, Property, {
        'properties': [False, Property, None],
    }],
    'hudson.plugins.parameterizedtrigger.PredefinedBuildParameters': [False, Property, {
        'properties': [False, Property, None],
    }],
    'hudson.plugins.parameterizedtrigger.FileBuildParameters': [False, Property, {
        'propertiesFile': [False, Property, None],
    }],
}

__buildsteptags__ = {
    'hudson.plugins.parameterizedtrigger.BlockableBuildTriggerConfig': [False, Property, {
        'configs': [True, Property, __triggerconfigtags__],
        'projects': [False, Property, None],
        'condition': [False, Property, None],
        'triggerWithNoParameters': [False, Property, None],
        'block': [False, Property, {
            'buildStepFailureThreshold': [False, Builder, __buildstepthresholdtags__],
            'unstableThreshold': [False, Builder, __buildstepthresholdtags__],
            'failureThreshold': [False, Builder, __buildstepthresholdtags__],
        }],
        'buildAllNodesWithLabel': [False, Property, None],
    }],
}

__buildertags__ = {
    # Shell
    'hudson.tasks.Shell': [False, Builder, {
        'command': [False, Property, None, True],
    }],

    # PowerShell
    'hudson.plugins.powershell.PowerShell': [False, Builder, {
        'command': [False, Property, None, True],
    }],

    # BatchFile
    'hudson.tasks.BatchFile': [False, Builder, {
        'command': [False, Property, None, True],
    }],

    # TriggerBuilder
    'hudson.plugins.parameterizedtrigger.TriggerBuilder': [False, Builder, {
        'configs': [True, Property, __buildsteptags__, True],
    }],

    # SSHBuilder
    'org.jvnet.hudson.plugins.SSHBuilder': [False, Builder, {
        'siteName': [False, Property, None],
        'command': [False, Property, None],
    }],

    # Ant
    'hudson.tasks.Ant': [False, Builder, {
        'targets': [False, Property, None, True],
        'antName': [False, Property, None],
        'antOpts': [False, Property, None],
        'buildFile': [False, Property, None],
        'properties': [False, Property, None],
    }],

    # Dsl
    'javaposse.jobdsl.plugin.ExecuteDslScripts': [False, Builder, {
        'targets': [False, Property, None, True],
        'usingScriptText': [False, Property, None],
        'scriptText': [False, Property, None],
        'ignoreExisting': [False, Property, None],
        'ignoreMissingFiles': [False, Property, None],
        'failOnMissingPlugin': [False, Property, None],
        'unstableOnDeprecation': [False, Property, None],
        'removedJobAction': [False, Property, None],
        'removedViewAction': [False, Property, None],
        'lookupStrategy': [False, Property, None],
        'additionalClasspath': [False, Property, None],
    }],

    # Maven
    'hudson.tasks.Maven': [False, Builder, {
        'targets': [False, Property, None, True],
        'mavenName': [False, Property, None],
        'jvmOptions': [False, Property, None],
        'pom': [False, Property, None],
        'usePrivateRepository': [False, Property, None],
        'properties': [False, Property, None],
        'injectBuildVariables': [False, Property, None],
        'settings': [True, Property, None],
        'globalSettings': [True, Property, None],
    }],

    # EnvInjectBuilder
    'EnvInjectBuilder': [False, Builder, {
        'info': [False, Property, {
            'propertiesFilePath': [False, Property, None],
            'propertiesContent': [False, Property, None],
        }],
    }],

    # SystemGroovy
    'hudson.plugins.groovy.SystemGroovy': [False, Builder, {
        'scriptSource': [False, Property, {
            'scriptFile': [False, Property, None],
            'command': [False, Property, None, True],
        }],
        'bindings': [False, Property, None],
        'classpath': [True, Property, None],
    }],

    # Groovy
    'hudson.plugins.groovy.Groovy': [False, Builder, {
        'scriptSource': [False, Property, {
            'scriptFile': [False, Property, None],
            'command': [False, Property, None, True],
        }],
        'groovyName': [False, Property, None],
        'parameters': [True, Property, None],
        'scriptParameters': [False, Property, None],
        'properties': [True, Property, None],
        'javaOpts': [True, Property, None],
        'classPath': [True, Property, None],
    }],

    # CopyArtifact
    'hudson.plugins.copyartifact.CopyArtifact': [False, Builder, {
        'projectName': [False, Property, None],
        'filter': [False, Property, None],
        'target': [False, Property, None],
        'flatten': [False, Property, None],
        'selector': [False, Property, {
            'stable': [False, Property, None],
            'parameterName': [False, Property, None],
        }],
    }],

    # ArtifactResolver
    'org.jvnet.hudson.plugins.repositoryconnector.ArtifactResolver': [False, Builder, {
        'targetDirectory': [False, Property, None],
        'artifacts': [True, Property, {
            'org.jvnet.hudson.plugins.repositoryconnector.Artifact': [False, Property, {
                'groupId': [False, Property, None],
                'artifactId': [False, Property, None],
                'classifier': [False, Property, None],
                'version': [False, Property, None],
                'extension': [False, Property, None],
                'targetFileName': [False, Property, None],
            }],
        }],
        'failOnError': [False, Property, None],
        'enableRepoLogging': [False, Property, None],
        'snapshotUpdatePolicy': [False, Property, None],
        'releaseUpdatePolicy': [False, Property, None],
        'snapshotChecksumPolicy': [False, Property, None],
        'releaseChecksumPolicy': [False, Property, None],
    }],

    # ScriptlerBuilder
    'org.jenkinsci.plugins.scriptler.builder.ScriptlerBuilder': [False, Builder, {
        'builderId': [False, Property, None],
        'scriptId': [False, Property, None],
        'propagateParams': [False, Property, None],
        'parameters': [True, Builder, {
            'org.jenkinsci.plugins.scriptler.config.Parameter': [False, Property, {
                'name': [False, Property, None],
                'value': [False, Property, None],
            }],
        }],
    }],

    # MultiJobBuilder
    'com.tikal.jenkins.plugins.multijob.MultiJobBuilder': [False, Builder, {
        'continuationCondition': [False, Property, None],
        'phaseName': [False, Property, None, True],
        'phaseJobs': [True, Property, {
            'com.tikal.jenkins.plugins.multijob.PhaseJobsConfig': [False, Property, {
                'jobName': [False, Property, None, True],
                'currParams': [False, Property, None],
                'exposedSCM': [False, Property, None],
                'disableJob': [False, Property, None],
                'parsingRulesPath': [False, Property, None],
                'enableRetryStrategy': [False, Property, None],
                'enableCondition': [False, Property, None],
                'abortAllJob': [False, Property, None],
                'maxRetries': [False, Property, None],
                'disableJob': [False, Property, None],
                'killPhaseOnJobResultCondition': [False, Property, None],
                'buildOnlyIfSCMChanges': [False, Property, None],
                'condition': [False, Property, None],
                'configs': [True, Property, __triggerconfigtags__],
            }],
        }],
    }],

    # SingleConditionalBuilder
    'org.jenkinsci.plugins.conditionalbuildstep.singlestep.SingleConditionalBuilder': [False, Builder, {
        'condition': [False, Property, {
            'expression': [False, Property, None],
            'label': [False, Property, None],
            'token': [False, Property, None, True],
            'arg1': [False, Property, None],
            'arg2': [False, Property, None],
            'ignoreCase': [False, Property, None],
            'command': [False, Property, None, True],
            'worstResult': [False, Builder, __buildstepthresholdtags__],
            'bestResult': [False, Builder, __buildstepthresholdtags__],
        }],
        'buildStep': [False, Property, {
            '__class__': [],  # Special case, extend with __buildertags__, to handle buildsteps
            'configs': [True, Property, __buildsteptags__, True],
            'command': [False, Property, None, True],
        }],
        'runner': [False, Property, None],
    }],

    # SonarRunnerBuilder
    'hudson.plugins.sonar.SonarRunnerBuilder': [False, Builder, {
        'project': [False, Property, None],
        'properties': [False, Property, None],
        'javaOpts': [False, Property, None],
        'additionalArguments': [False, Property, None],
        'jdk': [False, Property, None],
        'task': [False, Property, None],
    }],

    # ConditionalBuilder
    'org.jenkinsci.plugins.conditionalbuildstep.ConditionalBuilder': [False, Builder, {
        'runner': [False, Property, None],
        'runCondition': [False, Property, {
            'token': [False, Property, None, True],
            'file': [False, Property, None],
            'baseDir': [False, Property, None],
            'command': [False, Property, None],
            'conditions': [True, Property, {
                'org.jenkins__ci.plugins.run__condition.logic.ConditionContainer': [False, Property, {
                    'condition': [True, Property, {
                        'token': [False, Property, None, True],
                        'file': [False, Property, None],
                        'baseDir': [False, Property, None],
                    }],
                }],
            }],
            'worstResult': [False, Builder, __buildstepthresholdtags__],
            'bestResult': [False, Builder, __buildstepthresholdtags__],
        }],
        'conditionalbuilders': [True, Property, True],
    }],

    # BapSssh
    'jenkins.plugins.publish__over__ssh.BapSshBuilderPlugin': [False, Builder, {
        'delegate': [True, Property, {
            'consolePrefix': [False, Property, None],
            'delegate': [True, Property, {
                'publishers': [True, Property, {
                    'jenkins.plugins.publish__over__ssh.BapSshPublisher': [False, Property, {
                        'configName': [False, Property, None],
                        'verbose': [False, Property, None],
                        'transfers': [True, Property, {
                            'jenkins.plugins.publish__over__ssh.BapSshTransfer': [False, Property, {
                                'remoteDirectory': [False, Property, None],
                                'sourceFiles': [False, Property, None],
                                'excludes': [True, Property, None],
                                'removePrefix': [True, Property, None],
                                'remoteDirectorySDF': [False, Property, None],
                                'flatten': [False, Property, None],
                                'cleanRemote': [False, Property, None],
                                'noDefaultExcludes': [False, Property, None],
                                'makeEmptyDirs': [False, Property, None],
                                'execCommand': [True, Property, None],
                                'execTimeout': [False, Property, None],
                                'usePty': [False, Property, None],
                            }],
                        }],
                        'useWorkspaceInPromotion': [False, Property, None],
                        'usePromotionTimestamp': [False, Property, None],
                    }],
                }],
                'continueOnError': [False, Property, None],
                'failOnError': [False, Property, None],
                'alwaysPublishFromMaster': [False, Property, None],
                'hostConfigurationAccess': [True, Property, None],
            }],
        }],
    }],

    # BapFtpBuilder
    'jenkins.plugins.publish__over__ftp.BapFtpBuilder': [False, Builder, {
        'delegate': [False, Property, {
            'consolePrefix': [False, Property, None],
            'delegate': [False, Property, {
                'publishers': [False, Property, {
                    'jenkins.plugins.publish__over__ftp.BapFtpPublisher': [False, Property, {
                        'configName': [False, Property, None, True],
                        'verbose': [False, Property, None],
                        'transfers': [False, Property, {
                            'jenkins.plugins.publish__over__ftp.BapFtpTransfer': [False, Property, {
                                'remoteDirectory': [False, Property, None],
                                'sourceFiles': [False, Property, None],
                                'excludes': [False, Property, None],
                                'removePrefix': [False, Property, None],
                                'remoteDirectorySDF': [False, Property, None],
                                'flatten': [False, Property, None],
                                'cleanRemote': [False, Property, None],
                                'noDefaultExcludes': [False, Property, None],
                                'makeEmptyDirs': [False, Property, None],
                                'patternSeparator': [False, Property, None, True],
                                'asciiMode': [False, Property, None],
                            }],
                        }],
                        'useWorkspaceInPromotion': [False, Property, None],
                        'usePromotionTimestamp': [False, Property, None],
                        'label': [True, Property, {
                            'label': [False, Property, None],
                        }],
                    }],
                }],
                'continueOnError': [False, Property, None],
                'failOnError': [False, Property, None],
                'alwaysPublishFromMaster': [False, Property, None],
                'hostConfigurationAccess': [True, Property, None],
            }],
        }],
    }],

    # Gradle
    'hudson.plugins.gradle.Gradle': [False, Builder, {
        'description': [False, Property, None],
        'rootBuildScriptDir': [False, Property, None],
        'switches': [False, Property, None],
        'tasks': [False, Property, None],
        'buildFile': [False, Property, None],
        'gradleName': [False, Property, None],
        'useWrapper': [False, Property, None],
        'makeExecutable': [False, Property, None],
        'fromRootBuildScriptDir': [False, Property, None],
        'useWorkspaceAsHome': [False, Property, None],
    }],
}


@rule(__name__)
def execute(show=False):
    """
    Extract builders rule
    :rtype : bool
    :type show: bool
    :param show:
    """

    def replacetype(value, old_tag, new_tag):
        if value[1] == old_tag:
            value[1] = new_tag
        return value

    rvalue = False
    mapped_buildertags = dict(map(lambda (k, v): (k, replacetype(v, PostBuilder, PreBuilder)), __buildertags__.iteritems()))
    mapped_buildertags = dict(map(lambda (k, v): (k, replacetype(v, Builder, PreBuilder)), __buildertags__.iteritems()))
    rvalue = extract(show, None, 'prebuilders', mapped_buildertags) or rvalue

    mapped_buildertags = dict(map(lambda (k, v): (k, replacetype(v, PreBuilder, Builder)), __buildertags__.iteritems()))
    rvalue = extract(show, None, 'builders', mapped_buildertags) or rvalue

    mapped_buildertags = dict(map(lambda (k, v): (k, replacetype(v, Builder, PostBuilder)), __buildertags__.iteritems()))
    rvalue = extract(show, None, 'postbuilders', mapped_buildertags) or rvalue

    return rvalue
