"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import __main__
from controller.common.log import info
from controller.common.rule import rule, __stacks__


@rule(__name__)
def execute(show=False):
    """
    Push element rule
    """
    elements = __stacks__['elements']
    lines = __stacks__['lines']
    while not lines.isempty:
        line = lines.first()
        info(show, '-', __name__, 'To be handled: %s' % line)
        elements.put(line)

    tags = __stacks__['tags']
    while not tags.isempty:
        tag = tags.pop().strip()
        info(show, '-', __name__, 'To be handled: %s' % tag)
        elements.put(tag)
