"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import __main__
from controller.common.rule import rule, __stacks__
from controller.common.log import info


@rule(__name__)
def execute():
    """
    Push tag rule
    :rtype : bool
    """

    chars = __stacks__['chars']
    tags = __stacks__['tags']
    show = False
    if not chars.isempty and chars.peek() == '>':
        tag = ''
        zeichen = chars.pop()
        while not chars.isempty and zeichen != '<':
            tag = str(zeichen) + tag
            zeichen = chars.pop()

        tag = '<' + tag
        tags.push(tag.strip())
        info(show, '#', __name__, 'Found tag = %s' % tag)
        return True
    else:
        return False
