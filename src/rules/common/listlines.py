"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import __main__
from controller.common.rule import rule, __stacks__


@rule(__name__)
def execute():
    """
    Listline rule
    """
    lines = __stacks__['lines']
    while not lines.isempty:
        print lines.first()
