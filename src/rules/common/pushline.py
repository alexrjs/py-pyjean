"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import __main__
from controller.common.rule import rule, __stacks__


@rule(__name__)
def execute(ncr=True):
    """
    Push line rule
    :type ncr: bool
    :param ncr:
    :rtype : None
    """

    chars = __stacks__['chars']
    lines = __stacks__['lines']
    if chars.isempty:
        return

    # if needs caridge return and last char is no caridge return, then return
    if ncr and chars.peek() != '\n':
        return

    line = ''
    if str(chars.peek()) == '\n':
        chars.pop()
    while not chars.isempty:
        line = str(chars.pop()) + line

    if line != ' ':
        line = line.strip()

    if len(line) > 0:
        lines.push(line)
