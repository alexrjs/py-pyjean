"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import __main__
from controller.common.rule import rule, __stacks__
from controller.common.log import info

ignoretags = [
    # 'properties',
    # 'org.jvnet.hudson.plugins.shelveproject.ShelveProjectProperty',
    # 'triggers',
    # 'buildWrappers',
    # 'prebuilders',
    # 'postbuilders',
    # 'builders',
    # 'reporters',
    # 'publishers',
    # 'hudson.plugins.parameterizedtrigger.TriggerBuilder',
    # 'configs',
    # 'hudson.plugins.disk__usage.DiskUsageProperty',
    # 'project',
    # 'maven2-moduleset',
    # 'hudson.plugins.ws__cleanup.PreBuildCleanup',
    # 'hudson.tasks.Maven'
]


@rule(__name__)
def execute():
    """
    Pop tag rule
    """
    tags = __stacks__['tags']
    if tags.isempty:
        return

    if not tags.isempty:
        tag = tags.peek().strip()
        for checktag in ignoretags:
            if checktag in tag:
                info(False, '-', __name__, 'Ignored: %s' % checktag)
                tags.pop()
                break
        else:
            info(True, '-', __name__, 'To be handled: %s' % tags.pop())
