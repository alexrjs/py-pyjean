"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.rule import rule, __stacks__


@rule(__name__)
def execute():
    """
    Pop element rule
    """
    unhandled = []
    elements = __stacks__['elements']
    while not elements.isempty:
        element = elements.get()
        if not element:
            continue

        unhandled.append(element.strip())

    if len(unhandled) > 0:
        exit(1)

    return True
