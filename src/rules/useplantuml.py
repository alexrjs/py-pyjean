"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import time
import os
from controller.common.log import view
from controller.common.rule import rule, __stacks__, __rules__, __stacks__
from controller.export import save


@rule(__name__)
def execute(show=True):
    """
    Execute plantuml rule
    :rtype : bool
    :type show: bool
    :param show:
    """

    if not __stacks__['parts'].isempty:
        return

    picformat = 'png'
    exportfile = '%s.seq' % __rules__.projectfilename
    if save.__outfolder__:
        file_name = os.path.basename(exportfile)
        file_path = os.path.dirname(exportfile)
        if os.path.exists(save.__outfolder__):
            file_path = os.path.join(save.__outfolder__, __rules__.projectpath)
        else:
            file_path = os.path.join(file_path, save.__outfolder__)
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        exportfile = os.path.join(file_path, file_name)

    start = time.time()
    if show:
        view('.', 'Execute: java -splash:no -DPLANTUML_LIMIT_SIZE=8192 -jar libs/plantuml.jar -t%s %s' % (picformat, exportfile.replace('$', '\\$')))

    os.system("java -jar libs/plantuml.jar -t%s %s" % (picformat, exportfile.replace('$', '\\$')))

    ende = time.time()
    if show:
        view('.', 'Executed in %s s' % (ende - start))

    if not os.stat(exportfile).st_size:
        view('!!!', 'File %s is empty. Problem!' % exportfile)
        exit(1)
