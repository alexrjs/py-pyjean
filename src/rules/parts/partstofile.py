"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import os
from model.jenkins.tag import Property, Matrix, Project, Publisher, Builder, Buildwrapper
from controller.common.rule import rule, __stacks__, __rules__
from controller.export.save import __outfolder__

__batchfilecount__ = {}


def dumppart(file_pointer, part, prefix=None, istop=False, islist=0):
    """
    Dump parts to file
    :param f:
    :param part:
    :param prefix:
    :param istop:
    :param islist:
    """
    if prefix and istop:
        file_pointer.write('[%s::%s]\n' % (prefix, part.name))
        prefix = ''
    elif prefix and islist > 0:
        if islist > 0:
            file_pointer.write('%s%s::%s::[\n' % ('  ' * (islist - 1), prefix, part.name))
        else:
            file_pointer.write('%s::%s::[\n' % (prefix, part.name))
    elif prefix:
        if islist > 0:
            file_pointer.write('%s%s::%s\n' % ('  ' * islist, prefix, part.name))
        else:
            file_pointer.write('%s::%s\n' % (prefix, part.name))
    else:
        prefix = part.name

    while not part.elements.isempty:
        element = part.elements.first()
        name = element.name if not isinstance(element, str) else element
        if isinstance(element, str):
            if islist > 0:
                file_pointer.write('%s%s\n' % ('  ' * islist, name))
            else:
                items = name.split('\n')
                if len(items) > 1:
                    file_pointer.write('[%s]\n' % name)
                else:
                    file_pointer.write('%s\n' % name)
        elif element.elements.count == 0:
            if islist > 0:
                file_pointer.write('%s%s:\n' % ('  ' * islist, name))
            else:
                file_pointer.write('%s:\n' % name)
        elif element.elements.count == 1:
            item = element.elements.first()
            if isinstance(item, str):
                if name in ['command', 'groovyString']:
                    if name not in __batchfilecount__:
                        __batchfilecount__[name] = 0
                    __batchfilecount__[name] += 1
                    batchfile = file_pointer.name + '.' + name + '.' + str(__batchfilecount__[name])
                    tfile_pointer = open(batchfile, 'w')
                    tfile_pointer.write(item)
                    tfile_pointer.close()
                    if prefix:
                        file_pointer.write('%s::%s: !include(%s)\n' % (prefix, name, batchfile))
                    else:
                        file_pointer.write('%s: !include(%s)\n' % (name, batchfile))
                else:
                    if prefix:
                        if islist > 0:
                            file_pointer.write('%s%s::%s: %s\n' % ('  ' * islist, prefix, name, item))
                        else:
                            file_pointer.write('%s::%s: %s\n' % (prefix, name, item))
                    else:
                        if islist > 0:
                            file_pointer.write('%s%s: %s\n' % ('  ' * islist, name, item))
                        else:
                            file_pointer.write('%s: %s\n' % (name, item))
            else:
                if name in ['condition (BatchFileCondition)']:
                    if 'command' not in __batchfilecount__:
                        __batchfilecount__['command'] = 0
                    __batchfilecount__['command'] += 1
                    batchfile = file_pointer.name + '.command.' + str(__batchfilecount__['command'])
                    file_pointer_t = open(batchfile, 'w')
                    string = item.elements.first()
                    file_pointer_t.write(string)
                    file_pointer_t.close()
                    if prefix:
                        file_pointer.write('%s::%s: !include(%s)' % (prefix, name, batchfile))
                    else:
                        file_pointer.write('%s: !include(%s)' % (name, batchfile))
                else:
                    file_pointer.write('%s:\n' % name)
                    dumppart(file_pointer, item)
        elif element.elements.count > 1:
            if prefix:
                dumppart(file_pointer, element, prefix, False, islist + 1)
                if islist > 0:
                    file_pointer.write('%s]\n' % '  ' * islist)
                else:
                    file_pointer.write(']\n')
            else:
                dumppart(file_pointer, element)
        else:
            if islist > 0:
                file_pointer.write('%s%s:\n' % ('  ' * islist, name))
            else:
                file_pointer.write('%s:\n' % name)
            dumppart(file_pointer, element, part.name)

    if islist == 0:
        file_pointer.write('\n')


def dumpelements(file_pointer, element):
    while not element.elements.isempty:
        item = element.elements.first()
        name = item.name if item is not isinstance(str) else item
        file_pointer.write('%s\n' % name)
        if item is not isinstance(str):
            dumpelements(file_pointer, item)


@rule(__name__)
def execute(show=False):
    """
    Dump parts rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    show = show
    parts = __stacks__['parts']
    unhandled = __stacks__['unhandled']
    if parts.isempty:
        return

    extension = 'part'
    if __outfolder__:
        file_name = os.path.basename(__rules__.projectname)
        file_path = os.path.dirname(__rules__.projectname)
        file_path = os.path.join(file_path, __outfolder__)
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        file_name = '%s.%s' % (file_name, extension)
        file_name = os.path.join(file_path, file_name)
    else:
        file_name = '%s.%s' % (__rules__.projectname, extension)

    file_pointer = open(file_name, 'w')

    for part in [p for p in parts if isinstance(p, Project)]: # filter(lambda p: type(p) is Project, parts):
        dumppart(file_pointer, part, 'Project', True)
        parts.remove(part)
    file_pointer.write('\n')

    for part in [p for p in parts if isinstance(p, Property)]: #  filter(lambda p: type(p) is Property, parts):
        dumppart(file_pointer, part, 'Property', True)
        parts.remove(part)
    file_pointer.write('\n')

    for part in [p for p in parts if isinstance(p, Matrix)]: #  filter(lambda p: type(p) is Matrix, parts):
        dumppart(file_pointer, part, 'Matrix', True)
        parts.remove(part)
    file_pointer.write('\n')

    for part in [p for p in parts if isinstance(p, Buildwrapper)]: #  filter(lambda p: type(p) is Buildwrapper, parts):
        dumppart(file_pointer, part, 'Buildwrapper', True)
        parts.remove(part)
    file_pointer.write('\n')

    for part in [p for p in parts if isinstance(p, Builder)]: #  filter(lambda p: type(p) is Builder, parts):
        dumppart(file_pointer, part, 'Builder', True)
        parts.remove(part)
    file_pointer.write('\n')

    for part in [p for p in parts if isinstance(p, Publisher)]: #  filter(lambda p: type(p) is Publisher, parts):
        dumppart(file_pointer, part, 'Publisher', True)
        parts.remove(part)
    file_pointer.write('\n')

    if not unhandled.isempty:
        file_pointer.write('Dump unhandled\n')
        while not unhandled.isempty:
            part = unhandled.first()
            if not part:
                continue

            name = part.name if not isinstance(part, str) else part
            file_pointer.write('%s Unhandled (%s): %s\n' % ('!!', part.__class__.__name__, name))

    file_pointer.close()

    return True
