"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import os
from model.jenkins.tag import Property, Matrix, Project
from controller.common.log import view
from controller.common.rule import rule, __stacks__, __rules__
from controller.export.save import __outfolder__
from controller.jenkins.extract import extract


def dumpelements(file_pointer, element, level):
    """
    Dump elements recursivly
    :rtype : None
    :param f:
    :param element:
    :param level:
    """
    while not element.elements.isempty:
        item = element.elements.first()
        name = item.name if not isinstance(item, str) else item
        file_pointer.write('%s Element (%s): %s\n' % ('.' * level, item.__class__.__name__, name))
        if not isinstance(item, str):
            dumpelements(file_pointer, item, level + 1)


@rule(__name__)
def execute(show=False):
    """
    Dump to file rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    if show:
        show = show

    parts = __stacks__['parts']
    unhandled = __stacks__['unhandled']
    if parts.isempty:
        return

    extension = 'dmp'
    if __outfolder__:
        file_name = os.path.basename(__rules__.projectfilename)
        file_path = os.path.dirname(__rules__.projectfilename)
        file_path = os.path.join(file_path, __outfolder__)
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        file_name = '%s.%s' % (file_name, extension)
        file_name = os.path.join(file_path, file_name)
    else:
        file_name = '%s.%s' % (__rules__.projectfilename, extension)

    file_pointer = open(file_name, 'w')
    file_pointer.write('Dumped parts:\n')
    for art in [Project, Matrix, Property]:
        newline = False
        for part in [p for p in parts if isinstance(p, art)]: # filter(lambda p: isinstance(p, art), parts):
            file_pointer.write('%s Part (%s): %s\n' % ('.', part.__class__.__name__, part.name))
            dumpelements(file_pointer, part, 2)
            parts.remove(part)
            newline = True

        if newline:
            file_pointer.write('\n')

    while not parts.isempty:
        part = parts.first()
        if not part:
            continue

        file_pointer.write('%s Part (%s): %s\n' % ('.', part.__class__.__name__, part.name))
        while not part.elements.isempty:
            element = part.elements.first()
            name = element.name if not isinstance(element, str) else element
            file_pointer.write('%s Element (%s): %s\n' % ('.' * 2, element.__class__.__name__, name))
            if not isinstance(element, str):
                dumpelements(file_pointer, element, 3)

        file_pointer.write('\n')

    if not unhandled.isempty:
        view('.', '! Unhandled Dump unhandled from %s\n' % file_name)
        file_pointer.write('Dump unhandled\n')
        while not unhandled.isempty:
            part = unhandled.first()
            if not part:
                continue

            name = part.name if not isinstance(part, str) else part
            out = '%s Unhandled (%s): %s\n' % ('!', part.__class__.__name__, name)
            view('.', out)
            file_pointer.write(out)

    file_pointer.close()

    return True
