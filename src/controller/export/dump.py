"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from controller.common.log import view


def dump(yaml, file_name, newfile=False, show=False):
    """
    :type yaml: []
    :param yaml:
    :type file_name: str
    :param file_name:
    :type newfile: bool
    :param newfile:
    :type show: bool
    :param show:
    :rtype : bool
    """
    if show:
        view('*', 'Filename: %s' % file_name)

    file_mode = 'w' if newfile else 'a'
    with open(file_name, file_mode) as file_pointer:
        for item in yaml:
            file_pointer.write('%s\n' % item)

    return True
