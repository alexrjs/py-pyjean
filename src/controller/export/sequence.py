"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.enum import enum

endnote = True
colors = {
    'default': '#White',
    'enabled': '#Lightblue',
    'disabled': '#Lightgrey',
    'note': '#LemonChiffon',
    'execute': '#Crimson',
    'participant': '#SpringGreen',
    'Job': '#FFBBBB',
    'Matrix': '#SpringGreen',
    'Engine': '#RoyalBlue',
    'Trigger': '#Red',
    'Jenkins': '#Crimson',
    'External': '#Red',
    'Builder': '#SpringGreen',
    'Publisher': '#SpringGreen',
    'Reporter': '#SpringGreen',
    'Buildwrapper': '#SpringGreen',
    'DSL': '#SpringGreen',
    'SCM': '#Gold',
}
splits = {}
highlights = []
registercategories = enum('Color', 'Highlights', 'Splits')
skiplegend = False


def register(that, this):
    """
    Registers funciton for colors, Highlights and Splits
    :rtype : None
    :param that:
    :param this:
    :raise Exception:
    """
    if that is registercategories.Color:
        if this:
            colors.update(this)
    elif that is registercategories.Highlights:
        if this:
            for highlight in this:
                if not highlight in highlights:
                    highlights.append(highlight)
    elif that is registercategories.Splits:
        if this:
            splits.update(this)
    else:
        raise Exception('Error: Unknown registry category!')


def getcolor(key, default='default'):
    """
    Get colors
    :rtype : color
    :param key:
    :param default:
    :return:
    """
    if key:
        return colors.get(key, colors.get(default))
    else:
        return colors.get(default)


def activate(what, color='default'):
    """
    Retuns activate string
    :rtype : str
    :param what:
    :param color:
    :return:
    """
    return ' activate %s %s' % (what, getcolor(what, color))


def deactivate(what):
    """
    Returns deactivate string
    :rtype : str
    :param what:
    :return:
    """
    return ' deactivate %s' % what


def highlightcode(item, level, value=''):
    """
    Returns highlight code
    :rtype : str[]
    :param item:
    :param level:
    :param value:
    :return:
    """
    if item in highlights:
        if value:
            return ['%s<b>%s : %s</b>' % (' ' * level, item, value)]
        else:
            return ['%s<b>%s</b>' % (' ' * level, item)]
    else:
        if value:
            return ['%s%s : %s' % (' ' * level, item, value)]
        else:
            return ['%s%s' % (' ' * level, item)]


def noteright(what, color='note'):
    """
    Returns note for right
    :rtype : str
    :param what:
    :param color:
    :return:
    """
    return ' note right of %s %s' % (what, getcolor(color))


def noteleft(what, color='note'):
    """
    Returns note for left
    :rtype : str
    :param what:
    :param color:
    :return:
    """
    return ' note left of %s %s' % (what, getcolor(color))


def noteover(what, color='note'):
    """
    Returns note over string
    :rtype : str
    :param what:
    :param color:
    :return:
    """
    return ' note over %s %s' % (what, getcolor(color))


def dosplitstart(code, item):
    """
    Appends code for splits
    :rtype : None
    :param code:
    :param item:
    """
    global endnote
    if item.name in splits:
        if endnote:
            code.append(' end note ')
            code.append('')
            endnote = False

        if splits[item.name]:
            participant = splits[item.name][0]
            execute = splits[item.name][1]
            code.append(' participant %s %s' % (execute, getcolor(execute)))
            code.append(' create %s' % execute)
            code.append(' %s->%s : delegateTo( %s )' % (participant, execute, execute))
            code.append(deactivate(participant))
            code.append(activate(execute, 'execute'))
            code.append('')
            code.append(noteleft(execute))
        else:
            code.append(noteright('Engine'))


def dosplitend(code, item):
    """
    Appends split end code
    :rtype : None
    :param code:
    :param item:
    """
    if item.name in splits:
        code.append(' end note ')
        code.append('')
        if splits[item.name]:
            participant = splits[item.name][0]
            execute = splits[item.name][1]
            code.append(' %s<-%s' % (participant, execute))
            code.append(deactivate(execute))
            code.append(' destroy %s' % execute)
            code.append(activate(participant, 'participant'))


def fillnote(item, level=0):
    """
    Adds a note with info
    :rtype : str[]
    :param item:
    :param level:
    :return:
    """
    code = []
    global endnote
    if type(item) is str:
        code += highlightcode(item, level)

    elif item.elements.count > 1:
        dosplitstart(code, item)
        code += highlightcode(item.name, level, ' ')

        for element in item.elements:
            if type(element) is str:
                code += highlightcode(element, level)
            elif element.elements.isempty:
                code += highlightcode(element.name, level + 1)
            else:
                code += fillnote(element, level + 1)

        dosplitend(code, item)

    elif item.elements.count == 1:
        element = item.elements.peek()
        if type(element) is str:
            lines = element.split('\n')
            if len(lines) == 1:
                code += highlightcode(item.name, level, lines[0])
            else:
                code += highlightcode(item.name, level, ' ')
                for line in lines:
                    ml = line.replace('#', '\#')
                    if item.name == 'command':
                        ml = ml.replace('[', '\[')
                        ml = ml.replace(']', '\]')

                    code += highlightcode(ml, level + 1)
        else:
            dosplitstart(code, item)
            code += highlightcode(item.name, level, ' ')
            code += fillnote(element, level + 1)
            dosplitend(code, item)

    elif item.elements.count == 0:
        code += highlightcode(item.name, level)

    return code


def addgroupitem(item, participant, action, external):
    """
    Adds code for a group item
    :rtype : str[]
    :param item:
    :param participant:
    :param action:
    :param external:
    :return:
    """
    code = [
        ' Job->Engine : register( %s : %s )' % (participant, item.name),
        activate('Engine'),
        '',
        # ' participant %s %s' % (participant, getcolor(participant)),
        ' create %s' % participant,
        ' Engine->%s : %s( %s )' % (participant, action, item.name),
        deactivate('Engine'),
        activate(participant, 'participant'),
    ]

    global endnote
    endnote = True

    if external and item.name in external:
        execute = external[item.name]
        code.append(' participant %s %s' % (execute, getcolor(execute)))
        code.append(' create %s' % execute)
        code.append(' %s->%s : delegateTo( %s )' % (participant, execute, execute))
        code.append(deactivate(participant))
        code.append(activate(execute, 'execute'))
    else:
        execute = participant

    code.append(noteleft(execute))

    for element in item.elements:
        code += fillnote(element, 2)
        if not endnote:
            code.append(noteleft(execute))
            endnote = True

    if endnote:
        code.append(' end note')
        code.append('')

    if external and item.name in external:
        execute = external[item.name]
        code.append(' %s<-%s' % (participant, execute))
        code.append(deactivate(execute))
        code.append(' destroy %s' % execute)
        code.append(activate(participant, 'participant'))

    code.append(' Engine<-%s' % participant)
    code.append(deactivate(participant))
    code.append(' destroy %s' % participant)
    code.append(activate('Engine'))
    code.append(' Job<-Engine')
    code.append(deactivate('Engine'))
    code.append('')

    return code


def fillgroup(parts, tag, participant, action, external=None, agi=None):
    """
    Fill a group with information
    :rtype : str[]
    :param parts:
    :param tag:
    :param participant:
    :param action:
    :param external:
    :param agi:
    :return:
    """
    code = []
    for part in filter(lambda p: type(p) is tag, parts):
        parts.remove(part)
        if not agi:
            code += addgroupitem(part, participant, action, external)
        else:
            code += agi(part, participant, action, external)

    return code
