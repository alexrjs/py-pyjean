"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import os

from controller.common.log import view
from controller.common.rule import __rules__, __stacks__


__outfolder__ = ''


def save(code, extension, newfile=False, show=False):
    """
    :type code: str
    :param code:
    :type show: bool
    :param show:
    :rtype : bool
    """
    if not __rules__:
        return False

    if not __rules__.projectname:
        return False

    if __outfolder__:
        file_name = os.path.basename(__rules__.projectfilename)
        file_path = os.path.dirname(__rules__.projectfilename)
        if os.path.exists(__outfolder__):
            file_path = os.path.join(__outfolder__, __rules__.projectpath)
        else:
            file_path = os.path.join(file_path, __outfolder__)
        if not os.path.exists(file_path):
            os.makedirs(file_path)

        file_name = '%s.%s' % (file_name, extension)
        file_name = os.path.join(file_path, file_name)
    else:
        file_name = '%s.%s' % (__rules__.projectfilename, extension)

    if show:
        view('*', 'Filename: %s' % file_name)

    file_mode = 'w' if newfile else 'a'
    with open(file_name, file_mode) as file_pointer:
        file_pointer.write(code)

    return True
