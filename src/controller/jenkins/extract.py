"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

from model.stack import Stack
from model.jenkins.tag import Builder
from controller.common.log import info
from controller.common.rule import __stacks__
from model.jenkins.tag import TAG_TYPE_UNKNOWN, TAG_TYPE_OPEN, TAG_TYPE_CLOSE, TAG_TYPE_SINGLE


def splitcomma(element):
    """
    Splits a string by commas
    :param element:
    :type element: str
    :return:
    :rtype : []
    """
    return element.split(',')


def splitcommandline(element):
    """
    Splits a command line be dash
    :param element:
    :type element: str
    :return:
    :rtype : []
    """
    helper = element.replace(' - ', ' ? ').replace(' -', ' #-').replace(' ? ', ' - ')
    return helper.split(' #')


def preparetag(master, element):
    """
    Uses master and element and prepare the tag and other information for further usage
    :param element:
    :type element: str
    :return:
    :rtype : str, int, str, str
    """
    modified = element
    klasse = None
    fullklasse = None
    tagkind = TAG_TYPE_UNKNOWN
    if element.startswith('</'):
        tagkind = TAG_TYPE_CLOSE
        modified = element[+2:-1]
        return modified, modified, tagkind, fullklasse, klasse
    elif element.startswith('<'):
        modified = element[+1:]
        if element.endswith('/>'):
            tagkind = TAG_TYPE_SINGLE
            modified = modified[:-2]
        else:
            tagkind = TAG_TYPE_OPEN
            modified = modified[:-1]

    if tagkind != TAG_TYPE_UNKNOWN:
        modified = modified.split(' ')
        if len(modified) >= 2:
            for i in range(1, len(modified), 1):
                if 'class' in modified[i]:
                    klasse = fullklasse = modified[i].split('"')[1]
                    if '.' in fullklasse:
                        klasse = fullklasse.split('.')
                        klasse = klasse[len(klasse) - 1]
                    break

        modified = modified[0].replace('\n', '')

    short = modified.split('.')
    short = short[len(short) - 1]

    if 'com.seitenbau' in element:
        if short:
            short = 'SB%s' % short
        if klasse:
            klasse = 'SB%s' % klasse
    elif 'jenkins.plugins.jobicon' in element:
        if short:
            short = 'JI%s' % short
        if klasse:
            klasse = 'JI%s' % klasse
    elif 'hudson.tasks.BatchFile' in element:
        if short:
            short = '%sBuilder' % short
        if klasse:
            klasse = '%sBuilder' % klasse
    elif 'hudson.tasks.BuildTrigger' in element:
        if short:
            short = '%sTask' % short
        if klasse:
            klasse = '%sTask' % klasse
    elif 'defaultValue' in element:
        if master.name == 'ExtendedChoiceParameterDefinition':
            if short:
                short = 'ECP%s' % short
    elif 'targets' in element:
        if master.name == 'Ant':
            if short:
                short = 'antT%s' % short[1:]

    return modified, short, tagkind, fullklasse, klasse


def findendtag(idx, kind, start, tag):
    """
    Looks for a specific tag
    :param idx:
    :type idx: int
    :param kind:
    :type kind: int
    :param s:
    :type s: int
    :param tag:
    :type tag: str
    :return:
    :rtype : int
    """
    save = Stack()
    ende = idx if kind == TAG_TYPE_SINGLE else __elements__.find('</' + tag + '>')
    while ende < start:
        save.push(__elements__.peek(ende))
        save.push(ende)
        __elements__.set(ende, None)
        ende = __elements__.find('</' + tag + '>')

    while not save.isempty:
        __elements__.set(save.pop(), save.pop())

    return ende


def handleproperty(sidx, eidx, part, donotrun):
    """
    Function to handle properties
    :param sidx:
    :type sidx: int
    :param eidx:
    :type eidx: int
    :param part:
    :type part: Tag
    :param donotrun:
    :type donotrun: bool
    :return:
    :rtype : bool
    """
    for idx in xrange(sidx, eidx, 1):
        element = __elements__.peek(idx)
        if not element:
            continue

        elem = element.replace('&amp;', '&')
        elem = elem.replace('&gt;', '>')
        elem = elem.replace('&lt;', '<')
        elem = elem.replace('&quot;', '\"')
        elem = elem.replace('&apos;', '\'')
        elem = elem.replace('&#xd;', '')
        if '\\' in elem:
            elem = elem.replace('\\', '\\\\')
            if '*' not in elem and '?' not in elem:
                elem = elem.replace('\\\\', '\\')
        if donotrun:
            part.elements.push(elem)
        else:
            found = False
            for marker in __markers__:
                if marker in element:
                    found = True
                    func = __markers__[marker]
                    if func:
                        items = func(elem)
                        for item in items:
                            part.elements.push(item.strip())
            if not found:
                part.elements.push(elem)

        __elements__.set(idx, None)

    return True


def handletag(sidx, eidx, master, handledtags, handleunknown=True, show=False):
    """
    Function to handle tags
    :param sidx:
    :type sidx: int
    :param eidx:
    :type eidx: int
    :param master:
    :type master: Tag
    :param handledtags:
    :type handledtags: {}
    :param show:
    :type show: bool
    :return:
    :rtype : bool
    """
    if sidx == eidx:
        return False

    count = __parts__.count
    handle = False
    for idx in xrange(sidx, eidx, 1):
        element = __elements__.peek(idx)
        if not element:
            handle = False
            continue

        tag, short, kind, fullklasse, klasse = preparetag(master, element)
        if kind == TAG_TYPE_CLOSE and handleunknown:
            __elements__.set(idx, None)
            continue

        if tag in handledtags:
            handle = kind is TAG_TYPE_OPEN
            ignore = handledtags[tag][0]  # ignore
            prop_type = handledtags[tag][1]  # property type
            handle_tags = handledtags[tag][2]  # tags to handle t
            values = handledtags[tag][3] if len(handledtags[tag]) == 4 else None  # if 4 elements, use value of 4 element

            use_handle = handle_tags if not isinstance(handle_tags, bool) else __mastertags__  # if True, use mastertags
            if ignore and not use_handle:
                __elements__.set(idx, None)
                continue

            handle_tags = fullklasse if isinstance(handle_tags, bool) and handle_tags else None
            if use_handle and '__class__' in use_handle:
                use_handle.update(__mastertags__)
                handle_tags = fullklasse

            start = idx
            ende = findendtag(idx, kind, start, tag)
            if values and use_handle:  # Override
                for relative in reversed(xrange(start, eidx, 1)):
                    item = __elements__.peek(relative)
                    if tag in item:
                        ende = relative
                        break

            if not handle_tags:
                __elements__.set(start, None)
                __elements__.set(ende, None)
            else:
                __elements__.set(start, '<%s>' % handle_tags)
                __elements__.set(ende, '</%s>' % handle_tags)
                start -= 1

            if not use_handle:
                name = short if not klasse else '%s (%s)' % (short, klasse)
                part = prop_type(name)
                skip_property = False
                if master:
                    if values == Builder:
                        skip_property = False
                        found = [e for e in __parts__ if isinstance(e, Builder)]
                        if not found:
                            _tmp = values('Maven')
                            _tmp.elements.push(part)
                            __parts__.put(_tmp)
                        else:
                            found[0].elements.push(part)
                    else:
                        master.elements.push(part)
                else:
                    __parts__.put(part)

                if not skip_property:
                    handleproperty(start, ende, part, values)
                continue

            if not ignore:
                name = short if not klasse else '%s (%s)' % (short, klasse)
                part = prop_type(name)
                if master:
                    master.elements.push(part)
                else:
                    __parts__.put(part)
            else:
                part = master

            handletag(start + 1, ende, part, use_handle, handleunknown, show)
        elif handle and master and kind == TAG_TYPE_UNKNOWN:
            master.elements.push(element)
            __elements__.set(idx, None)
        elif handleunknown:
            __unhandled__.push(element)
            __elements__.set(idx, None)

    if master:
        handled = master.elements.count > 0
    else:
        handled = __parts__.count - count

    return handled


def extract(show, master, marker, handledtags, handleunknown=True):
    """
    General function to extract information from tags and properties
    :param show:
    :type show: bool
    :param master:
    :type master: Tag
    :param marker:
    :type marker: str
    :param handledtags:
    :type handledtags: {}
    :param show:
    :type handleunknown: bool
    :return:
    :rtype : bool
    """
    if __elements__.isempty:
        return False

    global __mastertags__
    __mastertags__ = handledtags

    # Optimize queue by removing None items
    for idx in reversed(xrange(0, __elements__.count)):
        if not __elements__.peek(idx):
            __elements__.get(idx)

    start = sidx = 0
    ende = eidx = __elements__.count
    if marker:
        for idx in xrange(sidx, eidx):
            element = __elements__.peek(idx)
            if not element:
                continue

            if element.startswith('<' + marker):
                start = idx
                break
        else:
            info(show, '#', __name__, 'Marker %s not found!' % marker)
            return False

        ende = start if __elements__.peek(start).endswith('/>') else __elements__.find('</' + marker + '>')
        __elements__.set(start, None)
        __elements__.set(ende, None)

    info(show, '#', __name__, 'Work on indexes from %s to %s' % (start, ende))

    if handledtags:
        return handletag(start, ende, master, handledtags, handleunknown, show)
    else:
        return handleproperty(start, ende, master, True)


__parts__ = __stacks__['parts']
__elements__ = __stacks__['elements']
__unhandled__ = __stacks__['unhandled']
__mastertags__ = None
__markers__ = {
    ' -': splitcommandline,
    ',': splitcomma,
}
