"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import os
import sys
import datetime
from controller.common.log import __logger__, log, view, info, __level__, __fname__
from model.queue import Queue
from model.stack import Stack


__stacks__ = dict(chars=Stack(), lines=Stack(), tags=Stack(), elements=Queue(), parts=Queue(), unhandled=Stack())
__showstatistics__ = False
__dumpparts__ = False
__projectname__ = ''
__projectpath__ = ''
__projectfilename__ = ''
__project__ = None


class Rule(object):
    """Rule: Holds the rules information to process"""

    def __init__(self):
        """
        #:type honorlevel: int
        #:param honorlevel:
        :rtype : Rule
        """
        super(Rule, self).__init__()
        self.rules = {}

    def dumpelements(self, element, lvl):
        """
        Dumps all elements
        :param element:
        :param lvl:
        """
        while not element.elements.isempty:
            item = element.elements.first()
            name = item.name if item is not isinstance(str) else item
            view('.' * lvl, 'Element (%s): %s' % (item.__class__.__name__, name))
            if item is not isinstance(str):
                self.dumpelements(item, lvl + 1)

    @log()
    def importer(self, show=False):
        """
        Imports the rules for the process
        :rtype : None
        :raise e:
        """

        try:
            for path, _, files in os.walk(os.path.join(os.path.abspath(os.path.curdir), 'src/rules')):
                __logger__.debug('(%s) %s Looping path = %s', datetime.datetime.now(), '*' * __level__, path)
                for rulefile in files:
                    if not rulefile.endswith('.py'):
                        __logger__.debug('(%s) %s Skipping file = %s' % (datetime.datetime.now(), '*' * __level__, rulefile))
                        continue

                    if rulefile.startswith('__'):
                        __logger__.debug('(%s) %s Skipping file = %s', datetime.datetime.now(), '*' * __level__, rulefile)
                        continue

                    if not path in sys.path:
                        info(show, '*', __name__, 'Adding path %s' % path)
                        sys.path.append(path)

                    info(show, '*', __name__, 'Importing rule file %s' % rulefile)
                    __import__(rulefile.replace('.py', ''), path)

        except Exception, exe:
            __logger__.exception('Exception (%s): %s', __fname__, exe.message)
            __logger__.error('(%s) %s Exception (%s): %s', datetime.datetime.now(), '*' * __level__, __fname__, exe.message)
            raise exe

    @log()
    def prepare(self, filetoread):
        """
        Prepare the info from the file to read
        :param filetoread:
        :type filetoread: str
        """
        end = False
        found = {
            'tag': False
        }
        with open(filetoread) as file_pointer:
            while True:
                zeichen = file_pointer.read(1)
                if not zeichen:
                    # zeichen = '\n'
                    end = True

                __stacks__['chars'].push(zeichen)

                found['tag'] = (__rules__.rules['pushtag'])()

                if found['tag']:
                    (__rules__.rules['pushline'])(False)

                if __stacks__['tags'].isempty:
                    if end:
                        while not __stacks__['chars'].isempty:
                            if len(__stacks__['chars'].peek().strip()) == 0:
                                __stacks__['chars'].pop()
                        break
                    else:
                        continue

                (__rules__.rules['pushelement'])()
                (__rules__.rules['listlines'])()
                (__rules__.rules['poptag'])()

                if end:
                    while not __stacks__['chars'].isempty:
                        if len(__stacks__['chars'].peek().strip()) == 0:
                            __stacks__['chars'].pop()
                    break

            if __showstatistics__:
                view('.', 'Statistics:')
                view('.', 'Chars left = %s' % __stacks__['chars'].count)
                view('.', 'Lines left = %s' % __stacks__['lines'].count)
                view('.', 'Elements to process = %s' % __stacks__['elements'].count)
                view('', '')

    @log()
    def process(self, rulefile, verbose=False):
        """
        Process rule files
        :type rulefile: str
        :param rulefile:
        :type verbose: bool
        :param verbose:
        """
        if __stacks__['elements'].isempty:
            return

        with open(rulefile) as file_pointer:
            processrule = file_pointer.readlines()

        for item in processrule:
            item = item.strip()
            if item.startswith('\n'):
                continue

            if not item:
                continue

            if item.startswith('#'):
                if verbose:
                    view('*', item[1:].strip())
                continue

            if item.startswith('$include('):
                file_path = os.path.dirname(rulefile)
                file_name = item[9:-1].strip()
                file_name = os.path.join(file_path, file_name)
                if os.path.isfile(file_name) and os.path.exists(file_name):
                    if verbose:
                        view('*', 'include %s' % file_name)
                    self.process(file_name, verbose)
                else:
                    raise EnvironmentError("File '%s' not found!" % file_name)

                continue

            if item in __rules__.rules:
                (__rules__.rules[item])()


def rule(text):
    """
    Marker for a rule implementation
    :rtype : function
    :param text: Name of the actual step
    :type text: str
    :return:
    """

    def register(func):
        """
        Register the implementation function
        :rtype : function
        :param func:
        :type func: function
        :return:
        """
        if text in __rules__.rules.keys():
            __logger__.error('(%s) %s Ignored rule %s for \"%s\"', datetime.datetime.now(), '!' * __level__, str(func.__name__), str(text))
        else:
            __logger__.debug('%s Registered rule %s for \"%s\"', '+' * __level__, str(func.__name__), str(text))
            __rules__.rules[text] = func

        return func

    return register


__rules__ = Rule()
