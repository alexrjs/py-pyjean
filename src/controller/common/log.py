"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import sys
import logging
import datetime


logging.basicConfig(level=logging.INFO, stream=sys.stderr, format="%(message)s")
__logger__ = logging.getLogger(__name__)

__level__ = 0
__fname__ = ''


def log(show=False):
    """
    Log what function is called
    :rtype : func
    :param show:
    """
    # global level1

    def wrap(func):
        '''
        :rtype : func
        :param: func:
        '''
        # global level1

        def wrap_log(*args, **kwargs):
            '''
            :rtype : func
            :param args:
            :param kwargs:
            '''

            fnameold = __fname__
            log.__fname__ = func.__name__
            # logger = logging.getLogger(name)
            # logger.setLevel(logging.INFO)
            #logging.basicConfig(level=logging.INFO, stream=sys.stderr, format="%(message)s")
            #logger = logging.getLogger(__name__)
            __logger__.addHandler(logging.NullHandler())

            # add file handler
            #fh = logging.FileHandler("%s.log" % 'logging')
            format_handler = logging.NullHandler()
            format_handler.setLevel(logging.DEBUG)
            fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
            formatter = logging.Formatter(fmt)
            format_handler.setFormatter(formatter)
            #ch.setFormatter(formatter)
            __logger__.addHandler(format_handler)
            #logger.addHandler(ch)

            global __level__
            __level__ += 1
            sof = datetime.datetime.now()
            if show:
                __logger__.info('(%s) %s Entered "%s"', sof, '+' * __level__, __fname__)
            ret = func(*args, **kwargs)
            eof = datetime.datetime.now()
            if show:
                __logger__.info('(%s) %s Left "%s"', eof, '+' * __level__, __fname__)
            __level__ -= 1
            log.__fname__ = fnameold
            return func, ret

        return wrap_log

    return wrap


def info(visible, marker, name, text):
    """
    Simple info function
    :param visible:
    :param marker:
    :param name:
    :param text:
    :return: None
    """
    if not visible:
        return

    if name:
        __logger__.info('(%s) %s %s : %s', datetime.datetime.now(), marker * __level__, name, text)
    else:
        __logger__.info('(%s) %s %s', datetime.datetime.now(), marker * __level__, text)


def view(marker, text):
    """
    Simple view function
    :param marker:
    :param text:
    """
    if marker:
        marker_text = marker * __level__ if __level__ > 0 else marker * 3
    else:
        marker_text = ''

    formatstring = '(%s) %s %s' % (datetime.datetime.now(), marker_text, text) \
        if marker_text else '(%s) %s' % (datetime.datetime.now(), text)

    __logger__.info(formatstring)
