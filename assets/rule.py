"""
Author:  arjs.net
License: http://opensource.org/licenses/mit-license.html; year=2014,2015; copyright holders=arjs.net
"""

import os

from controller.common.log import view
from controller.common.rule import rule, __stacks__, rules
from controller.jenkins.extract import *


@rule(__name__)
def execute(show=False):
    """
    A rule
    :rtype : bool
    :type show: bool
    :param show:
    """
    if parts.isempty:
        return

    view('i', 'Parts\n')
    while not parts.isempty:
        part = parts.first()
        if not part:
            continue

        name = part.name if type(part) is not str else part
        view('i', '%s\n' % name)

    if not unhandled.isempty:
        view('!', 'Dump unhandled\n')
        while not unhandled.isempty:
            part = unhandled.first()
            if not part:
                continue

            name = part.name if type(part) is not str else part
            out = '%s Unhandled (%s): %s\n' % ('!', part.__class__.__name__, name)
            view('.', out)

    return True
