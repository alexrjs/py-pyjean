#!/usr/bin/kivy
"""
Application from a .kv
======================

The root application is created from the corresponding .kv. Check the test.kv
file to see what will be the root widget.
"""

import shutil
import kivy
import os
import subprocess
import filecmp
from xml.etree import ElementTree
from xmlconfigs import XmlDictConfig, XmlListConfig  # DictDiffer

kivy.require('1.8.0')

from kivy.logger import Logger
from kivy.config import Config

Config.set('kivy', 'log_level', 'info')
Config.set('kivy', 'log_enable', '1')
Config.set('kivy', 'log_dir', 'logs')
Config.set('kivy', 'log_name', 'kivy_%y-%m-%d_%_.txt')
Config.set('graphics', 'width', str(1440))
Config.set('graphics', 'height', str(800))
# Config.set('graphics', 'fullscreen', str('auto'))

from kivy.app import App
from kivy.uix.codeinput import CodeInput
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.clock import Clock
from kivy.properties import StringProperty
from kivy.properties import ObjectProperty
from pygments.lexers.web import XmlLexer

app = None
ignoredebug = True


class MyPanel(BoxLayout):
    currentfile = ''
    current_fn = ''
    current_ext = ''
    current_fp = ''
    current_rp = ''
    pypy = 'pypy'
    pyjean = '/Users/alexrjs/Work/Code/py/pyjean/src/'
    diffmerge = '/Applications/DiffMerge.app/Contents/MacOS/DiffMerge -nosplash'
    main = 'main.py'
    out = 'out'
    ok = 'ok'
    buffer = []
    incall = False
    increate = False
    viewdiffcmds = []

    def all_callback(self, instance, value):
        #print('the switch', instance, 'is', value)
        self.ids.btn_create.disabled = not value

    def dump(self, xml, level):
        ident = ' ' * (4 * level)
        for key in xml.iterkeys():
            print '%s%s' % (ident, key)
            value = xml[key]
            if type(value) in [XmlDictConfig, dict]:
                self.dump(value, level + 1)
            elif type(value) is XmlListConfig:
                for element in value:
                    if type(element) in [XmlDictConfig, dict]:
                        self.dump(element, level + 1)
                    else:
                        print '%s%s' % (ident, element)

    def debug(self, level, text, ignore=ignoredebug):
        ident = ' ' * (4 * level)
        out = '%s%s' % (ident, text)
        self.buffer.append(out)
        if ignore:
            return

        print out

    def compare(self, info, xml1, xml2, level, full=False):
        try:
            if not xml1 and not xml2:
                self.debug(level, 'equal (no xml1 and no xml2): %s' % info)
                return True
            elif xml1 and not xml2:
                self.debug(level, 'source only (no xml2): %s' % info)
                rv = True
                if type(xml1) is not str:
                    for key in xml1.iterkeys():
                        self.debug(level, "%s {" % key)
                        if key in ['plugin', 'class', 'reference']:
                            self.debug(level, "} %s: ignored" % key)
                        else:
                            rv = rv and False
                            self.debug(level, "} %s: not found" % key)
                            self.ids.text_diff1.text += '~ ' + info + ':' + key + '\n'
                            self.ids.text_diff2.text += '~ ' + info + ': ?\n'
                else:
                    rv = False

                return rv
            elif xml2 and not xml1:
                self.debug(level, 'target only (no xml1): %s' % info)
                rv = True
                if type(xml2) is not str:
                    for key in xml2.iterkeys():
                        self.debug(level, "%s {" % key)
                        if key in ['plugin', 'class', 'reference']:
                            self.debug(level, "} %s: ignored" % key)
                        else:
                            rv = rv and False
                            self.debug(level, "} %s: not found" % key)
                            self.ids.text_diff1.text += '~ ' + info + ': ?\n'
                            self.ids.text_diff2.text += '~ ' + info + ':' + key + '\n'
                else:
                    rv = False

                return rv
            elif type(xml1) in [XmlDictConfig, dict] and type(xml2) in [XmlDictConfig, dict]:
                self.debug(level, 'Dict1: %s' % str(xml1.keys()))
                self.debug(level, 'Dict2: %s' % str(xml2.keys()))
                rv = True
                delkeys = []
                for key in xml1.iterkeys():
                    self.debug(level, "\n")
                    self.debug(level, "'%s' {" % key)
                    value1 = xml1[key]
                    if key in xml2:
                        if key in ['plugin', 'class', 'reference']:
                            self.debug(level, "} %s: ignored" % key)
                            del xml2[key]
                            continue

                        if full:
                            self.ids.text_diff1.text += '= ' + key + '\n'
                            self.ids.text_diff2.text += '= ' + key + '\n'

                        value2 = xml2[key]
                        found = self.compare(key, value1, value2, level + 1)
                        if found:
                            self.debug(level, "} %s: equal" % key)
                            rv = rv and True
                            if full:
                                if type(value1) not in [XmlDictConfig, dict]:
                                    self.ids.text_diff1.text += '= ' + str(value1) + '\n'
                                    self.ids.text_diff2.text += '= ' + str(value2) + '\n'
                        else:
                            self.debug(level, "} %s: changed or not found" % key)

                            rv = rv and False
                            if type(value1) not in [XmlDictConfig, dict]:
                                self.ids.text_diff1.text += '~ ' + info + ':' + key + '\n'
                                self.ids.text_diff2.text += '~ ' + info + ':' + key + '\n'
                                self.ids.text_diff1.text += '~ ' + str(value1) + '\n\n'
                                self.ids.text_diff2.text += '~ ' + str(value2) + '\n\n'

                        delkeys.append(key)
                        del xml2[key]
                    else:
                        if key in ['plugin', 'class', 'reference']:
                            self.debug(level, "} %s: ignored" % key)
                            continue

                        rv = rv and False
                        self.debug(level, "} %s: source only" % key)
                        self.ids.text_diff1.text += '< ' + info + '\n'
                        self.ids.text_diff2.text += '< ' + info + '\n'

                        if type(value1) not in [XmlDictConfig, dict]:
                            self.ids.text_diff1.text += '< ' + key + ': ' + str(value1) + '\n\n'
                            self.ids.text_diff2.text += '< ' + key + ': ?\n\n'
                        else:
                            self.ids.text_diff1.text += '< ' + key + '\n\n'
                            self.ids.text_diff2.text += '< ?\n\n'

                for key in delkeys:
                    del xml1[key]

                if len(xml2) > 0:
                    self.debug(level, "\n")
                    for key in xml2:
                        if key in ['plugin', 'class', 'reference']:
                            self.debug(level, "%s: ignored" % key)
                            continue

                        rv = rv and False
                        self.debug(level, "%s: target only" % key)
                        self.ids.text_diff1.text += '> ' + info + ': ?\n\n'
                        self.ids.text_diff2.text += '> ' + info + ': ' + str(key) + '\n\n'

                return rv
            elif type(xml1) is XmlListConfig and type(xml2) is XmlListConfig:
                rv = True
                for element in xml1:
                    #self.debug(level, "\n")
                    self.debug(level, "(l) xml1 = %s" % element)
                    if type(element) in [XmlDictConfig, dict]:
                        found = False
                        for other in xml2:
                            if not other:
                                self.debug(level, "(l) skipped xml2 = %s\n" % other)
                                continue

                            self.debug(level, "(l) xml2 = %s\n" % other)
                            self.debug(level, "{")
                            if type(other) in [XmlDictConfig, dict]:
                                found = self.compare(info, element, other, level + 1)
                                if found:
                                    self.debug(level, 'found: %s = %s' % (element, other))
                                    break
                            else:
                                self.ids.text_diff1.text += '<1 ' + info + ': ' + str(element) + '\n'
                                self.ids.text_diff2.text += '<2 ' + info + ': ' + str(other) + '\n'

                            self.debug(level, "}")

                        if found:
                            #self.debug(level, 'found: %s' % xml1)
                            xml2.remove({})

                        rv = rv and found

                    elif element in xml2:
                        if full:
                            self.ids.text_diff1.text += '= ' + str(element) + '\n'
                            self.ids.text_diff2.text += '= ' + str(element) + '\n'

                        xml2.remove(element)
                        rv = rv and True

                    else:
                        if info in ['plugin', 'class', 'reference']:
                            self.debug(level, "ignored: %s" % info)
                            continue

                        rv = rv and False
                        if type(element) not in [XmlDictConfig, dict]:
                            self.ids.text_diff1.text += '< ' + info + ': ' + str(element) + '\n'
                            self.ids.text_diff2.text += '< ' + '\n'
                        else:
                            self.ids.text_diff1.text += '<5 ' + info + '\n'
                            self.ids.text_diff2.text += '<6 ' + '\n'
                            #self.compare('root', xml1, xml2)

                    result = 'equal' if rv else 'diffrent'
                    self.debug(level, "} xml1,xml2 ==> %s" % result)
                    self.debug(level, "\n")

                if len(xml2) > 0:
                    for element in xml2:
                        if len(element):
                            if element in ['plugin', 'class', 'reference', '<blank>']:
                                self.debug(level, "5,ignored: %s" % element)
                                continue

                            rv = False
                            self.debug(level, '5,target only: %s' % element)
                            self.ids.text_diff1.text += '> ' + '\n'
                            self.ids.text_diff2.text += '> ' + info + ': ' + str(element) + '\n'

                return rv
            elif type(xml1) is str and type(xml2) is str:
                self.debug(level, '(1) %s:' % info)
                self.debug(level, '(1) xml1: %s\n' % str(xml1).strip())
                self.debug(level, '(1) xml2: %s\n' % str(xml2).strip())
                if info == 'description':
                    x1 = xml1.expandtabs(4).replace('\n', '').replace(' ', '').strip()
                    x2 = xml2.replace('&#xd;', '')
                    x2 = x2.replace('&gt;', '>')
                    x2 = x2.replace('&lt;', '<')
                    x2 = x2.replace('&quot;', '\"')
                    x2 = x2.replace('&apos;', '\'')
                    x2 = x2.replace('&amp;', '&')
                    x2 = x2.expandtabs(4).replace('\n', '').replace(' ', '').strip()
                    c = (x1 == x2)
                    self.debug(level, '(1 - wo/ws) xml1: %s\n' % x1)
                    self.debug(level, '(1 - wo/ws) xml2: %s\n' % x2)
                    self.debug(level, '(1 - wo/ws) xml1 == xml2: %s\n' % str(c))
                    return c
                elif info == 'defaultValue':
                    x1 = xml1.replace(',', '').replace(' ', '').strip()
                    x2 = xml2.replace('&#xd;', '')
                    x2 = x2.replace('&gt;', '>')
                    x2 = x2.replace('&lt;', '<')
                    x2 = x2.replace('&quot;', '\"')
                    x2 = x2.replace('&apos;', '\'')
                    x2 = x2.replace('&amp;', '&')
                    x2 = x2.replace(',', '').replace(' ', '').strip()
                    c = (x1 == x2)
                    self.debug(level, '(1 - wo/ws+comma) xml1: %s\n' % x1)
                    self.debug(level, '(1 - wo/ws+comma) xml2: %s\n' % x2)
                    self.debug(level, '(1 - wo/ws+comma) xml1 == xml2: %s\n' % str(c))
                    return c
                elif '\\n' in xml2:
                    x1 = xml1.replace('\\n', '\n').replace(' ', '').strip()
                    x2 = xml2.replace('\\n', '\n').replace(' ', '').strip()
                    c = (x1 == x2)
                    self.debug(level, '(1 - wo/ws+nl) xml1: %s\n' % x1)
                    self.debug(level, '(1 - wo/ws+nl) xml2: %s\n' % x2)
                    self.debug(level, '(1 - wo/ws+nl) xml1 == xml2: %s\n' % str(c))
                    return c
                else:
                    c = (xml1.strip() == xml2.strip())
                    if c:
                        self.debug(level, '(1 - w/ws) xml1 == xml2: %s\n' % str(c))
                        return c
                    else:
                        x1 = xml1.expandtabs(4).replace('\n', '').replace(' ', '').strip()
                        x2 = xml2.expandtabs(4).replace('\n', '').replace(' ', '').strip()
                        c = (x1 == x2)
                        self.debug(level, '(1 - single) xml1: %s\n' % x1)
                        self.debug(level, '(1 - single) xml2: %s\n' % x2)
                        self.debug(level, '(1 - single) xml1 == xml2: %s\n' % str(c))
                        return c
            else:
                self.debug(level, '(2) xml1: %s' % str(type(xml1)))
                self.debug(level, '(2) xml2: %s' % str(type(xml2)))
                self.debug(level, '(2) xml1: %s' % str(xml1))
                self.debug(level, '(2) xml2: %s' % str(xml2))
                self.ids.text_diff1.text += '~ ' + info + ':' + info + '\n'
                self.ids.text_diff2.text += '~ ' + info + ':' + info + '\n'
                self.ids.text_diff1.text += '~ ' + str(type(xml1)) + '\n\n'
                self.ids.text_diff2.text += '~ ' + str(type(xml2)) + '\n\n'

        except Exception as e:
            print '### - ', e.message

    def do_selected(self, filename):
        if not filename:
            self.ids.btn_create.disabled = True
            self.ids.btn_test.disabled = True
            self.ids.btn_ok.disabled = True
            self.ids.btn_clean.disabled = True
            self.ids.btn_viewdiff.disabled = True
            self.viewdiffcmds = []
            return

        #print "selected: %s" % filename[0]
        self.buffer = []
        fn = filename[0]
        if not os.path.exists(filename[0]):
            self.currentfile = ''
            self.current_fn = ''
            self.current_ext = ''
            self.current_fp = ''
            return
        else:
            global app
            app.title = 'PyJeAn Viewer: %s' % fn
            if '$' in self.currentfile:
                self.currentfile = fn.replace('$', '\$')
                xmlfile = fn
            else:
                self.currentfile = fn
                xmlfile = fn

            #print xmlfile
            #print self.currentfile

            self.current_fn, self.current_ext = os.path.splitext(os.path.basename(fn))
            #print self.current_fn
            if self.current_fn.endswith('.config'):
                self.current_fn = self.current_fn.replace('.config', '')
            #print self.current_fn
            self.current_rp = os.path.dirname(fn)
            #print self.current_rp
            self.current_fp = os.path.join(self.current_rp, self.out)
            #print self.current_fp

        with open(xmlfile, 'rb') as f:
            self.ids.text_source.text = f.read().decode('utf8').replace('\t', ' ' * 4)

        tab = self.ids.tp.get_current_tab().text
        if tab == 'XML Diff':
            self.do_compare()
        elif tab == 'XML Gen':
            self.do_xml()
        elif tab == 'Dump':
            self.do_dump()
        elif tab == 'Yaml':
            self.do_yaml()
        elif tab == 'Sequence':
            self.do_sequence()
        elif tab == 'Picture':
            self.do_picture()
        elif tab == 'Output':
            self.ids.tp.switch_to(self.ids.tp.get_def_tab())

        self.ids.btn_create.disabled = False
        self.ids.btn_test.disabled = False
        self.ids.btn_ok.disabled = False
        self.ids.btn_clean.disabled = False
        self.ids.btn_viewdiff.disabled = True
        self.viewdiffcmds = []

    def do_compare(self):
        #print 'xml file:', self.currentfile

        self.ids.text_out.text = ''
        if not self.currentfile:
            return

        if not ignoredebug:
            print '*' * 70

        try:
            self.ids.text_diff1.text = ''
            self.ids.text_diff2.text = ''
            x1fn = self.currentfile
            x2fn = os.path.join(self.current_fp, self.current_fn)
            # print 'x1fn file:', x1fn
            # print 'x2fn file:', x2fn
            if os.path.exists(x1fn):
                tree = ElementTree.parse(x1fn)
                root = tree.getroot()
                xmldict1 = XmlDictConfig(root)
                # if xmldict1:
                #     self.dump(xmldict1, 0)
                tree = ElementTree.parse(x2fn)
                root = tree.getroot()
                xmldict2 = XmlDictConfig(root)
                # if xmldict2:
                #     self.dump(xmldict2, 0)

                lines = ''
                self.compare('root', xmldict1, xmldict2, 0)
                #for line in self.buffer:
                #    print line
                #    lines += line + '\n'

                #self.ids.text_out.text = lines
            else:
                pass

        except Exception as e:
            self.ids.text_out.text = e.message
            print '###', e.message

        else:
            d1fn = os.path.join(self.current_fp, self.current_fn + '.1')
            with open(d1fn, 'wb') as f:
                f.write(self.ids.text_diff1.text)
            d2fn = os.path.join(self.current_fp, self.current_fn + '.2')
            with open(d2fn, 'wb') as f:
                f.write(self.ids.text_diff2.text)
            dfn = os.path.join(self.current_fp, self.current_fn + '.cmp')
            with open(dfn, 'wb') as f:
                f.write('\n'.join(self.buffer))
            self.buffer = []

    def dump_helper(self, use_file):
        out = ''
        if not use_file:
            return out

        #print 'xml file:', use_file

        cmd = 'cd %s; %s %s -d -p %s -f %s -o %s' % \
              (self.pyjean, self.pypy, self.main, 'data/dump.rule', use_file.replace('$', '\$'), self.out)
        #print cmd

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        p.wait()
        stdout, stderr = p.communicate()

        if stdout and len(stdout) > 0:
            out += stdout

        if stderr and len(stderr) > 0:
            out += stderr

        return out

    def do_dump(self):
        if not self.currentfile:
            return

        self.ids.text_out.text = ''
        try:
            tfn = os.path.join(self.current_fp, self.current_fn + '.dmp')
            #print 'dmp file:', tfn

            if self.ids.cb_recreate.active and os.path.exists(tfn):
                os.remove(tfn)

            if not os.path.exists(tfn):
                self.ids.text_out.text += self.dump_helper(self.currentfile)

            if os.path.exists(tfn):
                with open(tfn, 'rb') as f:
                    self.ids.text_dump.text = f.read().decode('utf8').replace('\t', ' ' * 4)

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def yaml_helper(self, use_file):
        out = ''
        if not use_file:
            return out

        #print 'yaml file:', use_file

        cmd = 'cd %s; %s %s -d -p %s -f %s -o %s' % \
              (self.pyjean, self.pypy, self.main, 'data/yaml.rule', use_file.replace('$', '\$'), self.out)
        # print cmd

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        p.wait()
        stdout, stderr = p.communicate()

        if stdout and len(stdout) > 0:
            out += stdout

        if stderr and len(stderr) > 0:
            out += stderr

        return out

    def do_yaml(self):
        if not self.currentfile:
            return

        self.ids.text_out.text = ''
        self.ids.text_yaml.text = ''
        try:
            tfn = os.path.join(self.current_fp, self.current_fn + '.yaml')
            #print 'yaml file:', tfn

            if self.ids.cb_recreate.active and os.path.exists(tfn):
                os.remove(tfn)

            if not os.path.exists(tfn):
                self.ids.text_out.text += self.yaml_helper(self.currentfile)

            if os.path.exists(tfn):
                with open(tfn, 'rb') as f:
                    self.ids.text_yaml.text = f.read().decode('utf8').replace('\t', ' ' * 4)

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def xml_path_helper(self, use_file):
        current_fn, current_ext = os.path.splitext(os.path.basename(use_file))
        # print current_fn
        if current_fn.endswith('.config'):
            current_fn = current_fn.replace('.config', '')
        #print current_fn
        current_rp = os.path.dirname(use_file)
        #print current_rp
        current_fp = os.path.join(current_rp, self.out)
        #print current_fp
        uf = os.path.join(current_fp, current_fn)
        #print uf
        return uf

    def xml_helper(self, use_file):
        out = ''
        if not use_file:
            return out

        #print 'xml file:', use_file
        uf = self.xml_path_helper(use_file)

        # jjbcmdfp = '/Applications/pypy/bin/jenkins-jobs'
        jjbcmd = 'jenkins-jobs'
        jjbcfgfp = '/Users/alexrjs/Work/Code/py/jjb'
        jjbcfg = 'jjb.ini'
        jjbcfgfn = os.path.join(jjbcfgfp, jjbcfg)
        jjbopt = 'test'
        yfn = os.path.join(self.current_fp, uf.replace('$', '\$') + '.yaml')
        cmd = '%s --conf %s %s %s -o %s' \
              % (jjbcmd, jjbcfgfn, jjbopt, yfn, self.current_fp)
        print cmd

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()
        stdout, stderr = p.communicate()

        if stdout and len(stdout) > 0:
            out += stdout

        if stderr and len(stderr) > 0:
            out += stderr

        return out

    def do_xml(self):
        if not self.currentfile:
            return

        self.ids.text_out.text = ''
        self.ids.text_generate.text = ''
        try:
            tfn = os.path.join(self.current_fp, self.current_fn)
            #print 'gen file:', tfn

            if self.ids.cb_recreate.active and os.path.exists(tfn):
                #print 'del file:', tfn
                os.remove(tfn)

            if not os.path.exists(tfn):
                self.ids.text_generate.text = self.xml_helper(self.currentfile)

            if os.path.exists(tfn):
                with open(tfn, 'rb') as f:
                    self.ids.text_generate.text = f.read().decode('utf8').replace('\t', ' ' * 4)

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def sequence_helper(self, use_file):
        out = ''
        if not use_file:
            return out

        #print 'seq file:', use_file

        cmd = 'cd %s; %s %s -d -l -p %s -f %s -o %s' \
              % (self.pyjean, self.pypy, self.main, 'data/sequence.rule', use_file.replace('$', '\$'), self.out)
        # print cmd

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        p.wait()
        stdout, stderr = p.communicate()

        if stdout and len(stdout) > 0:
            out += stdout

        if stderr and len(stderr) > 0:
            out += stderr

        return out

    def do_sequence(self):
        if not self.currentfile:
            return

        self.ids.text_out.text = ''
        try:
            tfn = os.path.join(self.current_fp, self.current_fn + '.seq')
            #print 'seq file:', tfn

            if self.ids.cb_recreate.active and os.path.exists(tfn):
                os.remove(tfn)

            if not os.path.exists(tfn):
                self.ids.text_out.text += self.sequence_helper(self.currentfile)

            if os.path.exists(tfn):
                with open(tfn, 'rb') as f:
                    self.ids.text_seq.text = f.read().decode('utf8').replace('\t', ' ' * 4)

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def picture_show(self, tfn):
        if not os.path.exists(tfn):
            tfn = 'waiting.png'

        self.ids.picture.source = tfn
        self.ids.picture.width = self.ids.picture.ids.seq_image.norm_image_size[0]
        self.ids.picture.height = self.ids.picture.ids.seq_image.norm_image_size[1]
        self.ids.picture.scroll_y = 1

    def picture_callback(self, dt):
        try:
            ofn = os.path.join(self.pyjean, 'stdout.txt')
            tfn = os.path.join(self.current_fp, self.current_fn + '.png')
            #print 'i', self.ids.picture.ids.seq_image.norm_image_size
            if os.path.exists(tfn):
                Clock.unschedule(self.picture_callback)
                self.picture_show(tfn)

                out = ''
                if os.path.exists(ofn):
                    with open(ofn, 'rb') as f:
                        out = f.read().decode('utf8').replace('\t', ' ' * 4)

                print 'out', out
                if self.increate:
                    self.ids.text_out.text += out
                else:
                    self.ids.text_out.text = out

                if os.path.exists(ofn):
                    os.remove(ofn)

                self.incall = False
                self.increate = False
                #print 'done'
            elif self.incall:
                #print 'incall'
                Clock.schedule_once(self.picture_callback, dt)
                self.ids.text_out.text += "### Scheduled, again"
            else:
                #print "else"
                self.incall = True
                Clock.schedule_once(self.picture_callback, dt)
                #out = self.picture_helper(self.currentfile)
                #self.picture_show(tfn)
                #self.increate = False

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def picture_helper(self, use_file):
        out = ''
        if not use_file:
            return out

        #print 'png file:', use_file

        cmd = 'cd %s; %s %s -d -l -p %s -f %s -o %s >stdout.txt 2>&1' \
              % (self.pyjean, self.pypy, self.main, 'data/picture.rule', use_file.replace('$', '\$'), self.out)
        #print cmd

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        p.wait()
        stdout, stderr = p.communicate()

        out = ''
        ofn = os.path.join(self.pyjean, 'stdout.txt')
        if os.path.exists(ofn):
            with open(ofn, 'rb') as f:
                out = f.read().decode('utf8').replace('\t', ' ' * 4)

        if stdout and len(stdout) > 0:
            out += stdout

        if stderr and len(stderr) > 0:
            out += stderr

        return out

    def do_picture(self):
        #print 'xml file:', self.currentfile

        if not self.currentfile:
            return

        if not self.increate:
            self.ids.text_out.text = ''

        try:
            ofn = os.path.join(self.pyjean, 'stdout.txt')
            if os.path.exists(ofn):
                os.remove(ofn)

            tfn = os.path.join(self.current_fp, self.current_fn + '.png')
            #print 'png file:', tfn

            if self.ids.cb_recreate.active and os.path.exists(tfn):
                #print 'del file:', tfn
                os.remove(tfn)

            if not os.path.exists(tfn):
                self.ids.text_out.text += self.picture_helper(self.currentfile)

            if os.path.exists(tfn):
                self.picture_show(tfn)
            else:
                self.picture_show('')
                self.ids.text_out.text += '### Scheduled'
                Clock.schedule_once(self.picture_callback, 0.5)

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def do_move(self):
        if not self.currentfile:
            return

        try:
            self.ids.tp.switch_to(self.ids.tph_out)
            ok_fp = os.path.join(self.current_rp, self.ok)
            if not os.path.exists(ok_fp):
                os.makedirs(ok_fp)
            ok_tgt = os.path.join(ok_fp, self.current_fn + '.config.xml')
            shutil.move(self.currentfile, ok_tgt)
            self.ids.text_out.text = 'Move to ok folder:\n'
            self.ids.text_out.text += str(''.join(self.current_fn)) + '\n\n'

            self.ids.text_out.text += 'Move to test folder:\n'
            test_fp = os.path.join(ok_fp, self.current_fn)
            if not os.path.exists(test_fp):
                os.makedirs(test_fp)
            files = os.listdir(self.current_fp)
            for f in files:
                if f.startswith(self.current_fn):
                    ok_src = os.path.join(self.current_fp, f)
                    ok_tgt = os.path.join(test_fp, f)
                    shutil.move(ok_src, ok_tgt)
                    self.ids.text_out.text += str(f) + '\n'

            self.ids.list_view_tab._trigger_update()
            self.ids.btn_create.disabled = True
            self.ids.btn_test.disabled = True
            self.ids.btn_ok.disabled = True
            self.ids.btn_clean.disabled = True
            self.ids.btn_viewdiff.disabled = True
            self.viewdiffcmds = []

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def do_test(self):
        if not self.currentfile:
            return

        try:
            self.ids.text_out.text = 'Test files:\n'
            self.ids.tp.switch_to(self.ids.tph_out)
            ok_fp = os.path.join(self.current_rp, self.ok)
            ok_src = self.current_fn + '.config.xml'
            ok_tgt = os.path.join(ok_fp, ok_src)
            if not os.path.exists(ok_tgt):
                self.ids.text_out.text += ' not found in ok: '

                self.ids.text_out.text += ok_src + '\n'
                return
            else:
                cmpare = filecmp.cmp(self.currentfile, ok_tgt)
                if not cmpare:
                    self.ids.text_out.text += ' not ok: '
                else:
                    self.ids.text_out.text += '     ok: '

                self.ids.text_out.text += ok_src + '\n'

            enable = False
            self.viewdiffcmds = []
            ok_fp = os.path.join(ok_fp, self.current_fn)
            out_fp = os.path.join(self.current_rp, self.out)
            files = os.listdir(ok_fp)
            for f in files:
                #if f.endswith('.png'):
                #    continue

                if f.startswith(self.current_fn):
                    ok_tgt = os.path.join(ok_fp, f)
                    if not os.path.exists(ok_tgt):
                        continue

    #                print ok_tgt

                    out_tgt = os.path.join(out_fp, f)
                    if not os.path.exists(out_tgt):
                        continue

    #                print out_tgt

                    cmpare = filecmp.cmp(ok_tgt, out_tgt)
                    if cmpare:
                        self.ids.text_out.text += '     ok: '
                    else:
                        enable = True
                        self.ids.text_out.text += ' not ok: '
                        if not f.endswith('.png'):
                            self.viewdiffcmds.append('%s %s %s' % (self.diffmerge, ok_tgt, out_tgt))
                        else:
                            p = f.replace('.png', '.diff.png')
                            p = os.path.join(self.current_rp, p)
                            self.viewdiffcmds.append('composite %s %s -compose difference %s && open %s' %
                                                     (ok_tgt, out_tgt, p, p))

                    self.ids.text_out.text += str(f) + '\n'
                    self.ids.btn_viewdiff.disabled = not enable

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def do_clean(self):
        if not self.currentfile:
            return

        try:
            self.ids.tp.switch_to(self.ids.tph_out)
            ok_src = self.current_fn + '.config.xml'
            ok_tgt = os.path.join(self.current_rp, ok_src)
            os.remove(ok_tgt)
            #print "1:remove", ok_src
            self.ids.text_out.text = 'Cleaned:\n'
            self.ids.text_out.text += ok_src + '\n\n'

            self.ids.text_out.text += 'Cleaned out folder:\n'
            out_fp = os.path.join(self.current_rp, self.out)
            files = os.listdir(out_fp)
            for f in files:
                if not f.startswith(self.current_fn):
                    continue

                ok_tgt = os.path.join(out_fp, f)
                if os.path.exists(ok_tgt):
                    #print "2:remove", ok_tgt
                    os.remove(ok_tgt)
                    self.ids.text_out.text += str(f) + '\n'

            self.ids.list_view_tab._trigger_update()

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def do_viewdiff(self):
        try:
            #print self.viewdiffcmds
            self.ids.text_out.text += '\n'
            for cmd in self.viewdiffcmds:
                #print cmd
                self.ids.text_out.text += cmd + '\n'
                p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                #p.wait()
                stdout, stderr = p.communicate()

                if stdout and len(stdout) > 0:
                    self.ids.text_out.text += stdout

                if stderr and len(stderr) > 0:
                    self.ids.text_out.text += stderr

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message

    def create_helper(self, use_file):
        out = []
        self.increate = True
        self.ids.tp.switch_to(self.ids.tph_out)

        out.append('### Create .dmp file')
        out.append(self.dump_helper(use_file))

        out.append('### Create .yaml file')
        out.append(self.yaml_helper(use_file))

        out.append('### Create .xml file')
        out.append(self.xml_helper(use_file))

        out.append('### Create .seq file')
        out.append(self.sequence_helper(use_file))

        out.append('### Create .png file')
        out.append(self.picture_helper(use_file))

        return '\n'.join(out)

    def do_create(self):
        try:
            if self.ids.cb_all.active:
                path = self.ids.list_view_tab.path
                if not path or not os.path.exists(path):
                    self.ids.text_out.text = '### Problems: Path not found'
                    return

                out = ''
                self.increate = True
                self.ids.tp.switch_to(self.ids.tph_out)
                files = os.listdir(path)
                for f in files:
                    print f
                    if not f.endswith('.xml'):
                        continue

                    if not f.endswith('.xml'):
                        continue

                    use_file = os.path.join(path, f)
                    out += '+' * 90 + '\n'
                    out += '### ' + str(f) + '\n'
                    out += self.create_helper(use_file) + '\n'
            else:
                if not self.currentfile:
                    self.ids.text_out.text = '### Problems: File currentfile not found'
                    return

                out = self.create_helper(self.currentfile)

            self.ids.text_out.text = out

        except IOError as e:
            self.ids.text_out.text += e.message
        except Exception as e:
            self.ids.text_out.text += e.message


class KivyRenderTextInput(CodeInput):
    # def _keyboard_on_key_down(self, window, keycode, text, modifiers):
    #     is_osx = sys.platform == 'darwin'
    #     # Keycodes on OSX:
    #     ctrl, cmd = 64, 1024
    #     key, key_str = keycode
    #
    #     if text and not key in (list(self.interesting_keys.keys()) + [27]):
    #         # This allows *either* ctrl *or* cmd, but not both.
    #         if modifiers == ['ctrl'] or (is_osx and modifiers == ['meta']):
    #             if key == ord('s'):
    #                 self.catalog.change_kv(True)
    #                 return
    #
    #     super(KivyRenderTextInput, self)._keyboard_on_key_down(
    #         window, keycode, text, modifiers)
    pass


class ScrollableLabel(ScrollView):
    text = StringProperty('')
    lexer = ObjectProperty(XmlLexer())


class ScrollableImage(ScrollView):
    source = StringProperty('noimage.jpg')


class ViewerApp(App):
    def __init__(self, **kwargs):
        super(ViewerApp, self).__init__(**kwargs)
        self.title = 'PyJeAn Viewer'

    def build(self):
        self.root.ids.cb_all.bind(active=self.root.all_callback)


if __name__ == '__main__':
    Logger.info('Application: This is a test')
    app = ViewerApp()
    app.run()
