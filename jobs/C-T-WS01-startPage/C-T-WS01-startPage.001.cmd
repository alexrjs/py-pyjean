@echo off
call %JENKINS_TOOLS%\cmd\test-function-execute-sikuli.cmd
set EC=%ERRORLEVEL%
if %EC% EQU 0 @(
  @echo *** Result: true %RELEASE_TAG% %EC%
) else (
  @echo !!! Result: false %RELEASE_TAG% %EC%
)
echo.
exit %EC%