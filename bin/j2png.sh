#!/bin/bash
dn=`dirname $0`
pushd $dn > /dev/null
dn=`pwd`
while [[ "$dn" != *pyjean ]]
do
    cd ..
    dn=`pwd`
done
pypy src/ -s -d -v -p data/picture.rule -o out -f $1
popd > /dev/null
