#!/bin/bash
dn=`dirname $0`
pushd $dn > /dev/null
dn=`pwd`
while [[ "$dn" != *pyjean ]]
do
    cd ..
    dn=`pwd`
done
pypy src/ -s -d -v -p src/data/$1.rule -o out -f $2
popd > /dev/null
